# Model Driven Engineering Project - Optimization and parallelization of CBD models #

This project extends the CBD (Causal Block Diagram) Multiple Output source code given from the course *Modelling of Software Intensive Systems* in order to present the notion of the Parallel Block.

### Abstract ###
This project presents methods to parallelize Causal Block Diagram models by using parallelized dependency graphs.This modified graph, combined with the original CBD model produces output traces to parallel languages and frameworks. The resulting source code is based on Domain, Functional and Task Decomposition methodologies.

The pipeline parallel programming pattern is applied in the case of loop and delay dependencies of the original CBD model. The generated pipeline uses computational overhead calculations in order to generate almost equally spaced stages.

The resulting source code uses the *Intel Thread Building Blocks*, *OpenCL*, *OpenMP* frameworks that run on modern CPU, GPU and DSP compute devices. After the execution of the output code, it is also possible to generate a custom OpenGL graph transformation to visualize the input and output values into a three dimensional cartesian system.

### Keywords ###
CBD, Parallel, Dependency Graph, Parallelization, Computational Overhead, Optimization, Block diagrams, Domain Decomposition, Task Decomposition, Pipeline, GPGPU, Compute Device, OpenCL, OpenMP, Intel Thread Building Blocks

###Documentation
- **Presentation**: https://onedrive.live.com/redir?resid=F3C315EB7F683B03!17863&authkey=!AIcJkHNSst6A5G4&ithint=file%2cpptx
- Full **Documentation**: https://onedrive.live.com/redir?resid=F3C315EB7F683B03!17862&authkey=!AH3PKR52ybYnu6g&ithint=file%2cpdf

###References
1. *OpenCL API 1.2 Reference Card*, page 1
2. *OpenMP API 3.1 Reference Card*, page 1
3. *Structured Parallel Programming - Pipeline*, page 99, 253
4. *The semantics and execution of a synchronous block-diagram language*, page 35
5. *Modularity vs Reusability: Code Generation from Synchronous Block Diagrams*, page 1504
6. *Task Synthesis for Latency-sensitive Synchronous Block Diagram*, page 10
7. *Heterogenous Computing with OpenCL*, page 10
8. *OpenCl Programming guide for Mac*, page 130-135