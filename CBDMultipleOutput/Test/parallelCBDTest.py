#!/usr/bin/env python
#
# Unit tests for all the basic CBD blocks, discrete-time CBD.

import unittest

from CBDMultipleOutput.Source.CBDParallelCompiler.CBDParallelCompiler import *
from CBDMultipleOutput.Source.CBDParallelCompiler.CBDUtilityEditors.CBDBenchmarkResultsEditor import CBDBenchmarkResultsEditor
from CBDMultipleOutput.Source.CBD import *

NUM_DISCR_TIME_STEPS = 26000000
USE_RANDOM_CONSTANTS = False

class ParallelCBDTestCase(unittest.TestCase):
  def setUp(self):
    self.CBD = CBD("CBD_for_block_under_test")

  def _run(self, num_steps=1, delta_t = 1.0):
    self.CBD.run(num_steps, delta_t)

  def _getSignal(self, blockname, output_port = None):
    foundBlocks = [ block for block in self.CBD.getBlocks() if block.getBlockName() == blockname ]
    numFoundBlocks = len(foundBlocks)
    if numFoundBlocks == 1:
      signal =  foundBlocks[0].getSignal(name_output = output_port)
      return [x.value for x in signal]
    else:
      raise Exception(str(numFoundBlocks) + " blocks with name " + blockname + " found.\nExpected a single block.")

  def splitBenchmarkResultsCsv(self, compilerSettings):

    benchmarkResultsEditor = CBDBenchmarkResultsEditor(compilerSettings, "benchmarkResults202349.csv")
    benchmarkResultsEditor.splitSaveToDisk()


  def testLoopBlockToC(self):
    self.CBD.addBlock(ConstantBlock(block_name="TWO", value=2.0))
    self.CBD.addBlock(ConstantBlock(block_name="tht", value=13.0))
    self.CBD.addBlock(ConstantBlock(block_name="TTWO", value=2.0))

    self.CBD.addBlock(ProductBlock(block_name="tx"))
    self.CBD.addBlock(NegatorBlock(block_name="mtx"))
    self.CBD.addBlock(AdderBlock(block_name="y"))
    self.CBD.addBlock(NegatorBlock(block_name="z"))
    self.CBD.addBlock(AdderBlock(block_name="x"))

    self.CBD.addConnection("TWO", "tx")
    self.CBD.addConnection("tx", "mtx")
    self.CBD.addConnection("tht", "y")
    self.CBD.addConnection("mtx", "y")
    self.CBD.addConnection("y", "z")
    self.CBD.addConnection("y", "x")
    self.CBD.addConnection("TTWO", "x")
    self.CBD.addConnection("x", "tx")

    self._run(1) # Need to run one step only

    parallelCompiler = CBDParallelCompiler(25000000, self.CBD, "outputParallelLoopSource1", USE_RANDOM_CONSTANTS, 4, 4, 5.0)

    # Start the execution
    parallelCompiler.toDisk()

    # Disable for now
    # self.splitBenchmarkResultsCsv(parallelCompiler.getCompilerSettings())

  def testNonLoopBlockToC(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=3.3))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=12.0))
    self.CBD.addBlock(ProductBlock(block_name="p"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(NegatorBlock(block_name="n2"))

    self.CBD.addConnection("c1", "p")
    self.CBD.addConnection("c2", "p")
    self.CBD.addConnection("p", "a1")
    self.CBD.addConnection("c3", "a1")
    self.CBD.addConnection("a1", "n1")
    self.CBD.addConnection("n1", "n2")

    self._run(1) # Need to run one step only

    parallelCompiler = CBDParallelCompiler(5000000, self.CBD, "outputParallelSource1", USE_RANDOM_CONSTANTS, 4, 28.5)

    # Start the execution
    parallelCompiler.toDisk()

  def testNonLoopBlockToC2(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=3.3))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=5))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=12))
    self.CBD.addBlock(ProductBlock(block_name="p"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(ConstantBlock(block_name="c4", value=-2))
    self.CBD.addBlock(NegatorBlock(block_name="n2"))
    self.CBD.addBlock(InverterBlock(block_name="i1"))

    self.CBD.addConnection("c1", "p")
    self.CBD.addConnection("c2", "p")
    self.CBD.addConnection("p", "a1")
    self.CBD.addConnection("c3", "a1")
    self.CBD.addConnection("a1", "n1")
    self.CBD.addConnection("c4", "n2")
    self.CBD.addConnection("n2", "i1")

    self._run(1)

    # Compile
    parallelCompiler = CBDParallelCompiler(52000000, self.CBD, "outputParallelSource2", USE_RANDOM_CONSTANTS, 4, 4, 0.5)
    parallelCompiler.toDisk()

  def testNonLoopBlockToC3(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=3.0))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c4", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c5", value=5.0))

    self.CBD.addBlock(DelayBlock(block_name="d"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(AdderBlock(block_name="a2"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(AdderBlock(block_name="a3"))
    self.CBD.addBlock(ProductBlock(block_name="m1"))
    self.CBD.addBlock(ProductBlock(block_name="ml1"))
    self.CBD.addBlock(AdderBlock(block_name="a4"))
    self.CBD.addBlock(RootBlock(block_name="r1"))
    self.CBD.addBlock(InverterBlock(block_name="i1"))
    self.CBD.addBlock(GenericBlock(block_name="g1", block_operator="fabs"))

    self.CBD.addConnection("c2", "d") # Delay d
    self.CBD.addConnection("c1", "d", input_port_name="IC") # Delay d
    self.CBD.addConnection("d", "a1") # Adder a1
    self.CBD.addConnection("c3", "a1") # Adder a1
    self.CBD.addConnection("c4", "a2") # Adder a2
    self.CBD.addConnection("c5", "a2") # Adder a2
    self.CBD.addConnection("a2", "n1") # Negator
    self.CBD.addConnection("a2", "a3") # Adder a3
    self.CBD.addConnection("c5", "a3") # Adder a3
    self.CBD.addConnection("a1", "m1") # Product
    self.CBD.addConnection("n1", "m1") # Product
    self.CBD.addConnection("m1", "ml1") # ProductBlock
    self.CBD.addConnection("a2", "ml1") # ProductBlock
    self.CBD.addConnection("c1", "a4") # Adder
    self.CBD.addConnection("c2", "a4") # Adder
    self.CBD.addConnection("a4", "r1") # Root
    self.CBD.addConnection("c2", "r1") # Root
    self.CBD.addConnection("r1", "i1") # Inverter
    self.CBD.addConnection("n1", "g1") # Generic block with the math.abs function

    self._run(1)
    # Compile
    parallelCompiler = CBDParallelCompiler(5000000, self.CBD, "outputParallelSource3", USE_RANDOM_CONSTANTS, 4, 4, 10.0)
    parallelCompiler.toDisk()

  def testNonLoopBlockToC4(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=12.0))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=3.3))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c4", value=12.0))
    self.CBD.addBlock(ConstantBlock(block_name="c5", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c6", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c7", value=12.0))
    self.CBD.addBlock(ConstantBlock(block_name="c8", value=5.0))

    self.CBD.addBlock(DelayBlock(block_name="d"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(AdderBlock(block_name="a2"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(AdderBlock(block_name="a3"))
    self.CBD.addBlock(ProductBlock(block_name="m1"))
    self.CBD.addBlock(ProductBlock(block_name="ml1"))
    self.CBD.addBlock(AdderBlock(block_name="a4"))
    self.CBD.addBlock(RootBlock(block_name="r1"))
    self.CBD.addBlock(InverterBlock(block_name="i1"))
    self.CBD.addBlock(GenericBlock(block_name="g1", block_operator="fabs"))
    self.CBD.addBlock(AdderBlock(block_name="a6"))
    self.CBD.addBlock(InverterBlock(block_name="i2"))
    self.CBD.addBlock(ProductBlock(block_name="m2"))
    self.CBD.addBlock(ProductBlock(block_name="m3"))

    self.CBD.addConnection("c2", "d") # Delay d
    self.CBD.addConnection("c1", "d", input_port_name="IC") # Delay d
    self.CBD.addConnection("d", "a1") # Adder a1
    self.CBD.addConnection("c3", "a1") # Adder a1
    self.CBD.addConnection("c4", "a2") # Adder a2
    self.CBD.addConnection("c5", "a2") # Adder a2
    self.CBD.addConnection("a2", "n1") # Negator
    self.CBD.addConnection("a2", "a3") # Adder a3
    self.CBD.addConnection("c5", "a3") # Adder a3
    self.CBD.addConnection("a1", "m1") # Product
    self.CBD.addConnection("n1", "m1") # Product
    self.CBD.addConnection("m1", "ml1") # ProductBlock
    self.CBD.addConnection("a2", "ml1") # ProductBlock
    self.CBD.addConnection("c1", "a4") # Adder
    self.CBD.addConnection("c2", "a4") # Adder
    self.CBD.addConnection("a4", "r1") # Root
    self.CBD.addConnection("c2", "r1") # Root
    self.CBD.addConnection("r1", "i1") # Inverter
    self.CBD.addConnection("n1", "g1") # Generic block with the math.abs function
    self.CBD.addConnection("g1", "a6") # Adder a6
    self.CBD.addConnection("c6", "a6") # Adder a6
    self.CBD.addConnection("c7", "i2") # Inverter
    self.CBD.addConnection("i2", "m2") # ProductBlock
    self.CBD.addConnection("a6", "m2") # ProductBlock

    self.CBD.addConnection("c8", "m3") # ProductBlock
    self.CBD.addConnection("m2", "m3") # ProductBlock

    self._run(1)
    # Compile
    parallelCompiler = CBDParallelCompiler(5000000, self.CBD, "outputParallelSource4", USE_RANDOM_CONSTANTS, 4, 4, 9.166666)
    parallelCompiler.toDisk()


if __name__ == '__main__':
    # When this module is executed from the command-line, run all its tests
    unittest.main(verbosity=2)









