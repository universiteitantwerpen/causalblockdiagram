#!/usr/bin/env python
#
# Unit tests for all the basic CBD blocks, discrete-time CBD.

import unittest

from CBDMultipleOutput.Source.CBDParallelCompiler import *
from CBDMultipleOutput.Source.CBD import *
from CBDMultipleOutput.Source.plot import ScopeWindow

NUM_DISCR_TIME_STEPS = 50

class BasicCBDTestCase(unittest.TestCase):
  def setUp(self):
    self.CBD = CBD("CBD_for_block_under_test")

  def _run(self, num_steps=1, delta_t = 1.0):
    self.CBD.run(num_steps, delta_t)

  def _getSignal(self, blockname, output_port = None):
    foundBlocks = [ block for block in self.CBD.getBlocks() if block.getBlockName() == blockname ]
    numFoundBlocks = len(foundBlocks)
    if numFoundBlocks == 1:
      signal =  foundBlocks[0].getSignal(name_output = output_port)
      return [x.value for x in signal]
    else:
      raise Exception(str(numFoundBlocks) + " blocks with name " + blockname + " found.\nExpected a single block.")

  def testIntegratorBlock2(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=6.0))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=0.0))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=0.001))
    self.CBD.addBlock(IntegratorBlock(block_name="int"))

    # self.CBD.addConnection("c3", "int", input_port_name="delta_t")
    # self.CBD.addConnection("c3", "int")
    self.CBD.addConnection("c2", "int")
    # self.CBD.addConnection("c1", "a")
    # self.CBD.addConnection("d", "a")
    # self.CBD.addConnection("a", "d")
    # self.CBD.addConnection("c2", "d", input_port_name="IC")
    self.CBD.run(5, 0.01)
    # self.assertEquals(self._getSignal("int"), [0.0, 0.012, 0.018000000000000002, 0.024, 0.029999999999999999])

  def testDerivatorBlock2(self):
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=6.0))
    self.CBD.addBlock(ConstantBlock(block_name="c4", value=0.0))
    self.CBD.addBlock(DerivatorBlock(block_name="der1"))

    # self.CBD.addConnection("c3", "int", input_port_name="delta_t")
    # self.CBD.addConnection("c3", "int")
    self.CBD.addConnection("c3", "der1")
    # self.CBD.addConnection("c1", "a")
    # self.CBD.addConnection("d", "a")
    # self.CBD.addConnection("a", "d")
    # self.CBD.addConnection("c2", "d", input_port_name="IC")
    self.CBD.run(5, 0.01)
    # self.assertEquals(self._getSignal("int"), [0.0, 0.012, 0.018000000000000002, 0.024, 0.029999999999999999])

  def testNonLoopBlock(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=3.3))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=5))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=12))
    # self.CBD.addBlock(AdderBlock(block_name="a2"))
    # self.CBD.addBlock(AdderBlock(block_name="a3"))
    self.CBD.addBlock(ProductBlock(block_name="p"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(NegatorBlock(block_name="n"))

    self.CBD.addConnection("c1", "p")
    self.CBD.addConnection("c2", "p")
    self.CBD.addConnection("p", "a1")
    self.CBD.addConnection("c3", "a1")
    self.CBD.addConnection("a1", "n")

    # self.CBD.addConnection("a1", "n")
    # self.CBD.addConnection("c3", "a2")
    # self.CBD.addConnection("p", "a2")
    # self.CBD.addConnection("c1", "a1")
    # self.CBD.addConnection("p", "a1")
    # self.CBD.addConnection("a2", "n")
    # self.CBD.addConnection("a1", "a3")
    # self.CBD.addConnection("n", "a3")

    self._run(NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c1"), [3.3] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c2"), [5] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c3"), [12] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("p"), [16.5] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("n"), [-28.5] * NUM_DISCR_TIME_STEPS)

    ScopeWindow([self._getSignal("c1"), self._getSignal("p"), self._getSignal("n")],
    ["c1", "p", "n"] )

    # self.assertEquals(self._getSignal("a3"), [-8.3] * NUM_DISCR_TIME_STEPS)

    print "Result"
    print self._getSignal("p")

  def testNonLoopBlockToC(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=3.3))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=5))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=12))
    # self.CBD.addBlock(AdderBlock(block_name="a2"))
    # self.CBD.addBlock(AdderBlock(block_name="a3"))
    self.CBD.addBlock(ProductBlock(block_name="p"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))

    self.CBD.addConnection("c1", "p")
    self.CBD.addConnection("c2", "p")
    self.CBD.addConnection("p", "a1")
    self.CBD.addConnection("c3", "a1")
    self.CBD.addConnection("a1", "n1")

    # self.CBD.addConnection("a1", "n")
    # self.CBD.addConnection("c3", "a2")
    # self.CBD.addConnection("p", "a2")
    # self.CBD.addConnection("c1", "a1")
    # self.CBD.addConnection("p", "a1")
    # self.CBD.addConnection("a2", "n")
    # self.CBD.addConnection("a1", "a3")
    # self.CBD.addConnection("n", "a3")

    self._run(NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c1"), [3.3] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c2"), [5] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c3"), [12] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("p"), [16.5] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("n1"), [-28.5] * NUM_DISCR_TIME_STEPS)

    cbdCompiler = CBDCompiler(NUM_DISCR_TIME_STEPS, self.CBD)

    # Start the execution
    cbdCompiler.toDisk()

    ScopeWindow([self._getSignal("c1"), self._getSignal("p"), self._getSignal("n1")],
    ["c1", "p", "n1"] )

  def testCounter(self):
    self.CBD.addBlock(ConstantBlock(block_name="c0", value=0.0))
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=1.0))
    self.CBD.addBlock(DelayBlock(block_name="d1"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))

    self.CBD.addConnection("c0", "d1", input_port_name="IC")
    self.CBD.addConnection("d1", "a1")
    self.CBD.addConnection("c1", "a1")
    self.CBD.addConnection("a1", "d1")

    self._run(3)
    self.assertEquals(self._getSignal("d1"), [0.0, 1.0, 2.0])

  def testIntegrator2(self):
    CBDLittle1 = CBD("first_child", output_ports = ["outCBD1"])

    CBDLittle1.addBlock(ConstantBlock(block_name="c1", value=2.0))
    CBDLittle1.addBlock(NegatorBlock(block_name="n1"))
    CBDLittle1.addConnection("c1", "n1")
    CBDLittle1.addConnection("n1", "outCBD1")

    self.CBD.addBlock(CBDLittle1)
    self.CBD.addBlock(NegatorBlock(block_name="n2"))

    self.CBD.addConnection("CBDLittle1", "n2")

    self.CBD.flatten()
    # CBDLittle1.run(3)
    self._run(3)
    self.assertEquals(self.CBD.getSignal("first_child.outCBD1"), [2.0]*3)


    # self.CBD.addBlock(ConstantBlock(block_name="c0", value=3.0)) # IC
    # self.CBD.addBlock(ConstantBlock(block_name="c1", value=1.0)) # Dt
    # self.CBD.addBlock(ConstantBlock(block_name="c2", value=1.0)) # X approx
    # self.CBD.addBlock(ProductBlock(block_name="p1"))
    # self.CBD.addBlock(AdderBlock(block_name="a1"))
    # self.CBD.addBlock(DelayBlock(block_name="d1"))
    #
    # self.CBD.addConnection("c0", "d1", input_port_name="IC")
    # self.CBD.addConnection("a1", "d1")
    #
    # self.CBD.addConnection("c2", "p1")
    # self.CBD.addConnection("c1", "p1")
    # self.CBD.addConnection("p1", "a1")
    # self.CBD.addConnection("d1", "a1")

  def testExtDerivator(self):
    self.CBD.addBlock(ConstantBlock(block_name="c0", value=0.0)) # IC
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=1.0)) # Dt
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=3.0)) # X approx
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(ProductBlock(block_name="p1"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(DelayBlock(block_name="d1"))
    self.CBD.addBlock(NegatorBlock(block_name="n2"))
    self.CBD.addBlock(AdderBlock(block_name="a2"))
    self.CBD.addBlock(ProductBlock(block_name="p2"))
    self.CBD.addBlock(InverterBlock(block_name="i1"))

    self.CBD.addConnection("c1", "n1")
    self.CBD.addConnection("n1", "p1")
    self.CBD.addConnection("c0", "p1")
    self.CBD.addConnection("p1", "a1")
    self.CBD.addConnection("c2", "a1")
    self.CBD.addConnection("a1", "d1", input_port_name="IC")
    self.CBD.addConnection("c2", "d1")
    self.CBD.addConnection("d1", "n2")
    self.CBD.addConnection("c2", "a2")
    self.CBD.addConnection("n2", "a2")

    self.CBD.addConnection("c1", "i1")
    self.CBD.addConnection("i1", "p2")
    self.CBD.addConnection("a2", "p2")

    self._run(3)
    self.assertEquals(self._getSignal("c2"), [3.0, 3.0, 3.0])
    self.assertEquals(self._getSignal("n1"), [-1.0, -1.0, -1.0])
    self.assertEquals(self._getSignal("p2"), [0.0, 0.0, 0.0])

  def testExtIntegrator(self):
    self.CBD.addBlock(ConstantBlock(block_name="c0", value=3.0)) # IC
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=1.0)) # Dt
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=3.0)) # X approx
    self.CBD.addBlock(ProductBlock(block_name="p1"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(DelayBlock(block_name="d1"))

    self.CBD.addConnection("c0", "d1", input_port_name="IC")
    self.CBD.addConnection("a1", "d1")

    self.CBD.addConnection("c2", "p1")
    self.CBD.addConnection("c1", "p1")
    self.CBD.addConnection("p1", "a1")
    self.CBD.addConnection("d1", "a1")

    self._run(3)
    self.assertEquals(self._getSignal("p1"), [3.0, 3.0, 3.0])
    self.assertEquals(self._getSignal("a1"), [3.0, 3.0, 3.0])

  def testEquation1(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=2.0))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=4.0))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=1.0))
    self.CBD.addBlock(ProductBlock(block_name="p1"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(NegatorBlock(block_name="n2"))
    self.CBD.addBlock(AdderBlock(block_name="a2"))

    self.CBD.addConnection("c1", "p1")
    self.CBD.addConnection("a2", "p1")
    self.CBD.addConnection("p1", "n1")
    self.CBD.addConnection("n1", "a1")
    self.CBD.addConnection("c2", "a1")
    self.CBD.addConnection("a1", "a2")
    self.CBD.addConnection("c3", "n2")
    self.CBD.addConnection("n2", "a2")

    self._run(3)
    self.assertEquals(self._getSignal("p1"), [2.0, 2.0, 2.0])
    self.assertEquals(self._getSignal("n1"), [-2.0, -2.0, -2.0])
    self.assertEquals(self._getSignal("a1"), [2.0, 2.0, 2.0])
    self.assertEquals(self._getSignal("a2"), [1.0, 1.0, 1.0])

  def testExt6Delays(self):
    self.CBD.addBlock(ConstantBlock(block_name="c0", value=3.3))
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=5))
    self.CBD.addBlock(DelayBlock(block_name="d1"))
    self.CBD.addBlock(DelayBlock(block_name="d2"))
    self.CBD.addBlock(DelayBlock(block_name="d3"))
    self.CBD.addBlock(DelayBlock(block_name="d4"))
    self.CBD.addBlock(DelayBlock(block_name="d5"))
    self.CBD.addBlock(DelayBlock(block_name="d6"))

    self.CBD.addConnection("c0", "d1", input_port_name="IC")
    self.CBD.addConnection("c1", "d1")
    self.CBD.addConnection("c0", "d2", input_port_name="IC")
    self.CBD.addConnection("d1", "d2")
    self.CBD.addConnection("c0", "d3", input_port_name="IC")
    self.CBD.addConnection("d2", "d3")
    self.CBD.addConnection("c0", "d4", input_port_name="IC")
    self.CBD.addConnection("d3", "d4")
    self.CBD.addConnection("c0", "d5", input_port_name="IC")
    self.CBD.addConnection("d4", "d5")
    self.CBD.addConnection("c0", "d6", input_port_name="IC")
    self.CBD.addConnection("d5", "d6")

    self._run(5)
    self.assertEquals(self._getSignal("c1"), [5.0] * 5)
    self.assertEquals(self._getSignal("d1"), [3.3, 5.0, 5.0, 5.0, 5.0])
    self.assertEquals(self._getSignal("d2"), [3.3, 3.3, 5.0, 5.0, 5.0])
    self.assertEquals(self._getSignal("d3"), [3.3, 3.3, 3.3, 5.0, 5.0])
    self.assertEquals(self._getSignal("d4"), [3.3, 3.3, 3.3, 3.3, 5.0])
    self.assertEquals(self._getSignal("d5"), [3.3] * 5)
    self.assertEquals(self._getSignal("d6"), [3.3] * 5)

  def testNonLoopBlockToC2(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=3.3))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=5))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=12))
    self.CBD.addBlock(ProductBlock(block_name="p"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(ConstantBlock(block_name="c4", value=-2))
    self.CBD.addBlock(NegatorBlock(block_name="n2"))
    self.CBD.addBlock(InverterBlock(block_name="i1"))

    self.CBD.addConnection("c1", "p")
    self.CBD.addConnection("c2", "p")
    self.CBD.addConnection("p", "a1")
    self.CBD.addConnection("c3", "a1")
    self.CBD.addConnection("a1", "n1")
    self.CBD.addConnection("c4", "n2")
    self.CBD.addConnection("n2", "i1")

    self._run(NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c1"), [3.3] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c2"), [5] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("c3"), [12] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("p"), [16.5] * NUM_DISCR_TIME_STEPS)
    self.assertEquals(self._getSignal("n1"), [-28.5] * NUM_DISCR_TIME_STEPS)

    # Compile
    cbdCompiler = CBDCompiler(NUM_DISCR_TIME_STEPS, self.CBD)
    cbdCompiler.toDisk("output2.c")

    # Plot
    ScopeWindow([self._getSignal("c1"), self._getSignal("p"), self._getSignal("n1"), self._getSignal("i1")],
      ["c1", "p", "n1", "i1"] )


  def testkExtComplex1(self):
    self.CBD.addBlock(ConstantBlock(block_name="c1", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c2", value=3.0))
    self.CBD.addBlock(ConstantBlock(block_name="c3", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c4", value=5.0))
    self.CBD.addBlock(ConstantBlock(block_name="c5", value=5.0))
    self.CBD.addBlock(DelayBlock(block_name="d"))
    self.CBD.addBlock(AdderBlock(block_name="a1"))
    self.CBD.addBlock(AdderBlock(block_name="a2"))
    self.CBD.addBlock(NegatorBlock(block_name="n1"))
    self.CBD.addBlock(AdderBlock(block_name="a3"))
    self.CBD.addBlock(ProductBlock(block_name="m1"))
    self.CBD.addBlock(ModuloBlock(block_name="ml1"))
    self.CBD.addBlock(AdderBlock(block_name="a4"))
    self.CBD.addBlock(RootBlock(block_name="r1"))
    self.CBD.addBlock(InverterBlock(block_name="i1"))
    self.CBD.addBlock(GenericBlock(block_name="g1", block_operator="fabs"))

    self.CBD.addConnection("c2", "d") # Delay d
    self.CBD.addConnection("c1", "d", input_port_name="IC") # Delay d
    self.CBD.addConnection("d", "a1") # Adder a1
    self.CBD.addConnection("c3", "a1") # Adder a1
    self.CBD.addConnection("c4", "a2") # Adder a2
    self.CBD.addConnection("c5", "a2") # Adder a2
    self.CBD.addConnection("a2", "n1") # Negator
    self.CBD.addConnection("a2", "a3") # Adder a3
    self.CBD.addConnection("c5", "a3") # Adder a3
    self.CBD.addConnection("a1", "m1") # Product
    self.CBD.addConnection("n1", "m1") # Product
    self.CBD.addConnection("m1", "ml1") # Modulo
    self.CBD.addConnection("a2", "ml1") # Modulo
    self.CBD.addConnection("c1", "a4") # Adder
    self.CBD.addConnection("c2", "a4") # Adder
    self.CBD.addConnection("a4", "r1") # Root
    self.CBD.addConnection("c2", "r1") # Root
    self.CBD.addConnection("r1", "i1") # Inverter
    self.CBD.addConnection("n1", "g1") # Generic block with the math.abs function

    self._run(4)
    self.assertEquals(self._getSignal("d"), [5.0, 3.0, 3.0, 3.0])
    self.assertEquals(self._getSignal("a1"), [10.0, 8.0, 8.0, 8.0])
    self.assertEquals(self._getSignal("a2"), [10.0, 10.0, 10.0, 10.0])
    self.assertEquals(self._getSignal("n1"), [-10.0, -10.0, -10.0, -10.0])
    self.assertEquals(self._getSignal("a3"), [15.0, 15.0, 15.0, 15.0])
    self.assertEquals(self._getSignal("m1"), [-100.0, -80.0, -80.0, -80.0])
    self.assertEquals(self._getSignal("ml1"), [0.0, 0.0, 0.0, 0.0])
    self.assertEquals(self._getSignal("a4"), [8.0, 8.0, 8.0, 8.0])
    self.assertEquals(self._getSignal("r1"), [2.0, 2.0, 2.0, 2.0])
    self.assertEquals(self._getSignal("i1"), [0.5, 0.5, 0.5, 0.5])
    self.assertEquals(self._getSignal("g1"), [10.0, 10.0, 10.0, 10.0])

def suite():
    """Returns a test suite containing all the test cases in this module."""
    suite = unittest.makeSuite(BasicCBDTestCase)

    return unittest.TestSuite((suite))

if __name__ == '__main__':
    # When this module is executed from the command-line, run all its tests
    unittest.main(verbosity=2)









