/* Some standard libs that are useful for you */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Your code generator changes the nr of signals based on your model*/

#define M 9 /*nr of signals*/

/* Hash defines for signal names. This is better for understanding the generated code*/

#define C1 result[0]
#define C2 result[1]
#define C3 result[2]
#define P result[3]
#define A1 result[4]
#define N1 result[5]
#define C4 result[6]
#define N2 result[7]
#define I1 result[8]

/* Specify the number of iterations you simulate */

#define N 10 /*nr of iterations*/

/* This code implements the solver for algebraic loops (gaussian elimination). Use this as a template in your own solution*/

#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))

void swap_row(double *a, double *b, int r1, int r2, int n)
{
    double tmp, *p1, *p2;
    int i;
    
    if (r1 == r2) return;
    for (i = 0; i < n; i++) {
        p1 = mat_elem(a, r1, i, n);
        p2 = mat_elem(a, r2, i, n);
        tmp = *p1, *p1 = *p2, *p2 = tmp;
    }
    tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;
}

/*///////////////////////////////////////////////////////////////////////////
 // Input for the solver is of the form: 
 // double a[] = {C11, C12, C13,
 //               C21, C22, C23,
 //               C31, C32, C33};
 // double b[] = {C1, C2, C3};
 // double x[3];
 //
 //
 // represents the following linear equation system:
 // {
 //   C11*x + C12*y + C13*z = C1
 //   C21*x + C22*y + C23*z = C2
 //   C31*x + C32*y + C33*z = C3
 // }
 // where x, y, and x are signals (block outputs) inside a strong component.
 // call:
 // gauss_eliminate(a,b,x,3);
 ///////////////////////////////////////////////////////////////////////////*/

void gauss_eliminate(double *a, double *b, double *x, int n)
{
#define A(y, x) (*mat_elem(a, y, x, n))
    int i, j, col, row, max_row,dia;
    double max, tmp;
    
    for (dia = 0; dia < n; dia++) {
        max_row = dia, max = A(dia, dia);
        
        for (row = dia + 1; row < n; row++)
        if ((tmp = fabs(A(row, dia))) > max)
        max_row = row, max = tmp;
        
        swap_row(a, b, dia, max_row, n);
        
        for (row = dia + 1; row < n; row++) {
            tmp = A(row, dia) / A(dia, dia);
            for (col = dia+1; col < n; col++)
            A(row, col) -= tmp * A(dia, col);
            A(row, dia) = 0;
            b[row] -= tmp * b[dia];
        }
    }
    for (row = n - 1; row >= 0; row--) {
        tmp = b[row];
        for (j = n - 1; j > row; j--)
        tmp -= x[j] * A(row, j);
        x[row] = tmp / A(row, row);
    }
#undef A
}

/* The code for the gaussian elimination solver ends here.*/

int main(int argc, char *argv[]){

    /*The implementation of your CBD model starts here. The example shows a simple model with 2 constant blocks, an add block and a product block. If there are linear algebraic loops in your model, you need to call the gaussian elimination solver.*/

    int i;
    double result[M][N]; /* the signal values are stored here: M is output of block, N are the number of iterations of your CBD simulation. Here we have a 4 by 100 matrix to store the results of our simulation */
    char *names[M] = {"c1", "c2", "c3", "p", "a1", "n1", "c4", "n2", "i1" };
    /* First iteration */
    C1[0] = 3.3;
    C2[0] = 5;
    C3[0] = 12;
    P[0] = C1[0] * C2[0];
    A1[0] = P[0] + C3[0];
    N1[0] = - 1.0 * A1[0];
    C4[0] = -2;
    N2[0] = - 1.0 * C4[0];
    I1[0] = 1.0 / N2[0];
    /* Other iterations */
    for (i=1; i<N; i++){
        C1[i] = 3.3;
        C2[i] = 5;
        C3[i] = 12;
        P[i] = C1[i] * C2[i];
        A1[i] = P[i] + C3[i];
        N1[i] = - 1.0 * A1[i];
        C4[i] = -2;
        N2[i] = - 1.0 * C4[i];
        I1[i] = 1.0 / N2[i];
    }
}
