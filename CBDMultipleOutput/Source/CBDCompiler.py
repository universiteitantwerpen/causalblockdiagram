__author__ = 'madks_000'

from CBD import *

class CBDCompiler:
    """
    A class that generates ansi c code by walking a sorted graph list
    """
    def __init__(self, iterations, cbd):

        self.__iterations = 0
        self.__signalCount = 0
        self.__returningStringList = []
        self.__blockNamesList = []

        self.setIterations(iterations)
        self.setCBD(cbd)
        self.setSignalCount(len(self.__cbd.getBlocks()))

        self.setBlockNames()

    def setIterations(self, iterations):
        self.__iterations = iterations

    def getIterations(self):
        return self.__iterations

    def setSignalCount(self, signalCount):
        self.__signalCount = signalCount

    def getSignalCount(self):
        return self.__signalCount

    def setSortedGraph(self, sortedGraph):
        self.__sortedGraph = sortedGraph

    def getSortedGraph(self):
        return self.__sortedGraph

    def setCBD(self, cbd):
        self.__cbd = cbd

    def dumpLibraries(self):
        self.__returningStringList.append("/* Some standard libs that are useful for you */\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("#include <stdio.h>\n")
        self.__returningStringList.append("#include <stdlib.h>\n")
        self.__returningStringList.append("#include <math.h>\n")
        self.__returningStringList.append("\n")

    def dumpSignals(self):
        self.__returningStringList.append("/*Your code generator changes the nr of signals based on your model*/\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("#define M %d /*nr of signals*/\n" % self.getSignalCount())

    def dumpIterations(self):
        self.__returningStringList.append("\n")
        self.__returningStringList.append("/* Specify the number of iterations you simulate */\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("#define N %d /*nr of iterations*/\n" % self.getIterations())

    def dumpSignalNameHashes(self):
        self.__returningStringList.append("\n")
        self.__returningStringList.append("/* Hash defines for signal names. This is better for understanding the generated code*/\n")
        self.__returningStringList.append("\n")

        # Loop through all the names
        for i, hashSignalName in enumerate(self.__blockNamesList):
            self.__returningStringList.append("#define %s result[%d]\n" % (hashSignalName.upper(), i))

    def dumpSolver(self):
        self.__returningStringList.append("\n")
        self.__returningStringList.append("/* This code implements the solver for algebraic loops (gaussian elimination). Use this as a template in your own solution*/\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("void swap_row(double *a, double *b, int r1, int r2, int n)\n")
        self.__returningStringList.append("{\n")
        self.__returningStringList.append("    double tmp, *p1, *p2;\n")
        self.__returningStringList.append("    int i;\n")
        self.__returningStringList.append("    \n")
        self.__returningStringList.append("    if (r1 == r2) return;\n")
        self.__returningStringList.append("    for (i = 0; i < n; i++) {\n")
        self.__returningStringList.append("        p1 = mat_elem(a, r1, i, n);\n")
        self.__returningStringList.append("        p2 = mat_elem(a, r2, i, n);\n")
        self.__returningStringList.append("        tmp = *p1, *p1 = *p2, *p2 = tmp;\n")
        self.__returningStringList.append("    }\n")
        self.__returningStringList.append("    tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;\n")
        self.__returningStringList.append("}\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("/*///////////////////////////////////////////////////////////////////////////\n")
        self.__returningStringList.append(" // Input for the solver is of the form: \n")
        self.__returningStringList.append(" // double a[] = {C11, C12, C13,\n")
        self.__returningStringList.append(" //               C21, C22, C23,\n")
        self.__returningStringList.append(" //               C31, C32, C33};\n")
        self.__returningStringList.append(" // double b[] = {C1, C2, C3};\n")
        self.__returningStringList.append(" // double x[3];\n")
        self.__returningStringList.append(" //\n")
        self.__returningStringList.append(" //\n")
        self.__returningStringList.append(" // represents the following linear equation system:\n")
        self.__returningStringList.append(" // {\n")
        self.__returningStringList.append(" //   C11*x + C12*y + C13*z = C1\n")
        self.__returningStringList.append(" //   C21*x + C22*y + C23*z = C2\n")
        self.__returningStringList.append(" //   C31*x + C32*y + C33*z = C3\n")
        self.__returningStringList.append(" // }\n")
        self.__returningStringList.append(" // where x, y, and x are signals (block outputs) inside a strong component.\n")
        self.__returningStringList.append(" // call:\n")
        self.__returningStringList.append(" // gauss_eliminate(a,b,x,3);\n")
        self.__returningStringList.append(" ///////////////////////////////////////////////////////////////////////////*/\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("void gauss_eliminate(double *a, double *b, double *x, int n)\n")
        self.__returningStringList.append("{\n")
        self.__returningStringList.append("#define A(y, x) (*mat_elem(a, y, x, n))\n")
        self.__returningStringList.append("    int i, j, col, row, max_row,dia;\n")
        self.__returningStringList.append("    double max, tmp;\n")
        self.__returningStringList.append("    \n")
        self.__returningStringList.append("    for (dia = 0; dia < n; dia++) {\n")
        self.__returningStringList.append("        max_row = dia, max = A(dia, dia);\n")
        self.__returningStringList.append("        \n")
        self.__returningStringList.append("        for (row = dia + 1; row < n; row++)\n")
        self.__returningStringList.append("        if ((tmp = fabs(A(row, dia))) > max)\n")
        self.__returningStringList.append("        max_row = row, max = tmp;\n")
        self.__returningStringList.append("        \n")
        self.__returningStringList.append("        swap_row(a, b, dia, max_row, n);\n")
        self.__returningStringList.append("        \n")
        self.__returningStringList.append("        for (row = dia + 1; row < n; row++) {\n")
        self.__returningStringList.append("            tmp = A(row, dia) / A(dia, dia);\n")
        self.__returningStringList.append("            for (col = dia+1; col < n; col++)\n")
        self.__returningStringList.append("            A(row, col) -= tmp * A(dia, col);\n")
        self.__returningStringList.append("            A(row, dia) = 0;\n")
        self.__returningStringList.append("            b[row] -= tmp * b[dia];\n")
        self.__returningStringList.append("        }\n")
        self.__returningStringList.append("    }\n")
        self.__returningStringList.append("    for (row = n - 1; row >= 0; row--) {\n")
        self.__returningStringList.append("        tmp = b[row];\n")
        self.__returningStringList.append("        for (j = n - 1; j > row; j--)\n")
        self.__returningStringList.append("        tmp -= x[j] * A(row, j);\n")
        self.__returningStringList.append("        x[row] = tmp / A(row, row);\n")
        self.__returningStringList.append("    }\n")
        self.__returningStringList.append("#undef A\n")
        self.__returningStringList.append("}\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("/* The code for the gaussian elimination solver ends here.*/\n")

    def dumpMain(self):
        self.__returningStringList.append("\n")
        self.__returningStringList.append("int main(int argc, char *argv[]){\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("    /*The implementation of your CBD model starts here. The example shows a simple model with 2 constant blocks, an add block and a product block. If there are linear algebraic loops in your model, you need to call the gaussian elimination solver.*/\n")
        self.__returningStringList.append("\n")
        self.__returningStringList.append("    int i;\n")
        self.__returningStringList.append("    double result[M][N]; /* the signal values are stored here: M is output of block, N are the number of iterations of your CBD simulation. Here we have a 4 by 100 matrix to store the results of our simulation */\n")

        # Loop through all the signal names
        namesArray = "    char *names[M] = {"
        for i, hashSignalName in enumerate(self.__blockNamesList):
            if i > 0 :
                namesArray += ", "
            namesArray += "\"" + hashSignalName.lower() + "\""
        namesArray += " };\n"
        self.__returningStringList.append(namesArray)

        self.__returningStringList.append("    /* First iteration */\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()

            if currentBlockType == "ConstantBlock":
                self.__returningStringList.append("    %s[0] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))

            elif currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = block.getLinksIn()["IN1"][0].getBlockName()

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "- 1.0 * %s[0];" % inputString.upper()
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s[0];" % inputString.upper()
                elif currentBlockType == "GenericBlock":
                    operationString = "generic(%s[0]);" % inputString.upper()

                self.__returningStringList.append("    %s[0] = %s\n" % (currentBlockName.upper(), operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = block.getLinksIn()["IN1"][0].getBlockName()
                inputString2 = block.getLinksIn()["IN2"][0].getBlockName()

                operatorString = ""
                if currentBlockType == "ProductBlock":
                    operatorString = "*"
                elif currentBlockType == "AdderBlock":
                    operatorString = "+"
                elif currentBlockType == "ModuloBlock":
                    operatorString = "%"
                elif currentBlockType == "RootBlock":
                    operatorString = "pow"

                self.__returningStringList.append("    %s[0] = %s[0] %s %s[0];\n" % (currentBlockName.upper(), inputString1.upper(), operatorString , inputString2.upper()))


        self.__returningStringList.append("    /* Other iterations */\n")
        self.__returningStringList.append("    for (i=1; i<N; i++){\n")

        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__returningStringList.append("        %s[i] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[i + 1].value))

            elif currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = block.getLinksIn()["IN1"][0].getBlockName()

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "(- 1.0) * %s[i];" % inputString.upper()
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s[i];" % inputString.upper()
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s[i]);" % (block.getBlockOperator(), inputString.upper())

                self.__returningStringList.append("        %s[i] = %s\n" % (currentBlockName.upper(), operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = block.getLinksIn()["IN1"][0].getBlockName()
                inputString2 = block.getLinksIn()["IN2"][0].getBlockName()

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    self.__returningStringList.append("        %s[i] = pow(%s[i], %s[i]);\n" % (currentBlockName.upper(), inputString1.upper(), inputString2.upper()))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    self.__returningStringList.append("        %s[i] = %s[i] %s %s[i];\n" % (currentBlockName.upper(), inputString1.upper(), operatorString , inputString2.upper()))

        self.__returningStringList.append("    }\n")
        self.__returningStringList.append("}\n")

    def walk(self):

        # Clean the output list of lines
        self.__returningStringList = []

        # Dump the Standard libraries
        self.dumpLibraries()

        self.dumpSignals()

        self.dumpSignalNameHashes()

        self.dumpIterations()

        self.dumpSolver()

        self.dumpMain()

        return "".join(self.__returningStringList)

    def setBlockNames(self):
        for block in self.__cbd.getBlocks():
            self.__blockNamesList.append("%s" % (block.getBlockName()))

    def toDisk(self, fileName="output.c"):
        outputString = self.walk()
        text_file = open(fileName, "w")
        text_file.write(outputString)
        text_file.close()
        print "Done saving the output Ansi C code."
