#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>

// The number of total simulation steps
#define NELEMENTS 5000000


// Main execution path
int main (int argc, char* const *argv)
{
	clock_t start, end;

	// Create the host (local) variables
	unsigned int i = 0;
	unsigned int nElements = NELEMENTS;
    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	printf("============ SERIAL ANSI C =============\nNumber of discrete time steps: %d\n\n",nElements);
	printf("Using: Serial Ansi C & CPU\n");
	// Start the execution time counter for OpenMP
	start = clock();
	
	float *ansiC_C1 = (float*)malloc(sizeof(float)*nElements);
	float *ansiC_C2 = (float*)malloc(sizeof(float)*nElements);
	float *ansiC_C3 = (float*)malloc(sizeof(float)*nElements);
	float *ansiC_OUT = (float*)malloc(sizeof(float)*nElements);


	// Pack Ansi C variables with data
	for (i = 0; i < nElements; i++) {
		ansiC_C1[i] = 5.0;
		ansiC_C2[i] = 3.3;
		ansiC_C3[i] = 12.0;
	}

	// Initialize intermediate variables
	float ansiC_P = 0.0;
	float ansiC_A1 = 0.0;
	float ansiC_N1 = 0.0;
	float ansiC_N2 = 0.0;

	// Calculate data
	for (i = 0; i < nElements; i++) {

	    ansiC_P = 0.0;
	    ansiC_A1 = 0.0;
	    ansiC_N1 = 0.0;
	    ansiC_N2 = 0.0;

        ansiC_P = ansiC_C1[i] * ansiC_C2[i];
        ansiC_A1 = ansiC_P + ansiC_C3[i];
        ansiC_N1 = (- 1.0) * ansiC_A1;
        ansiC_N2 = (- 1.0) * ansiC_N1;
        ansiC_OUT[i] = ansiC_N2;
	}

	// Stop the ansi c clock
	end = clock();
	printf("Execution time(secs): %f\n\n", (end-start)/(double)CLOCKS_PER_SEC );


	// Validate the results -optional- should use if we already know the final results
	int success = 1;
	for (i = 0; i < nElements; i++) {  
		if ( ansiC_OUT[i] != 0.0) {
			success = 0;
			printf("ansiC_OUT = %f\n", ansiC_OUT[i]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "AnsiC,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelSource1,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", (end-start)/(double)CLOCKS_PER_SEC);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
