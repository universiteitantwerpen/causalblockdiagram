chmod +x *
make
gcc -o outputAnsiC.o outputAnsiC.c
gcc -o outputOpenGL.o outputOpenGL.c -framework GLUT -w -framework OpenGL
icc -ltbb outputIntelTBB.cpp -std=c++11 -ltbbmalloc -o outputIntelTBB.o -O3
icc -openmp outputOpenMP.c -o outputOpenMP.o -O3
icc -openmp outputOpenMPSections.c -o outputOpenMPSections.o -O3
