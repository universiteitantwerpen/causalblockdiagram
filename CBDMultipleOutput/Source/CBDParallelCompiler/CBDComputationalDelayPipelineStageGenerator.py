__author__ = 'ktheodorakos'

class CBDComputationalDelayPipelineStageGenerator:
    """
    A class that computes an as close to optimal Pipeline Stages schedule, given a
    Serial Schedule and a Computational Delay Dictionary
    """
    def __init__(self, cbd, computationalDelayDictionary={}, pipelineStages=4):
        self.__idealStageSize = 0
        self.__computationalDelaySum = 0
        self.__cbd = cbd
        self.__pipelineStages = pipelineStages
        self.__completeStagesList = []
        self.__bestPipelineStagesSchedule = []
        self.__computationalDelayDictionary = computationalDelayDictionary
        if len(self.__computationalDelayDictionary) is 0:
            self.setDefaultComputationalDelayDictionary()

    def setDefaultComputationalDelayDictionary(self):
        # Default values
        self.__computationalDelayDictionary["AdderBlock"] = 3
        self.__computationalDelayDictionary["NegatorBlock"] = 2
        self.__computationalDelayDictionary["IntegratorBlock"] = 5
        self.__computationalDelayDictionary["DerivatorBlock"] = 5
        self.__computationalDelayDictionary["RootBlock"] = 4
        self.__computationalDelayDictionary["GenericBlock"] = 5
        self.__computationalDelayDictionary["ModuloBlock"] = 3
        self.__computationalDelayDictionary["ProductBlock"] = 3
        self.__computationalDelayDictionary["ConstantBlock"] = 0
        self.__computationalDelayDictionary["DelayBlock"] = 1
        self.__computationalDelayDictionary["InverterBlock"] = 3

    def generateTaskScheduleList(self):

        self.__completeStagesList = []
        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockType = block.getBlockType()
            currentCost = 0
            if currentBlockType in self.__computationalDelayDictionary:
                currentCost = self.__computationalDelayDictionary.get(currentBlockType)
            else:
                currentCost = 0
            self.__completeStagesList.append([block, currentCost])

    def getTotalComputationalDelay(self, completeStagesList):
        sum = 0
        if len(completeStagesList) > 0:
            for block in completeStagesList:
                sum += block[1]
            return sum
        else:
            return -1

    def getIdealPipelineStageSize(self, stages):
        if stages > 0 and stages <= len(self.__cbd.getBlocks()):
            return self.getTotalComputationalDelay(self.__completeStagesList) / (stages * 1.0)
        else:
            # Guard triggered, erroneous stage size
            return -1

    def getPipelineScheduleSumSquares(self, stages, currentScheduleList):

        idealStageSize = self.getIdealPipelineStageSize(stages)
        sumList = []
        stage = 0
        cur_sum = 0

        for block in currentScheduleList:
            if block[2] == stage:
                cur_sum += block[1]
            else:
                sumList.append((cur_sum - idealStageSize) ** 2)
                stage = block[2]
                cur_sum = block[1]

        # Add the last sum
        sumList.append((cur_sum - idealStageSize) ** 2)

        sumOfSquares = sum(sumList)

        return sumOfSquares

    def getPipelineStageSchedule(self, stages, completeStagesList, greater, inverted=False):

        caseScheduleGreater = []

        idealStageSize = self.getIdealPipelineStageSize(stages)
        # Top -> bottom 1
        currentSum = 0
        nextSum = 0
        currentStage = 0

        if greater == True:
            if inverted == False:
                for block in completeStagesList:
                    currentSum += block[1]
                    if currentSum > idealStageSize: # If there are enough stages left
                        # Add the element first, then increase stage\
                        caseScheduleGreater.append([block[0], block[1], currentStage])
                        currentSum = 0 # Reset stage computational delay
                        if currentStage < (stages - 1):
                            currentStage += 1
                    else: # Just add the element in the current stage
                        caseScheduleGreater.append([block[0], block[1], currentStage])
            else:
                currentStage = stages - 1
                for block in completeStagesList:
                    currentSum += block[1]
                    if currentSum > idealStageSize: # If there are enough stages left
                        # Add the element first, then increase stage\
                        caseScheduleGreater.append([block[0], block[1], currentStage])
                        currentSum = 0 # Reset stage computational delay
                        if currentStage > 0:
                            currentStage -= 1
                    else: # Just add the element in the current stage
                        caseScheduleGreater.append([block[0], block[1], currentStage])
                caseScheduleGreater.reverse()
        else:
            if inverted == False:
                for block in completeStagesList:
                    nextSum = currentSum + block[1]
                    if nextSum > idealStageSize and currentStage < stages: # If there are enough stages left
                        # Add the element first, then increase stage
                        if currentStage < (stages - 1):
                            currentStage += 1
                        caseScheduleGreater.append([block[0], block[1], currentStage])
                        currentSum = block[1] # Reset stage computational delay
                    else: # Just add the element in the current stage
                        currentSum = nextSum
                        caseScheduleGreater.append([block[0], block[1], currentStage])
            else:
                currentStage = stages - 1
                for block in completeStagesList:
                    nextSum = currentSum + block[1]
                    if nextSum > idealStageSize and currentStage < stages: # If there are enough stages left
                        # Add the element first, then increase stage
                        if currentStage > 0:
                            currentStage -= 1
                        caseScheduleGreater.append([block[0], block[1], currentStage])
                        currentSum = block[1] # Reset stage computational delay
                    else: # Just add the element in the current stage
                        currentSum = nextSum
                        caseScheduleGreater.append([block[0], block[1], currentStage])
                caseScheduleGreater.reverse()

        return caseScheduleGreater

    def generateIdealPipelineStagesSchedule(self, stages):

        # Set the ideal stages size and the total computational delay
        self.__computationalDelaySum = self.getTotalComputationalDelay(self.__completeStagesList)
        self.__idealStageSize = self.getIdealPipelineStageSize(stages)

        if self.__computationalDelaySum is not -1 and self.__idealStageSize is not -1:

            allSchedules = []
            minSquares = 500000

            # Top -> bottom Greater than ideal stage size
            allSchedules.append(self.getPipelineStageSchedule(self.__pipelineStages, self.__completeStagesList, True))

            # Top -> bottom Less than ideal stage size
            allSchedules.append(self.getPipelineStageSchedule(self.__pipelineStages, self.__completeStagesList, False))

            # Bottom -> top Greater than ideal stage size
            allSchedules.append(self.getPipelineStageSchedule(self.__pipelineStages, self.__completeStagesList[::-1], True, True))

            # Bottom -> top Less than ideal stage size
            allSchedules.append(self.getPipelineStageSchedule(self.__pipelineStages, self.__completeStagesList[::-1], False, True))

            bestSchedule = []
            for currentSchedule in allSchedules:
                currentSquares = self.getPipelineScheduleSumSquares(self.__pipelineStages, currentSchedule)
                if  currentSquares < minSquares and self.__pipelineStages == (currentSchedule[-1][2] + 1):
                    bestSchedule = currentSchedule
                    minSquares = currentSquares

            self.__bestPipelineStagesSchedule = bestSchedule

    def getOptimizedPipelineStagesSchedule(self):

        self.generateTaskScheduleList()
        self.generateIdealPipelineStagesSchedule(self.__pipelineStages)

        return self.__bestPipelineStagesSchedule

    def getOptimizedPipelineStagesDictionary(self):

        # Generate dictionary with blockName -> stage
        stagesDictionary = {}
        for block in self.getOptimizedPipelineStagesSchedule():
            stagesDictionary[block[0].getBlockName()] = block[2]

        return stagesDictionary


    def testPrintAll(self):

        self.generateTaskScheduleList()

        for block in self.__completeStagesList:
            print("%s: %d") % (block[0].getBlockType(), block[1])

        print ("Computational Delay Sum: %s") % (str(self.getTotalComputationalDelay(self.__completeStagesList)))
        print ("Ideal stage size: %s") % (str(self.getIdealPipelineStageSize(self.__pipelineStages)))

        self.generateIdealPipelineStagesSchedule(self.__pipelineStages)

        for block in self.__bestPipelineStagesSchedule:
            print("Stage %d: [%s, cost: %d]") % (block[2], block[0].getBlockType(), block[1])

        # Generate dictionary with blockName -> stage
        print self.getOptimizedPipelineStagesDictionary()

