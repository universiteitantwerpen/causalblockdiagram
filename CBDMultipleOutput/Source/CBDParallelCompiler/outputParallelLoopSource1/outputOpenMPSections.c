/*
	OpenMP output src code from the CBD model simulation for mac
	applying data/domain decomposition
	Do check the environment variables first (i.e):
	export DYLD_LIBRARY_PATH="/opt/intel/lib" // for Intel C++ open mp compiler for Mac
	export OMP_NUM_THREADS=4 // The number of maximum threads
	export OMP_STACKSIZE="50M" // Small stack size causes fragmentation errors, increase accordingly
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <omp.h>

// The number of total simulation steps
#define NELEMENTS 25000000
// The number of maxinum spawning threads
#define MAX_THREADS 4

// Main execution path
int main (int argc, char* const *argv)
{
	clock_t start, end;
	
	unsigned int nElements = NELEMENTS;
    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	// OpenMP variables: number of total threads, current thread id, min I item, max I item
	int currentThreadId;
	int maxThreads = MAX_THREADS;
	unsigned int minI, maxI;
	
	// Enforce the number of maximum threads
	omp_set_num_threads(maxThreads);
	
	// Job size/number of iterations for each thread
	unsigned int threadJobSize = nElements / maxThreads;
	
	printf("============= PIPELINE CPU =============\nNumber of discrete time steps: %d\n\n",nElements);	
	
	printf("Using: OpenMP & Optimized Pipeline\n");
	printf("Number of threads: %d\n", maxThreads);	
	printf("Number of Stages: %d\n", maxThreads);	

	// Start the execution time counter for OpenCL
	start = clock();
	
	// Create the input/constant variables
	unsigned int i = 0;
	float *openMP_TWO = (float*)malloc(sizeof(float)*nElements);
	float *openMP_THT = (float*)malloc(sizeof(float)*nElements);
	float *openMP_TTWO = (float*)malloc(sizeof(float)*nElements);
	float *openMP_OUT = (float*)malloc(sizeof(float)*nElements);

	// Pack input variables with data
	for (i = 0; i < nElements; i++) {
		openMP_TWO[i] = 2.0;
		openMP_THT[i] = 13.0;
		openMP_TTWO[i] = 2.0;
	}

		// Iteration counter for the current thread
		unsigned int i_mp = 0;

	    // Initialize private intermediate variables
       	float openMP_TX = 0.0;
       	float openMP_MTX = 0.0;
       	float openMP_Y = 0.0;
       	float openMP_Z = 0.0;
       	float openMP_X = 0.0;

	    // Iterate/calculate through all the steps via RoundRobin fashion
		for (i_mp = 0; i_mp < nElements; i_mp++) {
	       openMP_TX = 0.0;
	       openMP_MTX = 0.0;
	       openMP_Y = 0.0;
	       openMP_Z = 0.0;
	       openMP_X = 0.0;

	    	#pragma omp parallel sections private(openMP_TX, openMP_MTX, openMP_Y, openMP_Z, openMP_X)
		    {
		    	#pragma omp section
		    	{
				openMP_TX = openMP_TWO[i_mp] * openMP_X;
		    	}
		    	#pragma omp section
		    	{
				openMP_MTX = (- 1.0) * openMP_TX;
		    	}
		    	#pragma omp section
		    	{
				openMP_Y = openMP_THT[i_mp] + openMP_MTX;
		    	}
		    	#pragma omp section
		    	{
				openMP_Z = (- 1.0) * openMP_Y;
				openMP_X = openMP_Y + openMP_TTWO[i_mp];
				openMP_OUT[i_mp] = openMP_X;
		    	}
	         }// OpenMP sections region ends here

	      }

	// Stop the clock
	end = clock();
	printf("Execution time(secs): %f\n\n", (end-start)/(double)CLOCKS_PER_SEC );

	// Validate the results -optional- should use if we already know the final results

	int success = 1;
	for (i = 0; i < nElements; i++) {  
		if ( openMP_OUT[i] != 5.0) {
			success = 0;
			printf("openMP_OUT = %f\n", openMP_OUT[i]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "PipeLineOpenMP,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelLoopSource1,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", (end-start)/(double)CLOCKS_PER_SEC);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
