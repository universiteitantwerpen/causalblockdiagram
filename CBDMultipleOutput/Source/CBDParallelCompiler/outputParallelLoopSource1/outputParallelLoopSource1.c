/*

OpenCL output src code from the "CBDCompilerOpenCL.py". This example
has the ability to execute on internal or external GPU on a mac book pro.

Usage:
./test -t cpu32|cpu64|gpu32|gpu64 -i num -f kernel.bc

For example, to execute against the 32bit GPU in your system:
./test -t gpu32 -i 0 -f kernel.gpu32.bc

Or to test 32bit CPU bitcode:
arch -i386 ./test -t cpu32 -f kernel.cpu32.bc

Or 64bit CPU, presuming a 64bit machine:
./test -t cpu64 -f kernel.cpu64.bc
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>

#include <OpenCL/opencl.h>

#define MAXPATHLEN  512

// The number of total simulation steps
#define NELEMENTS 25000000

// Needed OpenCL objects
int nElements;
int              device_index;
cl_device_type   device_type;
cl_device_id     device;
cl_context       context;
cl_command_queue queue;
cl_program       program;
cl_kernel        kernel;
cl_mem           TWO, THT, TTWO, OUT;
bool             is32bit;
float *host_OUT;

// A utility function to simplify error checking messages.
static void check_status(char* msg, cl_int err) {
	if (err != CL_SUCCESS) {
		fprintf(stderr, "%s failed. Error: %d\n", msg, err);
	}
}

static void create_program_from_bitcode(char* bitcode_path) {
	cl_int err;
	unsigned int i;

	// Pass the pre-compiled bitcode to create the program
	size_t len = strlen(bitcode_path);
	program = clCreateProgramWithBinary(context, 1, &device, &len, (const unsigned char**)&bitcode_path, NULL, &err);
	check_status("clCreateProgramWithBinary", err);

	// Build the program to create executable binary for the specific chosen device
	err = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
	check_status("clBuildProgram", err);

	// Create the Kernel
	kernel = clCreateKernel(program, "cbdCompute", &err);
	check_status("clCreateKernel", err);

	// Create the host (local) variables
	float *host_TWO = (float*)malloc(sizeof(float)*nElements);
	float *host_THT = (float*)malloc(sizeof(float)*nElements);
	float *host_TTWO = (float*)malloc(sizeof(float)*nElements);
	host_OUT = (float*)malloc(sizeof(float)*nElements);
	bool is32bit;

	// Pack buffers with data
	for (i = 0; i < nElements; i++) {
		host_TWO[i] = 2.0;
		host_THT[i] = 13.0;
		host_TTWO[i] = 2.0;
	}

	// Create CL memory buffers and load them with the host data  
	cl_mem TWO = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*nElements, host_TWO, &err);
	cl_mem THT = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*nElements, host_THT, &err);
	cl_mem TTWO = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*nElements, host_TTWO, &err);

	// Set Buffer OUT as write only, since we only store the results
	cl_mem OUT = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float)*nElements, NULL, &err);

	if (TWO == NULL || THT == NULL || TTWO == NULL || OUT == NULL) {
		fprintf(stderr, "Error: Unable to create OpenCL buffer memory objects.\n");
		exit(1);
	}

	// We set the CL buffers as arguments for the 'cbdCompute' kernel.
	int argc = 0;
	err |= clSetKernelArg(kernel, argc++, sizeof(cl_mem), &TWO);
	err |= clSetKernelArg(kernel, argc++, sizeof(cl_mem), &THT);
	err |= clSetKernelArg(kernel, argc++, sizeof(cl_mem), &TTWO);
	err |= clSetKernelArg(kernel, argc++, sizeof(cl_mem), &OUT);
	check_status("clSetKernelArg", err);

	// Launch the kernel with a single dimension. OpenCL selects the local dimensions by
	// passing "NULL" as the 6th argument
	size_t global = nElements;
	err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, NULL, 0, NULL, NULL);
	check_status("clEnqueueNDRangeKernel", err);

	// Read the results from the N1 buffer in blocking mod (CL_TRUE) in order to synchronise
	// by waiting for all kernels to finish
	clEnqueueReadBuffer(queue, OUT, CL_TRUE, 0, nElements*sizeof(cl_float), host_OUT, 0, NULL, NULL);

}

// OpenCL Initialization routine
static void init_opencl() {
	cl_int err;
	cl_uint num_devices;

	// Get the number of compute-capable devices
	clGetDeviceIDs(NULL, device_type, 0, NULL, &num_devices);

	// Check if the devicex index is within device cound bounds
	if (device_index < 0 || device_index > num_devices - 1) {
		fprintf(stdout, "Requested index (%d) is out of range.  Using 0.\n", 
		device_index);
		device_index = 0;
	}

	// Get the selected device
	cl_device_id all_devices[num_devices];
	clGetDeviceIDs(NULL, device_type, num_devices, all_devices, NULL);
	device = all_devices[device_index];

	// Dump info about the device
	char name[128];
	clGetDeviceInfo(device, CL_DEVICE_NAME, 128*sizeof(char), name, NULL);
	fprintf(stdout, "Using OpenCL device: %s\n", name);

	// Create OpenCL context for the selected device
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	check_status("clCreateContext", err);

	// Create a command queue for the context of our selected device
	queue = clCreateCommandQueue(context, device, 0, &err);
	check_status("clCreateCommandQueue", err);
}

// Release/Free all the buffers, memory objects, program, kernels, queues, context
static void shutdown_opencl() {
	clReleaseMemObject(TWO);
	clReleaseMemObject(THT);
	clReleaseMemObject(TTWO);
	clReleaseMemObject(OUT);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);
}

// Executable usage message
static void usage(char* name) {
	fprintf(stdout, "\nUsage:   %s -t gpu32|gpu64|cpu32|cpu64 [-i index] -f filename\n", name);
	fprintf(stdout, "Example: %s -t gpu32 -i 0 -f kernel.gpu32.bc\n\n", name);
	exit(0);
}

// Command line Arguments check
static void process_arguments(int argc, char* const *argv, char* filepath) {
	int c;

	static struct option longopts[] = {
		{"type", required_argument, NULL, 't'},
		{"filename", required_argument, NULL, 'f'},
		{"index", required_argument, NULL, 'i'},
		{"help", no_argument, NULL, 'h'},
		{0, 0, 0, 0}
	};

	while ((c = getopt_long(argc, argv, "t:f:i:h", longopts, NULL)) != -1) {
		switch (c) {
			case 'f':
				filepath[0] = '\0';
				strlcat(filepath, optarg, MAXPATHLEN);
				break;

			case 't':
				if (0 == strncmp(optarg, "gpu", 3)) {
					device_type = CL_DEVICE_TYPE_GPU;
				} else if (0 == strncmp(optarg, "cpu", 3)) {
					device_type = CL_DEVICE_TYPE_CPU;
				} else {
					fprintf(stderr, "Unsupported test device type '%s'; using 'gpu'.\n", optarg);
				}

				if (0 == strncmp(optarg+3, "32", 2)) {
					is32bit = true;
				} else if (0 == strncmp(optarg+3, "64", 2)) {
					is32bit = false;
				} else {
					is32bit = true;
					fprintf(stderr, "Unsupported test device type '%s'; using 'gpu32'.\n", optarg);
				}
				break;

			case 'i':
				device_index = atoi(optarg);
				break;

			case 'h':
				default:
				usage(argv[0]);
		}
	}

	// Ensure the the selected devie type is valid
	if ((device_type != CL_DEVICE_TYPE_GPU) && (device_type != CL_DEVICE_TYPE_CPU)) {
		fprintf(stderr, "Error: device type not specified.\n");
		exit(1);
	}

	// Ensure that the precompiled kernel exists
	struct stat stat_buf;
	if (0 != stat(filepath, &stat_buf)) {
		fprintf(stderr, "Error: file '%s' does not exist.\n", filepath);
		exit(1);
	}
}

// Main execution path
int main (int argc, char* const *argv)
{
	clock_t start, end;
	nElements = NELEMENTS;
    if (argc > 7) {
        nElements = atoi(argv[7]);
    }
	char filepath[MAXPATHLEN];
	filepath[0] = '\0';

	process_arguments(argc, argv, filepath);

	printf("================= GPU ==================\nNumber of discrete time steps: %d\n\n",nElements);
	// Start the execution time counter for OpenCL
	start = clock();

	// Initialize OpenCL objects, context and command queue
	init_opencl();

	// Ensure that the current architecture is compatible
	if (device_type == CL_DEVICE_TYPE_CPU)
	{
#if __LP64__
		if (is32bit)
			fprintf(stderr, "Warning: user specified the 'cpu32' option on the 64bit architecture.\n");
#else
		if (!is32bit)
			fprintf(stderr, "Warning: user specified the 'cpu64' option on the 32bit architecture.\n");
#endif
	}
	else if (device_type == CL_DEVICE_TYPE_GPU)
	{
		cl_int err;
		cl_uint address_bits = 0;
		err = clGetDeviceInfo(device, CL_DEVICE_ADDRESS_BITS, sizeof(address_bits), &address_bits, NULL);

		if (!is32bit && (address_bits == 32))
			fprintf(stderr, "Warning: user specified the 'gpu64' option on the 32bit architecture.\n");
		else if (is32bit && (address_bits == 64))
			fprintf(stderr, "Warning: user specified the 'gpu32' option on the 64bit architecture.\n");
	}

	// Run the program on the kernel
	create_program_from_bitcode(filepath);

	// Shutdown the opencl device
	shutdown_opencl();

	// Stop the openCL clock
	end = clock();

	printf("Execution time(secs): %f\n\n", (end-start)/(double)CLOCKS_PER_SEC );

	// Validate the results -optional- should use if we already know the final results

	int success = 1;
	unsigned int i;
	for (i = 0; i < nElements; i++) {  
		if ( host_OUT[i] != 5.0) {
			success = 0;
			printf("openCL_OUT = %f\n", host_OUT[i]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 8) {
       fp = fopen(argv[8], "a+");
       fprintf(fp, "GPU \%d OpenCL,", device_index);
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelLoopSource1,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", (end-start)/(double)CLOCKS_PER_SEC);
       fclose(fp);
       printf("Saved results in %s\n", argv[8]);
   }
	return 0;
}
