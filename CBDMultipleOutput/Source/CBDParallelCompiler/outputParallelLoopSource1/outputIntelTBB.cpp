/*
	Intel TBB Pipeline output src code from the CBD model simulation for mac
*/

#include <algorithm>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include "tbb/cache_aligned_allocator.h"

// The number of total simulation steps
#define NELEMENTS 25000000
// The number of maxinum tokens
#define MAX_TOKENS 4

using namespace std;
using namespace tbb;
#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))
 
void swap_row(float *a, float *b, int r1, int r2, int n)
{
	float tmp, *p1, *p2;
	int i;
 
	if (r1 == r2) return;
	for (i = 0; i < n; i++) {
		p1 = mat_elem(a, r1, i, n);
		p2 = mat_elem(a, r2, i, n);
		tmp = *p1, *p1 = *p2, *p2 = tmp;
	}
	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;
}
 
void gauss_eliminate(float *a, float *b, float *x, int n)
{
#define A(y, x) (*mat_elem(a, y, x, n))
	int i, j, col, row, max_row,dia;
	float max, tmp;
 
	for (dia = 0; dia < n; dia++) {
		max_row = dia, max = A(dia, dia);
 
		for (row = dia + 1; row < n; row++)
			if ((tmp = fabs(A(row, dia))) > max)
				max_row = row, max = tmp;
 
		swap_row(a, b, dia, max_row, n);
 
		for (row = dia + 1; row < n; row++) {
			tmp = A(row, dia) / A(dia, dia);
			for (col = dia+1; col < n; col++)
				A(row, col) -= tmp * A(dia, col);
			A(row, dia) = 0;
			b[row] -= tmp * b[dia];
		}
	}
	for (row = n - 1; row >= 0; row--) {
		tmp = b[row];
		for (j = n - 1; j > row; j--)
			tmp -= x[j] * A(row, j);
		x[row] = tmp / A(row, row);
	}
#undef A
}

void getSolution(float *coefficients, float *result)
{
	float a[] = {-1,2.0,0,0,0,-1,1,0,0,0,-1,1,-1,0,0,-1};
	gauss_eliminate(a, coefficients, result, 4);
 
}

// Main execution path
int main (int argc, char* const *argv) {

	// Memory allocators to avoid false sharing
	tbb::cache_aligned_allocator<float> floatCacheAlignedAllocator;
	tbb::cache_aligned_allocator<unsigned int> uIntCacheAlignedAllocator;

	// Create the input/constant variables
	unsigned int nElements = *uIntCacheAlignedAllocator.allocate(1);
	unsigned int i_tbb = *uIntCacheAlignedAllocator.allocate(1);
	nElements = NELEMENTS;
	i_tbb = 0;

    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	printf("============ TBB PIPELINE ==============\nNumber of discrete time steps: %d\n\n",nElements);	
	
	printf("Using: IntelTBB & Optimized Pipeline\n");
	printf("Number of Tokens: %d\n", MAX_TOKENS);	

	// Start the execution time counter for OpenCL
	tbb::tick_count mainStartTime = tbb::tick_count::now();
	
	float *intelTBB_TWO = floatCacheAlignedAllocator.allocate(nElements);
	float *intelTBB_THT = floatCacheAlignedAllocator.allocate(nElements);
	float *intelTBB_TTWO = floatCacheAlignedAllocator.allocate(nElements);
	float *intelTBB_OUT = floatCacheAlignedAllocator.allocate(nElements);
	// Allocate the coefficients array
	float *x = floatCacheAlignedAllocator.allocate(4);


	// Pack input variables with data
	for (i_tbb = 0; i_tbb < nElements; i_tbb++) {
		intelTBB_TWO[i_tbb] = 2.0;
		intelTBB_THT[i_tbb] = 13.0;
		intelTBB_TTWO[i_tbb] = 2.0;
	}

    // Initialize private intermediate variables
    float intelTBB_TX = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_MTX = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_Y = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_Z = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_X = *floatCacheAlignedAllocator.allocate(1);

    i_tbb = 0;

    // Pipeline Start
    parallel_pipeline( /*max_number_of_live_token=*/MAX_TOKENS,
        // First Stage Start
        tbb::make_filter<void,int>(
            filter::serial_in_order,
            [&](flow_control& fc)-> int{
                if(i_tbb < nElements) {

					//Solve the algebraic loop
					float coefficients[] = { 0.0, - intelTBB_TTWO[i_tbb], - intelTBB_THT[i_tbb], 0.0 };
					float x[4];
					getSolution(coefficients, x);

					intelTBB_TX = x[0];
					intelTBB_MTX = x[3];
					intelTBB_Y = x[2];
					intelTBB_X = x[1];

				intelTBB_TX = intelTBB_TWO[i_tbb] * intelTBB_X;
                    return i_tbb++;
                 } else {
                    fc.stop();
                    return NULL;
                }
            }
        ) &
		// First Stage end
        // Stage 1 start
        make_filter<int,int>(
            filter::serial_in_order,
            [&](int i_tbb){
				intelTBB_MTX = (- 1.0) * intelTBB_TX;
                return i_tbb;
            }
        ) &
        // Stage 1 End
        // Stage 2 start
        make_filter<int,int>(
            filter::serial_in_order,
            [&](int i_tbb){
				intelTBB_Y = intelTBB_THT[i_tbb] + intelTBB_MTX;
                return i_tbb;
            }
        ) &
        // Stage 2 End
        // Last Stage 3 start
        make_filter<int,void>(
            filter::serial_in_order,
            [&](int i_tbb) {
				intelTBB_Z = (- 1.0) * intelTBB_Y;
				intelTBB_X = intelTBB_Y + intelTBB_TTWO[i_tbb];
				intelTBB_OUT[i_tbb] = intelTBB_X;
            }
        )
        // Last Stage 3 End
    );
	// Stop the clock
	double end = (double)(tbb::tick_count::now() - mainStartTime).seconds();
	printf("Execution time(secs): %f\n\n", end);

	// Validate the results -optional- should use if we already know the final results

	int success = 1;
	for (i_tbb = 0; i_tbb < nElements; i_tbb++) {  
		if ( intelTBB_OUT[i_tbb] != 5.0) {
			success = 0;
			printf("intelTBB_OUT = %f\n", intelTBB_OUT[i_tbb]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i_tbb);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "PipeLineIntelTBB,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelLoopSource1,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", end);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
