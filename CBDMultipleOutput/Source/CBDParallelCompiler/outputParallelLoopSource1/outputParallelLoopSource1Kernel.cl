/*
This kernel computes a flattened CBD with multiple input values as constants and one output
*/

// Optimization 1: The restrict qualifier for the kernel arugments aids in caching but you MUST ensure that the
// pointers do not point on overlapping locations.
// Optimization 2: Enabling double precision floating point (which is also faster than single precision
// since no casting is used to downgrade the accuracy
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))
 
void swap_row(float *a, float *b, int r1, int r2, int n)
{
	float tmp, *p1, *p2;
	int i;
 
	if (r1 == r2) return;
	for (i = 0; i < n; i++) {
		p1 = mat_elem(a, r1, i, n);
		p2 = mat_elem(a, r2, i, n);
		tmp = *p1, *p1 = *p2, *p2 = tmp;
	}
	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;
}
 
void gauss_eliminate(float *a, float *b, float *x, int n)
{
#define A(y, x) (*mat_elem(a, y, x, n))
	int i, j, col, row, max_row,dia;
	float max, tmp;
 
	for (dia = 0; dia < n; dia++) {
		max_row = dia, max = A(dia, dia);
 
		for (row = dia + 1; row < n; row++)
			if ((tmp = fabs(A(row, dia))) > max)
				max_row = row, max = tmp;
 
		swap_row(a, b, dia, max_row, n);
 
		for (row = dia + 1; row < n; row++) {
			tmp = A(row, dia) / A(dia, dia);
			for (col = dia+1; col < n; col++)
				A(row, col) -= tmp * A(dia, col);
			A(row, dia) = 0;
			b[row] -= tmp * b[dia];
		}
	}
	for (row = n - 1; row >= 0; row--) {
		tmp = b[row];
		for (j = n - 1; j > row; j--)
			tmp -= x[j] * A(row, j);
		x[row] = tmp / A(row, row);
	}
#undef A
}

void getSolution(float *coefficients, float *result)
{
	float a[] = {-1,2.0,0,0,0,-1,1,0,0,0,-1,1,-1,0,0,-1};
	gauss_eliminate(a, coefficients, result, 4);
 
}
kernel void cbdCompute(global const float* restrict TWO, global const float* restrict THT, global const float* restrict TTWO, global float* restrict OUT) {
	size_t i = get_global_id(0);


    //Solve the algebraic loop
    float coefficients[] = { 0.0, - TTWO[i], - THT[i], 0.0 };
    float x[4];
    getSolution(coefficients, x);

    __private float TX = x[0];
    __private float MTX = x[3];
    __private float Y = x[2];
    __private float Z = 0.0;
    __private float X = x[1];

    TX = TWO[i] * X;
    MTX = (- 1.0) * TX;
    Y = THT[i] + MTX;
    Z = (- 1.0) * Y;
    X = Y + TTWO[i];
    OUT[i] = X;
}
