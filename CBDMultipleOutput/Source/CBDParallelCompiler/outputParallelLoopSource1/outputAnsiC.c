#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>

// The number of total simulation steps
#define NELEMENTS 25000000

#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))
 
void swap_row(float *a, float *b, int r1, int r2, int n)
{
	float tmp, *p1, *p2;
	int i;
 
	if (r1 == r2) return;
	for (i = 0; i < n; i++) {
		p1 = mat_elem(a, r1, i, n);
		p2 = mat_elem(a, r2, i, n);
		tmp = *p1, *p1 = *p2, *p2 = tmp;
	}
	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;
}
 
void gauss_eliminate(float *a, float *b, float *x, int n)
{
#define A(y, x) (*mat_elem(a, y, x, n))
	int i, j, col, row, max_row,dia;
	float max, tmp;
 
	for (dia = 0; dia < n; dia++) {
		max_row = dia, max = A(dia, dia);
 
		for (row = dia + 1; row < n; row++)
			if ((tmp = fabs(A(row, dia))) > max)
				max_row = row, max = tmp;
 
		swap_row(a, b, dia, max_row, n);
 
		for (row = dia + 1; row < n; row++) {
			tmp = A(row, dia) / A(dia, dia);
			for (col = dia+1; col < n; col++)
				A(row, col) -= tmp * A(dia, col);
			A(row, dia) = 0;
			b[row] -= tmp * b[dia];
		}
	}
	for (row = n - 1; row >= 0; row--) {
		tmp = b[row];
		for (j = n - 1; j > row; j--)
			tmp -= x[j] * A(row, j);
		x[row] = tmp / A(row, row);
	}
#undef A
}

void getSolution(float *coefficients, float *result)
{
	float a[] = {-1,2.0,0,0,0,-1,1,0,0,0,-1,1,-1,0,0,-1};
	gauss_eliminate(a, coefficients, result, 4);
 
}

// Main execution path
int main (int argc, char* const *argv)
{
	clock_t start, end;

	// Create the host (local) variables
	unsigned int i = 0;
	unsigned int nElements = NELEMENTS;
    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	printf("============ SERIAL ANSI C =============\nNumber of discrete time steps: %d\n\n",nElements);
	printf("Using: Serial Ansi C & CPU\n");
	// Start the execution time counter for OpenMP
	start = clock();
	
	float *ansiC_TWO = (float*)malloc(sizeof(float)*nElements);
	float *ansiC_THT = (float*)malloc(sizeof(float)*nElements);
	float *ansiC_TTWO = (float*)malloc(sizeof(float)*nElements);
	float *ansiC_OUT = (float*)malloc(sizeof(float)*nElements);

	// Allocate the coefficients array
	float *x = (float*)malloc(sizeof(float)*4);

	// Pack Ansi C variables with data
	for (i = 0; i < nElements; i++) {
		ansiC_TWO[i] = 2.0;
		ansiC_THT[i] = 13.0;
		ansiC_TTWO[i] = 2.0;
	}

	// Initialize intermediate variables
	float ansiC_TX = 0.0;
	float ansiC_MTX = 0.0;
	float ansiC_Y = 0.0;
	float ansiC_Z = 0.0;
	float ansiC_X = 0.0;

	// Calculate data
	for (i = 0; i < nElements; i++) {

	       //Solve the algebraic loop
	       float coefficients[] = { 0.0, - ansiC_TTWO[i], - ansiC_THT[i], 0.0 };
	       float x[4];
	       getSolution(coefficients, x);

	       ansiC_TX = x[0];
	       ansiC_MTX = x[3];
	       ansiC_Y = x[2];
	       ansiC_X = x[1];

        ansiC_TX = ansiC_TWO[i] * ansiC_X;
        ansiC_MTX = (- 1.0) * ansiC_TX;
        ansiC_Y = ansiC_THT[i] + ansiC_MTX;
        ansiC_Z = (- 1.0) * ansiC_Y;
        ansiC_X = ansiC_Y + ansiC_TTWO[i];
        ansiC_OUT[i] = ansiC_X;
	}

	// Stop the ansi c clock
	end = clock();
	printf("Execution time(secs): %f\n\n", (end-start)/(double)CLOCKS_PER_SEC );


	// Validate the results -optional- should use if we already know the final results
	int success = 1;
	for (i = 0; i < nElements; i++) {  
		if ( ansiC_OUT[i] != 5.0) {
			success = 0;
			printf("ansiC_OUT = %f\n", ansiC_OUT[i]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "AnsiC,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelLoopSource1,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", (end-start)/(double)CLOCKS_PER_SEC);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
