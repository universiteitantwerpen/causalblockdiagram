/*
	OpenMP output src code from the CBD model simulation for mac
	applying data/domain decomposition
	Do check the environment variables first (i.e):
	export DYLD_LIBRARY_PATH="/opt/intel/lib" // for Intel C++ open mp compiler for Mac
	export OMP_NUM_THREADS=4 // The number of maximum threads
	export OMP_STACKSIZE="50M" // Small stack size causes fragmentation errors, increase accordingly
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <omp.h>

// The number of total simulation steps
#define NELEMENTS 25000000
// The number of maxinum spawning threads
#define MAX_THREADS 4
#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))
 
void swap_row(float *a, float *b, int r1, int r2, int n)
{
	float tmp, *p1, *p2;
	int i;
 
	if (r1 == r2) return;
	for (i = 0; i < n; i++) {
		p1 = mat_elem(a, r1, i, n);
		p2 = mat_elem(a, r2, i, n);
		tmp = *p1, *p1 = *p2, *p2 = tmp;
	}
	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;
}
 
void gauss_eliminate(float *a, float *b, float *x, int n)
{
#define A(y, x) (*mat_elem(a, y, x, n))
	int i, j, col, row, max_row,dia;
	float max, tmp;
 
	for (dia = 0; dia < n; dia++) {
		max_row = dia, max = A(dia, dia);
 
		for (row = dia + 1; row < n; row++)
			if ((tmp = fabs(A(row, dia))) > max)
				max_row = row, max = tmp;
 
		swap_row(a, b, dia, max_row, n);
 
		for (row = dia + 1; row < n; row++) {
			tmp = A(row, dia) / A(dia, dia);
			for (col = dia+1; col < n; col++)
				A(row, col) -= tmp * A(dia, col);
			A(row, dia) = 0;
			b[row] -= tmp * b[dia];
		}
	}
	for (row = n - 1; row >= 0; row--) {
		tmp = b[row];
		for (j = n - 1; j > row; j--)
			tmp -= x[j] * A(row, j);
		x[row] = tmp / A(row, row);
	}
#undef A
}

void getSolution(float *coefficients, float *result)
{
	float a[] = {-1,2.0,0,0,0,-1,1,0,0,0,-1,1,-1,0,0,-1};
	gauss_eliminate(a, coefficients, result, 4);
 
}

// Main execution path
int main (int argc, char* const *argv)
{
	clock_t start, end;
	
	unsigned int nElements = NELEMENTS;
    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	// OpenMP variables: number of total threads, current thread id, min I item, max I item
	int currentThreadId;
	int maxThreads = MAX_THREADS;
	unsigned int minI, maxI;
	
	// Enforce the number of maximum threads
	omp_set_num_threads(maxThreads);
	
	// Job size/number of iterations for each thread
	unsigned int threadJobSize = nElements / maxThreads;
	
	printf("=========== PARALLEL_FOR CPU ===========\nNumber of discrete time steps: %d\n\n",nElements);	
	
	printf("Using: OpenMP & CPU\n");
	printf("Number of threads: %d\n", maxThreads);	
	printf("Job size per thread: %d\n",threadJobSize);	

	// Start the execution time counter for OpenMP
	start = clock();
	
	// Create the input/constant variables
	unsigned int i = 0;
	float *openMP_TWO = (float*)malloc(sizeof(float)*nElements);
	float *openMP_THT = (float*)malloc(sizeof(float)*nElements);
	float *openMP_TTWO = (float*)malloc(sizeof(float)*nElements);
	float *openMP_OUT = (float*)malloc(sizeof(float)*nElements);

	// Allocate the coefficients array
	float *x = (float*)malloc(sizeof(float)*4);

	// Pack input variables with data
	for (i = 0; i < nElements; i++) {
		openMP_TWO[i] = 2.0;
		openMP_THT[i] = 13.0;
		openMP_TTWO[i] = 2.0;
	}

	// Do Calculate CBD
	
	// Iteration counter for the current thread
	unsigned int i_mp = 0;

	    // Initialize private intermediate variables
       	float openMP_TX = 0.0;
       	float openMP_MTX = 0.0;
       	float openMP_Y = 0.0;
       	float openMP_Z = 0.0;
       	float openMP_X = 0.0;

	#pragma omp parallel for private(openMP_TX, openMP_MTX, openMP_Y, openMP_Z, openMP_X)
	for (i_mp = 0; i_mp < nElements; i_mp++) {

	       //Solve the algebraic loop
	       float coefficients[] = { 0.0, - openMP_TTWO[i_mp], - openMP_THT[i_mp], 0.0 };
	       float x[4];
	       getSolution(coefficients, x);

	       openMP_TX = x[0];
	       openMP_MTX = x[3];
	       openMP_Y = x[2];
	       openMP_X = x[1];

        openMP_TX = openMP_TWO[i_mp] * openMP_X;
        openMP_MTX = (- 1.0) * openMP_TX;
        openMP_Y = openMP_THT[i_mp] + openMP_MTX;
        openMP_Z = (- 1.0) * openMP_Y;
        openMP_X = openMP_Y + openMP_TTWO[i_mp];
        openMP_OUT[i_mp] = openMP_X;
	}

	// Stop the clock
	end = clock();
	printf("Execution time(secs): %f\n\n", (end-start)/(double)CLOCKS_PER_SEC );

	// Validate the results -optional- should use if we already know the final results

	int success = 1;
	for (i = 0; i < nElements; i++) {  
		if ( openMP_OUT[i] != 5.0) {
			success = 0;
			printf("openMP_OUT = %f\n", openMP_OUT[i]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "ParallelForOpenMP,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelLoopSource1,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", (end-start)/(double)CLOCKS_PER_SEC);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
