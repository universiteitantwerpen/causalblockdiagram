__author__ = 'madks_000'

import os
import csv

class CBDBenchmarkResultsEditor():
    """
    A class that generates the script that compiles, builds, runs all the resulting src code and stores the result to a
     CSV file
    """
    def __init__(self, compilerSettings, benchmarkFileName):
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()
        self.__currentPath = compilerSettings.getCurrentPath()
        self.__benchmarkFileName = benchmarkFileName
        self.__allLinesList = []
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()

    def allRowsToList(self):
        with open(self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName) as csvfile:
            self.__allLinesList = csv.DictReader(csvfile)
            for row in self.__allLinesList:

                currentCsvFileWriterRootDirectory = os.path.splitext(self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName)[0]
                if not os.path.exists(currentCsvFileWriterRootDirectory):
                    os.makedirs(currentCsvFileWriterRootDirectory)

                currentCsvFileWriterDirectory = currentCsvFileWriterRootDirectory + "/" + row['ModelName'] + "/"
                if not os.path.exists(currentCsvFileWriterDirectory):
                    os.makedirs(currentCsvFileWriterDirectory)

                currentCsvFileWriterPath = currentCsvFileWriterDirectory + row['Platform']  + self.__benchmarkFileName

                if not os.path.exists(currentCsvFileWriterPath):
                    f = open(currentCsvFileWriterPath, "w")
                    f.write("Platform%sIterations%sModelName%sTime\n" % (self.__csvSeparationCharacter, self.__csvSeparationCharacter, self.__csvSeparationCharacter))

                f = open(currentCsvFileWriterPath, "a")
                f.write(row['Platform'] + self.__csvSeparationCharacter + row['Iterations'] + self.__csvSeparationCharacter + row['ModelName'] + self.__csvSeparationCharacter + row['Time'] + "\n")
                f.close()

    def splitSaveToDisk(self):
        self.allRowsToList()