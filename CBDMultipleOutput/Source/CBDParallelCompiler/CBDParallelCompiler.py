from CBDMultipleOutput.Source.CBDParallelCompiler.CBDScriptGenerators import CBDBuildScriptGenerator, \
    CBDRunScriptGenerator, CBDMakefileGenerator

__author__ = 'madks_000'

import os
from CBDCompilerSettings import CBDCompilerSettings
from CBDComputationalDelayPipelineStageGenerator import CBDComputationalDelayPipelineStageGenerator
from CBDSourceCodeGenerators.CBDAnsiCCompiler import CBDAnsiCCompiler
from CBDSourceCodeGenerators.CBDOpenGLCompiler import CBDOpenGLCompiler
from CBDSourceCodeGenerators.CBDOpenMPParallelForCompiler import CBDOpenMPParallelForCompiler
from CBDSourceCodeGenerators.CBDOpenMPPipelineStagesCompiler import CBDOpenMPPipelineStagesCompiler
from CBDScriptGenerators.CBDBuildScriptGenerator import CBDBuildScriptGenerator
from CBDScriptGenerators.CBDMakefileGenerator import CBDMakefileGenerator
from CBDScriptGenerators.CBDRunScriptGenerator import CBDRunScriptGenerator
from CBDSourceCodeGenerators.CBDOpenCLCompiler import CBDOpenCLCompiler
from CBDSourceCodeGenerators.CBDOpenCLKernelCompiler import CBDOpenCLKernelCompiler
from CBDScriptGenerators.CBDBenchmarkScriptGenerator import CBDBenchmarkScriptGenerator
from CBDSourceCodeGenerators.CBDIntelTBBPipelineStagesCompiler import CBDIntelTBBPipelineStagesCompiler

class CBDParallelCompiler:
    """
    A basic class that invokes all the necessary compilers to generate parallel source code
    """
    def __init__(self, iterations, cbd, fileName="output", rand=False, numMaxThreads=4, pipelineStages=4, expectingResult=0.0):
        self.__rand = rand
        self.__fileName = fileName
        self.__numMaxThreads = numMaxThreads
        self.__iterations = iterations
        self.__cbd = cbd
        self.__pipelineStages = pipelineStages
        self.__expectingResult = expectingResult

    def getIterations(self):
        return self.__iterations

    def getPipelineStages(self):
        return self.__pipelineStages

    def getCompilerSettings(self):
        # Get the directory path to save the output files
        directoryPath = os.path.dirname(os.path.realpath(__file__))

        if not os.path.exists(directoryPath + "/" + self.__fileName):
            os.makedirs(directoryPath + "/" + self.__fileName)

        # Calculate pipeline stages
        optimizedPipelineStagesDictionary  = CBDComputationalDelayPipelineStageGenerator(self.__cbd, {}, 4).getOptimizedPipelineStagesDictionary()

        # Store settings into the data class
        compilerSettings = CBDCompilerSettings(self.__rand, self.getIterations(), self.getPipelineStages(), self.__cbd, directoryPath, self.__numMaxThreads, self.__fileName, optimizedPipelineStagesDictionary, True, self.__expectingResult)

        return compilerSettings


    def toDisk(self):
        """
        Perform all the required operations to generate all the output source code to disk
        """

        compilerSettings = self.getCompilerSettings()

        # Generate the OpenCL src code
        openCLGenerator = CBDOpenCLCompiler(compilerSettings)
        openCLGenerator.toDisk()

        # Generate the OpenCL Kernel src code
        openCLKernelGenerator = CBDOpenCLKernelCompiler(compilerSettings)
        openCLKernelGenerator.toDisk()

        # Generate the OpenGL src code
        openGLGenerator = CBDOpenGLCompiler(compilerSettings)
        openGLGenerator.toDisk()

        # Generate the OpenMP src code
        openMPGenerator = CBDOpenMPParallelForCompiler(compilerSettings)
        openMPGenerator.toDisk()

        # Generate the OpenMP src code
        openMPRegionsGenerator = CBDOpenMPPipelineStagesCompiler(compilerSettings)
        openMPRegionsGenerator.toDisk()

        # Generate the Intel TBB src code
        intelTBBPipelineStagesGenerator = CBDIntelTBBPipelineStagesCompiler(compilerSettings)
        intelTBBPipelineStagesGenerator.toDisk()

        # Generate the AnsiC src code
        ansiCGenerator = CBDAnsiCCompiler(compilerSettings)
        ansiCGenerator.toDisk()

        # Generate the run script
        runScriptGenerator = CBDRunScriptGenerator(compilerSettings)
        runScriptGenerator.toDisk()

        # Generate the build script
        buildScriptGenerator = CBDBuildScriptGenerator(compilerSettings)
        buildScriptGenerator.toDisk()

        # Generate the Makefile
        makeFileGenerator = CBDMakefileGenerator(compilerSettings)
        makeFileGenerator.toDisk()

        # Generate the Benchmark batch file
        benchmarkScriptGenerator = CBDBenchmarkScriptGenerator(compilerSettings)
        benchmarkScriptGenerator.toDisk()

        print "Done saving the output Parallel code in the directory \"%s\"." % self.__fileName