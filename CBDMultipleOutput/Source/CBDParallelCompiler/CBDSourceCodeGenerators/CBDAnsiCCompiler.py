__author__ = 'madks_000'

import os

class CBDAnsiCCompiler():
    """
    A class that generates serial Ansi C src code
    """
    def __init__(self, compilerSettings):
        self.__blockNamesList = []
        self.__rowsAnsiC = []
        self.__iterations = compilerSettings.getIterations()
        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__rand = compilerSettings.getRand()
        self.__cbd = compilerSettings.getCBD()
        self.__signalCount = compilerSettings.getSignalCount()
        self.__iterations = compilerSettings.getIterations()
        self.__blockNamesList = compilerSettings.getBlockNames()
        self.__strongComponentNames = compilerSettings.getStrongComponentNames()
        self.__gaussianSolverInput = compilerSettings.getGaussianSolverInput()
        self.__benchmarkFileName = compilerSettings.getBenchmarkFileName()
        self.__currentPath = compilerSettings.getCurrentPath()
        self.__fileName = compilerSettings.getFileName()
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()
        self.__expectingResult = compilerSettings.getExpectingResult()

    def getIterations(self):
        return self.__iterations

    def getAnsiInputBlockComputationString(self, block, port, ansiC=True):
        inputType = block.getLinksIn()[port][0].getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def dumpGaussianSolver(self):
        self.__rowsAnsiC.append("#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))\n")
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("void swap_row(float *a, float *b, int r1, int r2, int n)\n")
        self.__rowsAnsiC.append("{\n")
        self.__rowsAnsiC.append("	float tmp, *p1, *p2;\n")
        self.__rowsAnsiC.append("	int i;\n")
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("	if (r1 == r2) return;\n")
        self.__rowsAnsiC.append("	for (i = 0; i < n; i++) {\n")
        self.__rowsAnsiC.append("		p1 = mat_elem(a, r1, i, n);\n")
        self.__rowsAnsiC.append("		p2 = mat_elem(a, r2, i, n);\n")
        self.__rowsAnsiC.append("		tmp = *p1, *p1 = *p2, *p2 = tmp;\n")
        self.__rowsAnsiC.append("	}\n")
        self.__rowsAnsiC.append("	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;\n")
        self.__rowsAnsiC.append("}\n")
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("void gauss_eliminate(float *a, float *b, float *x, int n)\n")
        self.__rowsAnsiC.append("{\n")
        self.__rowsAnsiC.append("#define A(y, x) (*mat_elem(a, y, x, n))\n")
        self.__rowsAnsiC.append("	int i, j, col, row, max_row,dia;\n")
        self.__rowsAnsiC.append("	float max, tmp;\n")
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("	for (dia = 0; dia < n; dia++) {\n")
        self.__rowsAnsiC.append("		max_row = dia, max = A(dia, dia);\n")
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("		for (row = dia + 1; row < n; row++)\n")
        self.__rowsAnsiC.append("			if ((tmp = fabs(A(row, dia))) > max)\n")
        self.__rowsAnsiC.append("				max_row = row, max = tmp;\n")
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("		swap_row(a, b, dia, max_row, n);\n")
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("		for (row = dia + 1; row < n; row++) {\n")
        self.__rowsAnsiC.append("			tmp = A(row, dia) / A(dia, dia);\n")
        self.__rowsAnsiC.append("			for (col = dia+1; col < n; col++)\n")
        self.__rowsAnsiC.append("				A(row, col) -= tmp * A(dia, col);\n")
        self.__rowsAnsiC.append("			A(row, dia) = 0;\n")
        self.__rowsAnsiC.append("			b[row] -= tmp * b[dia];\n")
        self.__rowsAnsiC.append("		}\n")
        self.__rowsAnsiC.append("	}\n")
        self.__rowsAnsiC.append("	for (row = n - 1; row >= 0; row--) {\n")
        self.__rowsAnsiC.append("		tmp = b[row];\n")
        self.__rowsAnsiC.append("		for (j = n - 1; j > row; j--)\n")
        self.__rowsAnsiC.append("			tmp -= x[j] * A(row, j);\n")
        self.__rowsAnsiC.append("		x[row] = tmp / A(row, row);\n")
        self.__rowsAnsiC.append("	}\n")
        self.__rowsAnsiC.append("#undef A\n")
        self.__rowsAnsiC.append("}\n")
        self.__rowsAnsiC.append("\n")

    def dumpAlgebraicCBDLoopSolver(self):
        self.__rowsAnsiC.append("void getSolution(float *coefficients, float *result)\n")
        self.__rowsAnsiC.append("{\n")

        self.__rowsAnsiC.append("	float a[] = {")
        for i, currentRow in enumerate(self.__gaussianSolverInput[0]):
            if i > 0:
                self.__rowsAnsiC.append(",")
            self.__rowsAnsiC.append("%s" % (','.join(map(str, currentRow))))
        self.__rowsAnsiC.append("};\n")
        self.__rowsAnsiC.append("	gauss_eliminate(a, coefficients, result, %s);\n" % len(self.__gaussianSolverInput[1]))
        self.__rowsAnsiC.append(" \n")
        self.__rowsAnsiC.append("}\n")

    def getAnsiCBlockComputationString(self, block, ansiC=True):
        inputType = block.getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def dumpAnsiC(self):
        self.__rowsAnsiC.append("#include <stdlib.h>\n")
        self.__rowsAnsiC.append("#include <stdio.h>\n")
        self.__rowsAnsiC.append("#include <string.h>\n")
        self.__rowsAnsiC.append("#include <getopt.h>\n")
        self.__rowsAnsiC.append("#include <time.h>\n")
        self.__rowsAnsiC.append("#include <math.h>\n")
        self.__rowsAnsiC.append("#include <sys/stat.h>\n")
        self.__rowsAnsiC.append("\n")
        self.__rowsAnsiC.append("// The number of total simulation steps\n")
        self.__rowsAnsiC.append("#define NELEMENTS %d\n" % self.getIterations())
        self.__rowsAnsiC.append("\n")

        if len(self.__strongComponentNames) > 0:
            self.dumpGaussianSolver()
            self.dumpAlgebraicCBDLoopSolver()

        self.__rowsAnsiC.append("\n")
        self.__rowsAnsiC.append("// Main execution path\n")
        self.__rowsAnsiC.append("int main (int argc, char* const *argv)\n")
        self.__rowsAnsiC.append("{\n")
        self.__rowsAnsiC.append("	clock_t start, end;\n")
        self.__rowsAnsiC.append("\n")

        # Create Ansi C local variables
        self.__rowsAnsiC.append("	// Create the host (local) variables\n")
        self.__rowsAnsiC.append("	unsigned int i = 0;\n")
        self.__rowsAnsiC.append("	unsigned int nElements = NELEMENTS;\n")

        self.__rowsAnsiC.append("    if (argc > 1) {\n")
        self.__rowsAnsiC.append("        nElements = atoi(argv[1]);\n")
        self.__rowsAnsiC.append("    }\n")
        self.__rowsAnsiC.append("	printf(\"============ SERIAL ANSI C =============\\nNumber of discrete time steps: %d\\n\\n\",nElements);\n")
        self.__rowsAnsiC.append("	printf(\"Using: Serial Ansi C & CPU\\n\");\n")

        self.__rowsAnsiC.append("	// Start the execution time counter for OpenMP\n")
        self.__rowsAnsiC.append("	start = clock();\n")
        self.__rowsAnsiC.append("	\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsAnsiC.append("	float *ansiC_%s = (float*)malloc(sizeof(float)*nElements);\n" % currentBlockName.upper())
        self.__rowsAnsiC.append("	float *ansiC_OUT = (float*)malloc(sizeof(float)*nElements);\n")
        self.__rowsAnsiC.append("\n")

        if len(self.__strongComponentNames) > 0:
            # Allocate the coefficients array
            self.__rowsAnsiC.append("	// Allocate the coefficients array\n")
            self.__rowsAnsiC.append("	float *x = (float*)malloc(sizeof(float)*%s);\n" % len(self.__gaussianSolverInput[1]))

        self.__rowsAnsiC.append("\n")
        self.__rowsAnsiC.append("	// Pack Ansi C variables with data\n")
        self.__rowsAnsiC.append("	for (i = 0; i < nElements; i++) {\n")
        # Put constant data on Constant buffers
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if self.__rand == False:
                    self.__rowsAnsiC.append("		ansiC_%s[i] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))
                else:
                    self.__rowsAnsiC.append("		ansiC_%s[i] = rand() %s 10;\n" % (currentBlockName.upper(), "%"))
        self.__rowsAnsiC.append("	}\n")

        self.__rowsAnsiC.append("\n")

        # Initialize intermediate variables
        self.__rowsAnsiC.append("	// Initialize intermediate variables\n")
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                self.__rowsAnsiC.append("	float ansiC_%s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsAnsiC.append("\n")

        self.__rowsAnsiC.append("	// Calculate data\n")
        self.__rowsAnsiC.append("	for (i = 0; i < nElements; i++) {\n")

        if len(self.__strongComponentNames) > 0:
            strOut = []
            for elementName in self.__gaussianSolverInput[3]:
                if not elementName:
                    strOut.append("0.0")
                else:
                    strOut.append("- ansiC_%s[i]" % elementName.upper())

        self.__rowsAnsiC.append("\n")

        if len(self.__strongComponentNames) > 0:
            self.__rowsAnsiC.append("	       //Solve the algebraic loop\n")
            self.__rowsAnsiC.append("	       float coefficients[] = { %s };\n" % (', '.join(map(str, strOut))))
            self.__rowsAnsiC.append("	       float x[%s];\n" % len(self.__gaussianSolverInput[1]))
            self.__rowsAnsiC.append("	       getSolution(coefficients, x);\n")
            self.__rowsAnsiC.append("\n")

        # Print intermediate variables
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                if len(self.__strongComponentNames) > 0:
                    if currentBlockName in self.__strongComponentNames:
                        self.__rowsAnsiC.append("	       ansiC_%s = x[%s];\n" % (currentBlockName.upper(), self.__strongComponentNames.index(currentBlockName)))

                else:
                    self.__rowsAnsiC.append("	    ansiC_%s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsAnsiC.append("\n")

        ##
        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = self.getAnsiCBlockComputationString(block)
            currentBlockType = block.getBlockType()

            if currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = self.getAnsiInputBlockComputationString(block, "IN1")

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "(- 1.0) * %s;" % inputString
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s;" % inputString
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s);" % (block.getBlockOperator(), inputString)

                self.__rowsAnsiC.append("        %s = %s\n" % (currentBlockName, operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = self.getAnsiInputBlockComputationString(block, "IN1")
                inputString2 = self.getAnsiInputBlockComputationString(block, "IN2")

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    self.__rowsAnsiC.append("        %s = pow(%s, %s);\n" % (currentBlockName, inputString1, inputString2))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    self.__rowsAnsiC.append("        %s = %s %s %s;\n" % (currentBlockName, inputString1, operatorString , inputString2))
        ##

        # Last line, store the last variable to the OUT global float
        lastBlock = self.__cbd.getBlocks()[-1].getBlockName().upper()
        self.__rowsAnsiC.append("        ansiC_OUT[i] = ansiC_%s;\n" % lastBlock)

        self.__rowsAnsiC.append("	}\n")
        self.__rowsAnsiC.append("\n")
        self.__rowsAnsiC.append("	// Stop the ansi c clock\n")
        self.__rowsAnsiC.append("	end = clock();\n")
        self.__rowsAnsiC.append("	printf(\"Execution time(secs): %f\\n\\n\", (end-start)/(double)CLOCKS_PER_SEC );\n")
        self.__rowsAnsiC.append("\n")

        self.__rowsAnsiC.append("\n")
        self.__rowsAnsiC.append("	// Validate the results -optional- should use if we already know the final results\n")
        self.__rowsAnsiC.append("	int success = 1;\n")
        self.__rowsAnsiC.append("	for (i = 0; i < nElements; i++) {  \n")
        self.__rowsAnsiC.append("		if ( ansiC_OUT[i] != %s) {\n" % self.__expectingResult)
        self.__rowsAnsiC.append("			success = 0;\n")
        self.__rowsAnsiC.append("			printf(\"ansiC_OUT = %f\\n\", ansiC_OUT[i]);\n")
        self.__rowsAnsiC.append("			fprintf(stderr, \"Validation failed at index %d\\n----------------------------------------\\n\", i);\n")
        self.__rowsAnsiC.append("			break;\n")
        self.__rowsAnsiC.append("		}\n")
        self.__rowsAnsiC.append("	}\n")
        self.__rowsAnsiC.append("\n")
        self.__rowsAnsiC.append("	if (success) {\n")
        self.__rowsAnsiC.append("		fprintf(stdout, \"Validation successful.\\n----------------------------------------\\n\");\n")
        self.__rowsAnsiC.append("	}\n")
        self.__rowsAnsiC.append("\n")

        self.dumpSaveResultsToCSV()

        self.__rowsAnsiC.append("	return 0;\n")
        self.__rowsAnsiC.append("}\n")

    def dumpSaveResultsToCSV(self):
        self.__rowsAnsiC.append("   // Save results to CSV\n")
        self.__rowsAnsiC.append("   FILE *fp;\n")
        self.__rowsAnsiC.append("\n")

        csvFileSavePath = self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName

        self.__rowsAnsiC.append("   if (argc > 2) {\n")
        self.__rowsAnsiC.append("       fp = fopen(argv[2], \"a+\");\n")
        self.__rowsAnsiC.append("       fprintf(fp, \"AnsiC%s\");\n" % self.__csvSeparationCharacter)
        self.__rowsAnsiC.append("       fprintf(fp, \"%s%s\", nElements);\n" % ("%d", self.__csvSeparationCharacter))
        self.__rowsAnsiC.append("       fprintf(fp, \"%s%s\");\n" % (self.__fileName, self.__csvSeparationCharacter))
        self.__rowsAnsiC.append("       fprintf(fp, \"%s%s\", success);\n" % ("%d", self.__csvSeparationCharacter))
        self.__rowsAnsiC.append("       fprintf(fp, \"%f\\n\", (end-start)/(double)CLOCKS_PER_SEC);\n")
        self.__rowsAnsiC.append("       fclose(fp);\n")
        self.__rowsAnsiC.append("       printf(\"Saved results in %s\\n\", argv[2]);\n")
        self.__rowsAnsiC.append("   }\n")

    def walkAnsiC(self):

        # Clean the output list of lines
        self.__rowsAnsiC = []

        # Dump the makefile
        self.dumpAnsiC()

        return "".join(self.__rowsAnsiC)

    def toDisk(self):

        outputString = self.walkAnsiC()
        text_file = open(self.__directoryPath + "/" + "outputAnsiC" + ".c", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output AnsiC src on %s." % self.__directoryPath