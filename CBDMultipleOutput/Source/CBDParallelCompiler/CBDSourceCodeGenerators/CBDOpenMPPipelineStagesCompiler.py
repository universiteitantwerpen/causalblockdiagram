__author__ = 'madks_000'

import os

class CBDOpenMPPipelineStagesCompiler():
    """
    A class that generates the OpenMP CPU Pipeline src code
    """
    def __init__(self, compilerSettings):
        self.__blockNamesList = []
        self.__rowsOpenMPRegions = []

        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__rand = compilerSettings.getRand()
        self.__numMaxThreads = compilerSettings.getMaxThreads()
        self.__cbd = compilerSettings.getCBD()
        self.__signalCount = compilerSettings.getSignalCount()
        self.__iterations = compilerSettings.getIterations()
        self.__blockNamesList = compilerSettings.getBlockNames()
        self.__intermediateVariablesList = []
        self.__optimizedPipelineStagesDictionary = compilerSettings.getOptimizedPipelineStagesDictionary()
        self.__expectingResult = compilerSettings.getExpectingResult()
        self.__benchmarkFileName = compilerSettings.getBenchmarkFileName()
        self.__currentPath = compilerSettings.getCurrentPath()
        self.__fileName = compilerSettings.getFileName()
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()

    def getIterations(self):
        return self.__iterations

    def getAnsiInputBlockComputationString(self, block, port, ansiC=True):
        inputType = block.getLinksIn()[port][0].getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def getAnsiCBlockComputationString(self, block, ansiC=True):
        inputType = block.getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def checkAdvanceStage(self, currentStage, currentBlockStage):
        if currentStage is not currentBlockStage:
            if currentStage is not -1:
                self.__rowsOpenMPRegions.append("		    	}\n")
            currentStage = currentBlockStage
            self.__rowsOpenMPRegions.append("		    	#pragma omp section\n")
            self.__rowsOpenMPRegions.append("		    	{\n")
        return currentStage

    def dumpOpenMP(self):
        self.__rowsOpenMPRegions.append("/*\n")
        self.__rowsOpenMPRegions.append("	OpenMP output src code from the CBD model simulation for mac\n")
        self.__rowsOpenMPRegions.append("	applying data/domain decomposition\n")
        self.__rowsOpenMPRegions.append("	Do check the environment variables first (i.e):\n")
        self.__rowsOpenMPRegions.append("	export DYLD_LIBRARY_PATH=\"/opt/intel/lib\" // for Intel C++ open mp compiler for Mac\n")
        self.__rowsOpenMPRegions.append("	export OMP_NUM_THREADS=4 // The number of maximum threads\n")
        self.__rowsOpenMPRegions.append("	export OMP_STACKSIZE=\"50M\" // Small stack size causes fragmentation errors, increase accordingly\n")
        self.__rowsOpenMPRegions.append("*/\n")
        self.__rowsOpenMPRegions.append("\n")
        self.__rowsOpenMPRegions.append("#include <stdlib.h>\n")
        self.__rowsOpenMPRegions.append("#include <stdio.h>\n")
        self.__rowsOpenMPRegions.append("#include <string.h>\n")
        self.__rowsOpenMPRegions.append("#include <getopt.h>\n")
        self.__rowsOpenMPRegions.append("#include <time.h>\n")
        self.__rowsOpenMPRegions.append("#include <math.h>\n")
        self.__rowsOpenMPRegions.append("#include <sys/stat.h>\n")
        self.__rowsOpenMPRegions.append("#include <omp.h>\n")
        self.__rowsOpenMPRegions.append("\n")

        self.__rowsOpenMPRegions.append("// The number of total simulation steps\n")
        self.__rowsOpenMPRegions.append("#define NELEMENTS %d\n" % self.getIterations())

        self.__rowsOpenMPRegions.append("// The number of maxinum spawning threads\n")
        self.__rowsOpenMPRegions.append("#define MAX_THREADS %d\n" % self.__numMaxThreads)

        self.__rowsOpenMPRegions.append("\n")
        self.__rowsOpenMPRegions.append("// Main execution path\n")
        self.__rowsOpenMPRegions.append("int main (int argc, char* const *argv)\n")
        self.__rowsOpenMPRegions.append("{\n")
        self.__rowsOpenMPRegions.append("	clock_t start, end;\n")
        self.__rowsOpenMPRegions.append("	\n")

        self.__rowsOpenMPRegions.append("	unsigned int nElements = NELEMENTS;\n")

        self.__rowsOpenMPRegions.append("    if (argc > 1) {\n")
        self.__rowsOpenMPRegions.append("        nElements = atoi(argv[1]);\n")
        self.__rowsOpenMPRegions.append("    }\n")

        self.__rowsOpenMPRegions.append("	// OpenMP variables: number of total threads, current thread id, min I item, max I item\n")
        self.__rowsOpenMPRegions.append("	int currentThreadId;\n")
        self.__rowsOpenMPRegions.append("	int maxThreads = MAX_THREADS;\n")
        self.__rowsOpenMPRegions.append("	unsigned int minI, maxI;\n")
        self.__rowsOpenMPRegions.append("	\n")
        self.__rowsOpenMPRegions.append("	// Enforce the number of maximum threads\n")
        self.__rowsOpenMPRegions.append("	omp_set_num_threads(maxThreads);\n")
        self.__rowsOpenMPRegions.append("	\n")
        self.__rowsOpenMPRegions.append("	// Job size/number of iterations for each thread\n")
        self.__rowsOpenMPRegions.append("	unsigned int threadJobSize = nElements / maxThreads;\n")
        self.__rowsOpenMPRegions.append("	\n")
        self.__rowsOpenMPRegions.append("	printf(\"============= PIPELINE CPU =============\\nNumber of discrete time steps: %d\\n\\n\",nElements);	\n")
        self.__rowsOpenMPRegions.append("	\n")
        self.__rowsOpenMPRegions.append("	printf(\"Using: OpenMP & Optimized Pipeline\\n\");\n")
        self.__rowsOpenMPRegions.append("	printf(\"Number of threads: %d\\n\", maxThreads);	\n")
        self.__rowsOpenMPRegions.append("	printf(\"Number of Stages: %d\\n\", maxThreads);	\n")
        self.__rowsOpenMPRegions.append("\n")
        self.__rowsOpenMPRegions.append("	// Start the execution time counter for OpenCL\n")
        self.__rowsOpenMPRegions.append("	start = clock();\n")
        self.__rowsOpenMPRegions.append("	\n")

         # Create OpenMP local variables
        self.__rowsOpenMPRegions.append("	// Create the input/constant variables\n")
        self.__rowsOpenMPRegions.append("	unsigned int i = 0;\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenMPRegions.append("	float *openMP_%s = (float*)malloc(sizeof(float)*nElements);\n" % currentBlockName.upper())
        self.__rowsOpenMPRegions.append("	float *openMP_OUT = (float*)malloc(sizeof(float)*nElements);\n")

        self.__rowsOpenMPRegions.append("\n")
        self.__rowsOpenMPRegions.append("	// Pack input variables with data\n")
        self.__rowsOpenMPRegions.append("	for (i = 0; i < nElements; i++) {\n")
        # Put constant data on Constant buffers
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if self.__rand == False:
                    self.__rowsOpenMPRegions.append("		openMP_%s[i] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))
                else:
                    self.__rowsOpenMPRegions.append("		openMP_%s[i] = rand() %s 10;\n" % (currentBlockName.upper(), "%"))
        self.__rowsOpenMPRegions.append("	}\n")

        self.__rowsOpenMPRegions.append("\n")

        self.__rowsOpenMPRegions.append("		// Iteration counter for the current thread\n")
        self.__rowsOpenMPRegions.append("		unsigned int i_mp = 0;\n")
        self.__rowsOpenMPRegions.append("\n")

        # Initialize intermediate variables
        self.__rowsOpenMPRegions.append("	    // Initialize private intermediate variables\n")
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                self.__rowsOpenMPRegions.append("       	float openMP_%s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsOpenMPRegions.append("\n")

        self.__rowsOpenMPRegions.append("	    // Iterate/calculate through all the steps via RoundRobin fashion\n")
        self.__rowsOpenMPRegions.append("		for (i_mp = 0; i_mp < nElements; i_mp++) {\n")

        # Print intermediate variables

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                self.__rowsOpenMPRegions.append("	       openMP_%s = 0.0;\n" % (currentBlockName.upper()))
                self.__intermediateVariablesList.append("openMP_%s" % (currentBlockName.upper()))
        self.__rowsOpenMPRegions.append("\n")

        self.__rowsOpenMPRegions.append("	    	#pragma omp parallel sections private(")
        intermediateVariablesString = ', '.join(map(str, self.__intermediateVariablesList))
        self.__rowsOpenMPRegions.append(intermediateVariablesString)
        self.__rowsOpenMPRegions.append(")\n")
        self.__rowsOpenMPRegions.append("		    {\n")

        ##
        currentStage = -1
        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = self.getAnsiCBlockComputationString(block, False)
            currentBlockType = block.getBlockType()
            currentBlockStage = self.__optimizedPipelineStagesDictionary[block.getBlockName()]

            if currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = self.getAnsiInputBlockComputationString(block, "IN1", False)

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "(- 1.0) * %s;" % inputString
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s;" % inputString
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s);" % (block.getBlockOperator(), inputString)

                currentStage = self.checkAdvanceStage(currentStage,currentBlockStage)
                self.__rowsOpenMPRegions.append("				%s = %s\n" % (currentBlockName, operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = self.getAnsiInputBlockComputationString(block, "IN1", False)
                inputString2 = self.getAnsiInputBlockComputationString(block, "IN2", False)

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    currentStage = self.checkAdvanceStage(currentStage,currentBlockStage)
                    self.__rowsOpenMPRegions.append("				%s = pow(%s, %s);\n" % (currentBlockName, inputString1, inputString2))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    currentStage = self.checkAdvanceStage(currentStage,currentBlockStage)
                    self.__rowsOpenMPRegions.append("				%s = %s %s %s;\n" % (currentBlockName, inputString1, operatorString , inputString2))
        ##

        # Last line, store the last variable to the OUT global float
        lastBlock = self.__cbd.getBlocks()[-1].getBlockName().upper()

        self.__rowsOpenMPRegions.append("				openMP_OUT[i_mp] = openMP_%s;\n" % lastBlock)
        self.__rowsOpenMPRegions.append("		    	}\n")

        self.__rowsOpenMPRegions.append("	         }// OpenMP sections region ends here\n")
        self.__rowsOpenMPRegions.append("\n")

        self.__rowsOpenMPRegions.append("	      }\n")
        self.__rowsOpenMPRegions.append("\n")


        self.__rowsOpenMPRegions.append("	// Stop the clock\n")
        self.__rowsOpenMPRegions.append("	end = clock();\n")
        self.__rowsOpenMPRegions.append("	printf(\"Execution time(secs): %f\\n\\n\", (end-start)/(double)CLOCKS_PER_SEC );\n")
        self.__rowsOpenMPRegions.append("\n")
        self.__rowsOpenMPRegions.append("	// Validate the results -optional- should use if we already know the final results\n")
        self.__rowsOpenMPRegions.append("\n")
        self.__rowsOpenMPRegions.append("	int success = 1;\n")
        self.__rowsOpenMPRegions.append("	for (i = 0; i < nElements; i++) {  \n")
        self.__rowsOpenMPRegions.append("		if ( openMP_OUT[i] != %s) {\n" % self.__expectingResult)
        self.__rowsOpenMPRegions.append("			success = 0;\n")
        self.__rowsOpenMPRegions.append("			printf(\"openMP_OUT = %f\\n\", openMP_OUT[i]);\n")
        self.__rowsOpenMPRegions.append("			fprintf(stderr, \"Validation failed at index %d\\n----------------------------------------\\n\", i);\n")
        self.__rowsOpenMPRegions.append("			break;\n")
        self.__rowsOpenMPRegions.append("		}\n")
        self.__rowsOpenMPRegions.append("	}\n")
        self.__rowsOpenMPRegions.append("\n")
        self.__rowsOpenMPRegions.append("	if (success) {\n")
        self.__rowsOpenMPRegions.append("		fprintf(stdout, \"Validation successful.\\n----------------------------------------\\n\");\n")
        self.__rowsOpenMPRegions.append("	}\n")
        self.__rowsOpenMPRegions.append("\n")

        self.dumpSaveResultsToCSV()

        self.__rowsOpenMPRegions.append("	return 0;\n")
        self.__rowsOpenMPRegions.append("}\n")

    def dumpSaveResultsToCSV(self):
        self.__rowsOpenMPRegions.append("   // Save results to CSV\n")
        self.__rowsOpenMPRegions.append("   FILE *fp;\n")
        self.__rowsOpenMPRegions.append("\n")

        csvFileSavePath = self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName

        self.__rowsOpenMPRegions.append("   if (argc > 2) {\n")
        self.__rowsOpenMPRegions.append("       fp = fopen(argv[2], \"a+\");\n")
        self.__rowsOpenMPRegions.append("       fprintf(fp, \"PipeLineOpenMP%s\");\n" % self.__csvSeparationCharacter)
        self.__rowsOpenMPRegions.append("       fprintf(fp, \"%s%s\", nElements);\n" % ("%d", self.__csvSeparationCharacter) )
        self.__rowsOpenMPRegions.append("       fprintf(fp, \"%s%s\");\n" % (self.__fileName, self.__csvSeparationCharacter))
        self.__rowsOpenMPRegions.append("       fprintf(fp, \"%s%s\", success);\n" % ("%d", self.__csvSeparationCharacter))
        self.__rowsOpenMPRegions.append("       fprintf(fp, \"%f\\n\", (end-start)/(double)CLOCKS_PER_SEC);\n")
        self.__rowsOpenMPRegions.append("       fclose(fp);\n")
        self.__rowsOpenMPRegions.append("       printf(\"Saved results in %s\\n\", argv[2]);\n")
        self.__rowsOpenMPRegions.append("   }\n")

    def walkOpenMP(self):

        # Clean the output list of lines
        self.__rowsOpenMPRegions = []

        # Dump the makefile
        self.dumpOpenMP()

        return "".join(self.__rowsOpenMPRegions)

    def toDisk(self):

        outputString = self.walkOpenMP()
        text_file = open(self.__directoryPath + "/" + "outputOpenMPSections" + ".c", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output OpenMP Regions src on %s." % self.__directoryPath