__author__ = 'madks_000'

import os

class CBDIntelTBBPipelineStagesCompiler():
    """
    A class that generates the Intel TBB CPU Pipeline src code
    """
    def __init__(self, compilerSettings):
        self.__blockNamesList = []
        self.__rowsIntelTBBPipeline = []

        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__rand = compilerSettings.getRand()
        self.__cbd = compilerSettings.getCBD()
        self.__signalCount = compilerSettings.getSignalCount()
        self.__iterations = compilerSettings.getIterations()
        self.__blockNamesList = compilerSettings.getBlockNames()
        self.__intermediateVariablesList = []
        self.__optimizedPipelineStagesDictionary = compilerSettings.getOptimizedPipelineStagesDictionary()
        self.__expectingResult = compilerSettings.getExpectingResult()
        self.__benchmarkFileName = compilerSettings.getBenchmarkFileName()
        self.__currentPath = compilerSettings.getCurrentPath()
        self.__fileName = compilerSettings.getFileName()
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()
        self.__gaussianSolverInput = compilerSettings.getGaussianSolverInput()
        self.__strongComponentNames = compilerSettings.getStrongComponentNames()
        self.__numTokens = 4
        self.__numStages = 4

    def getIterations(self):
        return self.__iterations

    def dumpGaussianSolver(self):
        self.__rowsIntelTBBPipeline.append("#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))\n")
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("void swap_row(float *a, float *b, int r1, int r2, int n)\n")
        self.__rowsIntelTBBPipeline.append("{\n")
        self.__rowsIntelTBBPipeline.append("	float tmp, *p1, *p2;\n")
        self.__rowsIntelTBBPipeline.append("	int i;\n")
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("	if (r1 == r2) return;\n")
        self.__rowsIntelTBBPipeline.append("	for (i = 0; i < n; i++) {\n")
        self.__rowsIntelTBBPipeline.append("		p1 = mat_elem(a, r1, i, n);\n")
        self.__rowsIntelTBBPipeline.append("		p2 = mat_elem(a, r2, i, n);\n")
        self.__rowsIntelTBBPipeline.append("		tmp = *p1, *p1 = *p2, *p2 = tmp;\n")
        self.__rowsIntelTBBPipeline.append("	}\n")
        self.__rowsIntelTBBPipeline.append("	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;\n")
        self.__rowsIntelTBBPipeline.append("}\n")
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("void gauss_eliminate(float *a, float *b, float *x, int n)\n")
        self.__rowsIntelTBBPipeline.append("{\n")
        self.__rowsIntelTBBPipeline.append("#define A(y, x) (*mat_elem(a, y, x, n))\n")
        self.__rowsIntelTBBPipeline.append("	int i, j, col, row, max_row,dia;\n")
        self.__rowsIntelTBBPipeline.append("	float max, tmp;\n")
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("	for (dia = 0; dia < n; dia++) {\n")
        self.__rowsIntelTBBPipeline.append("		max_row = dia, max = A(dia, dia);\n")
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("		for (row = dia + 1; row < n; row++)\n")
        self.__rowsIntelTBBPipeline.append("			if ((tmp = fabs(A(row, dia))) > max)\n")
        self.__rowsIntelTBBPipeline.append("				max_row = row, max = tmp;\n")
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("		swap_row(a, b, dia, max_row, n);\n")
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("		for (row = dia + 1; row < n; row++) {\n")
        self.__rowsIntelTBBPipeline.append("			tmp = A(row, dia) / A(dia, dia);\n")
        self.__rowsIntelTBBPipeline.append("			for (col = dia+1; col < n; col++)\n")
        self.__rowsIntelTBBPipeline.append("				A(row, col) -= tmp * A(dia, col);\n")
        self.__rowsIntelTBBPipeline.append("			A(row, dia) = 0;\n")
        self.__rowsIntelTBBPipeline.append("			b[row] -= tmp * b[dia];\n")
        self.__rowsIntelTBBPipeline.append("		}\n")
        self.__rowsIntelTBBPipeline.append("	}\n")
        self.__rowsIntelTBBPipeline.append("	for (row = n - 1; row >= 0; row--) {\n")
        self.__rowsIntelTBBPipeline.append("		tmp = b[row];\n")
        self.__rowsIntelTBBPipeline.append("		for (j = n - 1; j > row; j--)\n")
        self.__rowsIntelTBBPipeline.append("			tmp -= x[j] * A(row, j);\n")
        self.__rowsIntelTBBPipeline.append("		x[row] = tmp / A(row, row);\n")
        self.__rowsIntelTBBPipeline.append("	}\n")
        self.__rowsIntelTBBPipeline.append("#undef A\n")
        self.__rowsIntelTBBPipeline.append("}\n")
        self.__rowsIntelTBBPipeline.append("\n")

    def dumpAlgebraicCBDLoopSolver(self):
        self.__rowsIntelTBBPipeline.append("void getSolution(float *coefficients, float *result)\n")
        self.__rowsIntelTBBPipeline.append("{\n")

        self.__rowsIntelTBBPipeline.append("	float a[] = {")
        for i, currentRow in enumerate(self.__gaussianSolverInput[0]):
            if i > 0:
                self.__rowsIntelTBBPipeline.append(",")
            self.__rowsIntelTBBPipeline.append("%s" % (','.join(map(str, currentRow))))
        self.__rowsIntelTBBPipeline.append("};\n")
        self.__rowsIntelTBBPipeline.append("	gauss_eliminate(a, coefficients, result, %s);\n" % len(self.__gaussianSolverInput[1]))
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("}\n")

    def dumpAlgebraicCBDLoopSolver(self):
        self.__rowsIntelTBBPipeline.append("void getSolution(float *coefficients, float *result)\n")
        self.__rowsIntelTBBPipeline.append("{\n")

        self.__rowsIntelTBBPipeline.append("	float a[] = {")
        for i, currentRow in enumerate(self.__gaussianSolverInput[0]):
            if i > 0:
                self.__rowsIntelTBBPipeline.append(",")
            self.__rowsIntelTBBPipeline.append("%s" % (','.join(map(str, currentRow))))
        self.__rowsIntelTBBPipeline.append("};\n")
        self.__rowsIntelTBBPipeline.append("	gauss_eliminate(a, coefficients, result, %s);\n" % len(self.__gaussianSolverInput[1]))
        self.__rowsIntelTBBPipeline.append(" \n")
        self.__rowsIntelTBBPipeline.append("}\n")

    def getAnsiInputBlockComputationString(self, block, port, ansiC=True):
        inputType = block.getLinksIn()[port][0].getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "intelTBB_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_tbb]"

        return returningString

    def getAnsiCBlockComputationString(self, block, ansiC=True):
        inputType = block.getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "intelTBB_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_tbb]"

        return returningString

    def checkAdvanceStage(self, currentStage, currentBlockStage):
        if currentStage is not currentBlockStage:
            if currentStage == 0:
                self.__rowsIntelTBBPipeline.append("                    return i_tbb++;\n")
                self.__rowsIntelTBBPipeline.append("                 } else {\n")
                self.__rowsIntelTBBPipeline.append("                    fc.stop();\n")
                self.__rowsIntelTBBPipeline.append("                    return NULL;\n")
                self.__rowsIntelTBBPipeline.append("                }\n")
                self.__rowsIntelTBBPipeline.append("            }\n")
                self.__rowsIntelTBBPipeline.append("        ) &\n")
                self.__rowsIntelTBBPipeline.append("		// First Stage end\n")
            else:
                self.__rowsIntelTBBPipeline.append("                return i_tbb;\n")
                self.__rowsIntelTBBPipeline.append("            }\n")
                self.__rowsIntelTBBPipeline.append("        ) &\n")
                self.__rowsIntelTBBPipeline.append("        // Stage %s End\n" % currentStage)

            currentStage = currentBlockStage

            if currentStage == (self.__numStages - 1):
                self.__rowsIntelTBBPipeline.append("        // Last Stage %s start\n" % currentStage)
                self.__rowsIntelTBBPipeline.append("        make_filter<int,void>(\n")
                self.__rowsIntelTBBPipeline.append("            filter::serial_in_order,\n")
                self.__rowsIntelTBBPipeline.append("            [&](int i_tbb) {\n")
            else:
                self.__rowsIntelTBBPipeline.append("        // Stage %s start\n" % currentStage)
                self.__rowsIntelTBBPipeline.append("        make_filter<int,int>(\n")
                self.__rowsIntelTBBPipeline.append("            filter::serial_in_order,\n")
                self.__rowsIntelTBBPipeline.append("            [&](int i_tbb){\n")

        return currentStage

    def dumpIntelTBB(self):
        self.__rowsIntelTBBPipeline.append("/*\n")
        self.__rowsIntelTBBPipeline.append("	Intel TBB Pipeline output src code from the CBD model simulation for mac\n")
        self.__rowsIntelTBBPipeline.append("*/\n")
        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("#include <algorithm>\n")
        self.__rowsIntelTBBPipeline.append("#include <iostream>\n")
        self.__rowsIntelTBBPipeline.append("#include <string.h>\n")
        self.__rowsIntelTBBPipeline.append("#include <time.h>\n")
        self.__rowsIntelTBBPipeline.append("#include <math.h>\n")
        self.__rowsIntelTBBPipeline.append("#include \"tbb/pipeline.h\"\n")
        self.__rowsIntelTBBPipeline.append("#include \"tbb/tick_count.h\"\n")
        self.__rowsIntelTBBPipeline.append("#include \"tbb/cache_aligned_allocator.h\"\n")
        self.__rowsIntelTBBPipeline.append("\n")

        self.__rowsIntelTBBPipeline.append("// The number of total simulation steps\n")
        self.__rowsIntelTBBPipeline.append("#define NELEMENTS %d\n" % self.getIterations())

        self.__rowsIntelTBBPipeline.append("// The number of maxinum tokens\n")
        self.__rowsIntelTBBPipeline.append("#define MAX_TOKENS %d\n" % self.__numTokens)
        self.__rowsIntelTBBPipeline.append("\n")

        self.__rowsIntelTBBPipeline.append("using namespace std;\n")
        self.__rowsIntelTBBPipeline.append("using namespace tbb;\n")


        if len(self.__strongComponentNames) > 0:
            self.dumpGaussianSolver()
            self.dumpAlgebraicCBDLoopSolver()

        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("// Main execution path\n")
        self.__rowsIntelTBBPipeline.append("int main (int argc, char* const *argv) {\n")
        self.__rowsIntelTBBPipeline.append("\n")

        self.__rowsIntelTBBPipeline.append("	// Memory allocators to avoid false sharing\n")
        self.__rowsIntelTBBPipeline.append("	tbb::cache_aligned_allocator<float> floatCacheAlignedAllocator;\n")
        self.__rowsIntelTBBPipeline.append("	tbb::cache_aligned_allocator<unsigned int> uIntCacheAlignedAllocator;\n")
        self.__rowsIntelTBBPipeline.append("\n")

        # Create Intel TBB local variables
        self.__rowsIntelTBBPipeline.append("	// Create the input/constant variables\n")
        self.__rowsIntelTBBPipeline.append("	unsigned int nElements = *uIntCacheAlignedAllocator.allocate(1);\n")
        self.__rowsIntelTBBPipeline.append("	unsigned int i_tbb = *uIntCacheAlignedAllocator.allocate(1);\n")
        self.__rowsIntelTBBPipeline.append("	nElements = NELEMENTS;\n")
        self.__rowsIntelTBBPipeline.append("	i_tbb = 0;\n")
        self.__rowsIntelTBBPipeline.append("\n")

        self.__rowsIntelTBBPipeline.append("    if (argc > 1) {\n")
        self.__rowsIntelTBBPipeline.append("        nElements = atoi(argv[1]);\n")
        self.__rowsIntelTBBPipeline.append("    }\n")

        self.__rowsIntelTBBPipeline.append("	printf(\"============ TBB PIPELINE ==============\\nNumber of discrete time steps: %d\\n\\n\",nElements);	\n")
        self.__rowsIntelTBBPipeline.append("	\n")
        self.__rowsIntelTBBPipeline.append("	printf(\"Using: IntelTBB & Optimized Pipeline\\n\");\n")
        self.__rowsIntelTBBPipeline.append("	printf(\"Number of Tokens: %d\\n\", MAX_TOKENS);	\n")
        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("	// Start the execution time counter for OpenCL\n")
        self.__rowsIntelTBBPipeline.append("	tbb::tick_count mainStartTime = tbb::tick_count::now();\n")
        self.__rowsIntelTBBPipeline.append("	\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsIntelTBBPipeline.append("	float *intelTBB_%s = floatCacheAlignedAllocator.allocate(nElements);\n" % currentBlockName.upper())
        self.__rowsIntelTBBPipeline.append("	float *intelTBB_OUT = floatCacheAlignedAllocator.allocate(nElements);\n")

        if len(self.__strongComponentNames) > 0:
            # Allocate the coefficients array
            self.__rowsIntelTBBPipeline.append("	// Allocate the coefficients array\n")
            self.__rowsIntelTBBPipeline.append("	float *x = floatCacheAlignedAllocator.allocate(%s);\n" % len(self.__gaussianSolverInput[1]))
            self.__rowsIntelTBBPipeline.append("\n")

        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("	// Pack input variables with data\n")
        self.__rowsIntelTBBPipeline.append("	for (i_tbb = 0; i_tbb < nElements; i_tbb++) {\n")
        # Put constant data on Constant buffers
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if self.__rand == False:
                    self.__rowsIntelTBBPipeline.append("		intelTBB_%s[i_tbb] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))
                else:
                    self.__rowsIntelTBBPipeline.append("		intelTBB_%s[i_tbb] = rand() %s 10;\n" % (currentBlockName.upper(), "%"))
        self.__rowsIntelTBBPipeline.append("	}\n")

        self.__rowsIntelTBBPipeline.append("\n")


        # Initialize intermediate variables
        self.__rowsIntelTBBPipeline.append("    // Initialize private intermediate variables\n")
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                self.__rowsIntelTBBPipeline.append("    float intelTBB_%s = *floatCacheAlignedAllocator.allocate(1);\n" % (currentBlockName.upper()))
        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("    i_tbb = 0;\n")
        self.__rowsIntelTBBPipeline.append("\n")


        self.__rowsIntelTBBPipeline.append("    // Pipeline Start\n")
        self.__rowsIntelTBBPipeline.append("    parallel_pipeline( /*max_number_of_live_token=*/MAX_TOKENS,\n")
        self.__rowsIntelTBBPipeline.append("        // First Stage Start\n")
        self.__rowsIntelTBBPipeline.append("        tbb::make_filter<void,int>(\n")
        self.__rowsIntelTBBPipeline.append("            filter::serial_in_order,\n")
        self.__rowsIntelTBBPipeline.append("            [&](flow_control& fc)-> int{\n")
        self.__rowsIntelTBBPipeline.append("                if(i_tbb < nElements) {\n")

        if len(self.__strongComponentNames) > 0:
            strOut = []
            for elementName in self.__gaussianSolverInput[3]:
                if not elementName:
                    strOut.append("0.0")
                else:
                    strOut.append("- intelTBB_%s[i_tbb]" % elementName.upper())

        self.__rowsIntelTBBPipeline.append("\n")

        if len(self.__strongComponentNames) > 0:
            self.__rowsIntelTBBPipeline.append("					//Solve the algebraic loop\n")
            self.__rowsIntelTBBPipeline.append("					float coefficients[] = { %s };\n" % (', '.join(map(str, strOut))))
            self.__rowsIntelTBBPipeline.append("					float x[%s];\n" % len(self.__gaussianSolverInput[1]))
            self.__rowsIntelTBBPipeline.append("					getSolution(coefficients, x);\n")
            self.__rowsIntelTBBPipeline.append("\n")

        # Print intermediate variables
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                if len(self.__strongComponentNames) > 0:
                    if currentBlockName in self.__strongComponentNames:
                        self.__rowsIntelTBBPipeline.append("					intelTBB_%s = x[%s];\n" % (currentBlockName.upper(), self.__strongComponentNames.index(currentBlockName)))

                else:
                    self.__rowsIntelTBBPipeline.append("					intelTBB_%s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsIntelTBBPipeline.append("\n")

        ##
        currentStage = 0
        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = self.getAnsiCBlockComputationString(block, False)
            currentBlockType = block.getBlockType()
            currentBlockStage = self.__optimizedPipelineStagesDictionary[block.getBlockName()]

            if currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = self.getAnsiInputBlockComputationString(block, "IN1", False)

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "(- 1.0) * %s;" % inputString
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s;" % inputString
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s);" % (block.getBlockOperator(), inputString)

                currentStage = self.checkAdvanceStage(currentStage,currentBlockStage)
                self.__rowsIntelTBBPipeline.append("				%s = %s\n" % (currentBlockName, operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = self.getAnsiInputBlockComputationString(block, "IN1", False)
                inputString2 = self.getAnsiInputBlockComputationString(block, "IN2", False)

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    currentStage = self.checkAdvanceStage(currentStage,currentBlockStage)
                    self.__rowsIntelTBBPipeline.append("				%s = pow(%s, %s);\n" % (currentBlockName, inputString1, inputString2))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    currentStage = self.checkAdvanceStage(currentStage,currentBlockStage)
                    self.__rowsIntelTBBPipeline.append("				%s = %s %s %s;\n" % (currentBlockName, inputString1, operatorString , inputString2))
        ##

        # Last line, store the last variable to the OUT global float
        lastBlock = self.__cbd.getBlocks()[-1].getBlockName().upper()

        self.__rowsIntelTBBPipeline.append("				intelTBB_OUT[i_tbb] = intelTBB_%s;\n" % lastBlock)

        self.__rowsIntelTBBPipeline.append("            }\n")
        self.__rowsIntelTBBPipeline.append("        )\n")
        self.__rowsIntelTBBPipeline.append("        // Last Stage %s End\n" % currentStage)
        self.__rowsIntelTBBPipeline.append("    );\n")


        self.__rowsIntelTBBPipeline.append("	// Stop the clock\n")
        self.__rowsIntelTBBPipeline.append("	double end = (double)(tbb::tick_count::now() - mainStartTime).seconds();\n")
        self.__rowsIntelTBBPipeline.append("	printf(\"Execution time(secs): %f\\n\\n\", end);\n")
        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("	// Validate the results -optional- should use if we already know the final results\n")
        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("	int success = 1;\n")
        self.__rowsIntelTBBPipeline.append("	for (i_tbb = 0; i_tbb < nElements; i_tbb++) {  \n")
        self.__rowsIntelTBBPipeline.append("		if ( intelTBB_OUT[i_tbb] != %s) {\n" % self.__expectingResult)
        self.__rowsIntelTBBPipeline.append("			success = 0;\n")
        self.__rowsIntelTBBPipeline.append("			printf(\"intelTBB_OUT = %f\\n\", intelTBB_OUT[i_tbb]);\n")
        self.__rowsIntelTBBPipeline.append("			fprintf(stderr, \"Validation failed at index %d\\n----------------------------------------\\n\", i_tbb);\n")
        self.__rowsIntelTBBPipeline.append("			break;\n")
        self.__rowsIntelTBBPipeline.append("		}\n")
        self.__rowsIntelTBBPipeline.append("	}\n")
        self.__rowsIntelTBBPipeline.append("\n")
        self.__rowsIntelTBBPipeline.append("	if (success) {\n")
        self.__rowsIntelTBBPipeline.append("		fprintf(stdout, \"Validation successful.\\n----------------------------------------\\n\");\n")
        self.__rowsIntelTBBPipeline.append("	}\n")
        self.__rowsIntelTBBPipeline.append("\n")

        self.dumpSaveResultsToCSV()

        self.__rowsIntelTBBPipeline.append("	return 0;\n")
        self.__rowsIntelTBBPipeline.append("}\n")

    def dumpSaveResultsToCSV(self):
        self.__rowsIntelTBBPipeline.append("   // Save results to CSV\n")
        self.__rowsIntelTBBPipeline.append("   FILE *fp;\n")
        self.__rowsIntelTBBPipeline.append("\n")

        csvFileSavePath = self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName

        self.__rowsIntelTBBPipeline.append("   if (argc > 2) {\n")
        self.__rowsIntelTBBPipeline.append("       fp = fopen(argv[2], \"a+\");\n")
        self.__rowsIntelTBBPipeline.append("       fprintf(fp, \"PipeLineIntelTBB%s\");\n" % self.__csvSeparationCharacter)
        self.__rowsIntelTBBPipeline.append("       fprintf(fp, \"%s%s\", nElements);\n" % ("%d", self.__csvSeparationCharacter) )
        self.__rowsIntelTBBPipeline.append("       fprintf(fp, \"%s%s\");\n" % (self.__fileName, self.__csvSeparationCharacter))
        self.__rowsIntelTBBPipeline.append("       fprintf(fp, \"%s%s\", success);\n" % ("%d", self.__csvSeparationCharacter))
        self.__rowsIntelTBBPipeline.append("       fprintf(fp, \"%f\\n\", end);\n")
        self.__rowsIntelTBBPipeline.append("       fclose(fp);\n")
        self.__rowsIntelTBBPipeline.append("       printf(\"Saved results in %s\\n\", argv[2]);\n")
        self.__rowsIntelTBBPipeline.append("   }\n")

    def walkIntelTBB(self):

        # Clean the output list of lines
        self.__rowsIntelTBBPipeline = []

        # Dump the makefile
        self.dumpIntelTBB()

        return "".join(self.__rowsIntelTBBPipeline)

    def toDisk(self):

        outputString = self.walkIntelTBB()
        text_file = open(self.__directoryPath + "/" + "outputIntelTBB" + ".cpp", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output Intel TBB Pipeline src on %s." % self.__directoryPath