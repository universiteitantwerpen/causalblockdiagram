__author__ = 'madks_000'

import os

class CBDOpenGLCompiler():
    """
    A class that generates the OpenGL src code that renders the input and output Signals to a 3D cartesian system
    """
    def __init__(self, compilerSettings):
        self.__blockNamesList = []
        self.__rowsOpenGL = []

        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__rand = compilerSettings.getRand()
        self.__cbd = compilerSettings.getCBD()
        self.__signalCount = compilerSettings.getSignalCount()
        self.__iterations = compilerSettings.getIterations()
        self.__blockNamesList = compilerSettings.getBlockNames()

    def getAnsiInputBlockComputationString(self, block, port, ansiC=True):
        inputType = block.getLinksIn()[port][0].getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def getAnsiCBlockComputationString(self, block, ansiC=True):
        inputType = block.getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def dumpOpenGL(self):
        self.__rowsOpenGL.append("/*\n")
        self.__rowsOpenGL.append("	Generated OpenGL code to display a 3D graph of CBD models\n")
        self.__rowsOpenGL.append("*/\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("#include <stdio.h>\n")
        self.__rowsOpenGL.append("#include <stdarg.h>\n")
        self.__rowsOpenGL.append("#include <math.h>\n")
        self.__rowsOpenGL.append("#define GL_GLEXT_PROTOTYPES\n")
        self.__rowsOpenGL.append("#ifdef __APPLE__\n")
        self.__rowsOpenGL.append("#include <GLUT/glut.h>\n")
        self.__rowsOpenGL.append("#else\n")
        self.__rowsOpenGL.append("#include <GL/glut.h>\n")
        self.__rowsOpenGL.append("#endif\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Constants\n")
        self.__rowsOpenGL.append("#define xMIN 0.0\n")
        self.__rowsOpenGL.append("#define xMAX 10.0\n")
        self.__rowsOpenGL.append("#define yMIN 0.0\n")
        self.__rowsOpenGL.append("#define yMAX 10.0\n")
        self.__rowsOpenGL.append("#define zMIN 0.0\n")
        self.__rowsOpenGL.append("#define zMAX 10.0\n")
        self.__rowsOpenGL.append("#define zSTEP 0.1\n")
        self.__rowsOpenGL.append("#define WINDOW_WIDTH 800\n")
        self.__rowsOpenGL.append("#define WINDOW_HEIGHT 800\n")
        self.__rowsOpenGL.append("#define TIME_STEP 0.05\n")
        self.__rowsOpenGL.append("#define VALUES_MULTIPLIER 10.0\n")
        self.__rowsOpenGL.append("#define GRAPH_STEP 0.1\n")
        self.__rowsOpenGL.append("#define GRAPH_MIN 0.0\n")
        self.__rowsOpenGL.append("#define GRAPH_MAX 10.0\n")
        self.__rowsOpenGL.append("#define GRID_MAX 1.0\n")
        self.__rowsOpenGL.append("#define GRID_STEP 0.1\n")
        self.__rowsOpenGL.append("#define SIGNAL_SIZE_MAX 200\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Function Prototypes\n")
        self.__rowsOpenGL.append("void display();\n")
        self.__rowsOpenGL.append("void specialKeys();\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Global Variables\n")
        self.__rowsOpenGL.append("float rotate_y= -20.0; // Starting x, y camera angle\n")
        self.__rowsOpenGL.append("float rotate_x= 70.0;\n")
        self.__rowsOpenGL.append("float angle=0.0; // angle of rotation for the camera direction\n")
        self.__rowsOpenGL.append("float lx=0.0f,lz=-1.0f; // actual vector representing the camera's direction\n")
        self.__rowsOpenGL.append("float x=0.5f,z=0.5f, y = 0.0f; // XZ position of the camera\n")
        self.__rowsOpenGL.append("float scalePercentage = 1.0;\n")
        self.__rowsOpenGL.append("int showGrid = 1; // Toggle grid on/off\n")
        self.__rowsOpenGL.append("int showAreas = 1; // Toggle grid on/off\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Helpful function to draw a char* array of characters\n")
        self.__rowsOpenGL.append("void drawString (void * font, char *s, float x, float y, float z) {\n")
        self.__rowsOpenGL.append("	unsigned int i;\n")
        self.__rowsOpenGL.append("	glRasterPos3f(x, y, z);\n")
        self.__rowsOpenGL.append("	for (i = 0; i < strlen (s); i++)\n")
        self.__rowsOpenGL.append("		glutBitmapCharacter (font, s[i]);\n")
        self.__rowsOpenGL.append("}\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Helps in looping through all the colours based on ID\n")
        self.__rowsOpenGL.append("void setColour(int colour) {\n")
        self.__rowsOpenGL.append("	switch(colour) \n")
        self.__rowsOpenGL.append("	{\n")
        self.__rowsOpenGL.append("		case 0: // White\n")
        self.__rowsOpenGL.append("			glColor3f(1.0, 1.0, 1.0);\n")
        self.__rowsOpenGL.append("			break;\n")
        self.__rowsOpenGL.append("		case 1: // Green\n")
        self.__rowsOpenGL.append("			glColor3f(0.0, 1.0, 0.0);\n")
        self.__rowsOpenGL.append("			break;\n")
        self.__rowsOpenGL.append("		case 2: // Blue\n")
        self.__rowsOpenGL.append("			glColor3f(0.0, 0.0, 1.0);\n")
        self.__rowsOpenGL.append("			break;\n")
        self.__rowsOpenGL.append("		case 3: // Yellow\n")
        self.__rowsOpenGL.append("			glColor3f(1.0, 1.0, 0.0);\n")
        self.__rowsOpenGL.append("			break;\n")
        self.__rowsOpenGL.append("		case 4: //\n")
        self.__rowsOpenGL.append("			glColor3f(0.0, 1.0, 1.0);\n")
        self.__rowsOpenGL.append("			break;\n")
        self.__rowsOpenGL.append("		case 5: //\n")
        self.__rowsOpenGL.append("			glColor3f(1.0, 0.0, 1.0);\n")
        self.__rowsOpenGL.append("			break;\n")
        self.__rowsOpenGL.append("		case -1: // Red\n")
        self.__rowsOpenGL.append("			glColor3f(1.0, 0.0, 0.0);\n")
        self.__rowsOpenGL.append("			break;\n")
        self.__rowsOpenGL.append("	}\n")
        self.__rowsOpenGL.append("}\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Draw the 3d Cartesian system without any points\n")
        self.__rowsOpenGL.append("void draw3DCartesianSystem() {\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Print stationary help text\n")
        self.__rowsOpenGL.append("	glColor3f(1.0, 1.0, 1.0);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_TIMES_ROMAN_24, \"Signals vs Time Steps\", x -0.2, y + 1.2, z); // Chart Legend	\n")
        self.__rowsOpenGL.append("	glColor3f(1.0, 1.0, 1.0);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_12, \"Rotation: UP, DOWN, LEFT, RIGHT keys.\", x - 0.9, y - 1.35, z);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_12, \"Scale IN/OUT: 1, 2 keys.\", x - 0.9, y - 1.3, z);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_12, \"Toggle grid: 3 key.\", x - 0.9, y - 1.25, z);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_12, \"Toggle 2D Signal Areas: 4 key.\", x - 0.9, y - 1.4, z);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Rotate when user changes rotate_x and rotate_y angles\n")
        self.__rowsOpenGL.append("	glRotatef(rotate_x, 2.0, 0.0, 0.0 );\n")
        self.__rowsOpenGL.append("	glRotatef(rotate_y, 0.0, 2.0, 0.0 );\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Set the zoom scale\n")
        self.__rowsOpenGL.append("	glScalef(scalePercentage, scalePercentage, scalePercentage);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Draw x, y, z Axis\n")
        self.__rowsOpenGL.append("	glLineWidth(4);\n")
        self.__rowsOpenGL.append("	glBegin(GL_LINES);	\n")
        self.__rowsOpenGL.append("	// Axis X\n")
        self.__rowsOpenGL.append("	glColor3f(1.0, 0.0, 0.0);\n")
        self.__rowsOpenGL.append("	glVertex3f(xMIN, 0.0, 0.0);\n")
        self.__rowsOpenGL.append("	glVertex3f(xMAX, 0.0, 0.0);	\n")
        self.__rowsOpenGL.append("	// Axis Y\n")
        self.__rowsOpenGL.append("	glColor3f(1.0, 1.0, 0.0);\n")
        self.__rowsOpenGL.append("	glVertex3f(0.0, yMIN, 0.0);\n")
        self.__rowsOpenGL.append("	glVertex3f(0.0, yMAX, 0.0);\n")
        self.__rowsOpenGL.append("	// Axis Z\n")
        self.__rowsOpenGL.append("	glColor3f(0.0, 0.0, 1.0);\n")
        self.__rowsOpenGL.append("	glVertex3f(0.0, 0.0, zMIN);\n")
        self.__rowsOpenGL.append("	glVertex3f(0.0, 0.0, zMAX); \n")
        self.__rowsOpenGL.append("	glEnd();\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Draw Axis labels\n")
        self.__rowsOpenGL.append("	glColor3f(1.0,  1.0,  1.0);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_18, \"x (Time Step)\", 1.1, -0.2, 0);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_18, \"y (Value)\", -0.2, 1.1, 0);\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_18, \"z (Signal)\", 0, -0.2, 1.1); \n")
        self.__rowsOpenGL.append("\n")

        self.__rowsOpenGL.append("	// Draw Grid lines\n")
        self.__rowsOpenGL.append("	if (showGrid == 1) 	{\n")
        self.__rowsOpenGL.append("		glLineWidth(1);\n")
        self.__rowsOpenGL.append("		glBegin(GL_LINES);	\n")
        self.__rowsOpenGL.append("		for (float i = GRAPH_MIN; i < GRAPH_MAX; i+=0.1) {\n")
        self.__rowsOpenGL.append("			// Plane X\n")
        self.__rowsOpenGL.append("		    glColor3f(0.0, 1.0, 0.0);\n")
        self.__rowsOpenGL.append("			glVertex3f(xMIN, i, 0.0);\n")
        self.__rowsOpenGL.append("			glVertex3f(xMAX, i, 0.0);\n")
        self.__rowsOpenGL.append("			if ((int)(i * 10) % 2 == 0) {\n")
        self.__rowsOpenGL.append("				glVertex3f(i, yMIN, 0.0);\n")
        self.__rowsOpenGL.append("				glVertex3f(i, yMAX, 0.0);\n")
        self.__rowsOpenGL.append("			}\n")
        self.__rowsOpenGL.append("			// Plane Z\n")
        self.__rowsOpenGL.append("		    glColor3f(0.0, 0.0, 1.0);\n")
        self.__rowsOpenGL.append("			glVertex3f(zMIN, 0.0, i);\n")
        self.__rowsOpenGL.append("			glVertex3f(zMAX, 0.0, i); \n")
        self.__rowsOpenGL.append("			if ((int)(i * 10) % 2 == 0.0) {\n")
        self.__rowsOpenGL.append("				glVertex3f(i, 0.0, zMIN);\n")
        self.__rowsOpenGL.append("				glVertex3f(i, 0.0, zMAX); \n")
        self.__rowsOpenGL.append("			}\n")
        self.__rowsOpenGL.append("		}\n")
        self.__rowsOpenGL.append("		glEnd();\n")
        self.__rowsOpenGL.append("	}\n")

        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Draw axis division digit labels\n")
        self.__rowsOpenGL.append("	glColor3f(1.0,  1.0,  0.0);	\n")
        self.__rowsOpenGL.append("	for (float i = xMIN; i <= xMAX; i+=GRAPH_STEP) {\n")
        self.__rowsOpenGL.append("		char digits[5];\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("		snprintf(digits, sizeof(digits), \"%f\", i * VALUES_MULTIPLIER);\n")
        self.__rowsOpenGL.append("		drawString(GLUT_BITMAP_HELVETICA_10, digits, i, -0.05, 0);\n")
        self.__rowsOpenGL.append("		drawString(GLUT_BITMAP_HELVETICA_10, digits, -0.05, i, 0);\n")
        self.__rowsOpenGL.append("	}\n")
        self.__rowsOpenGL.append("}\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Helpful function to draw CBD signals from a float array\n")
        self.__rowsOpenGL.append("void drawSignal(char* label, float* points, int signalID) {\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	setColour(signalID % 6);// Set Colour rotation based on ID\n")
        self.__rowsOpenGL.append("	\n")
        self.__rowsOpenGL.append("	drawString(GLUT_BITMAP_HELVETICA_10, label, -0.1, 0.0, zSTEP * signalID); // Label\n")
        self.__rowsOpenGL.append("	\n")
        self.__rowsOpenGL.append("	// Draw points\n")
        self.__rowsOpenGL.append("	glBegin(GL_POINTS);\n")
        self.__rowsOpenGL.append("	glPushMatrix();\n")
        self.__rowsOpenGL.append("	glTranslated(0.0, 0.0, 0.0); // draw an empty circle, hack to remove a bug\n")
        self.__rowsOpenGL.append("	glutSolidSphere(0.0, 50.0, 50.0);\n")
        self.__rowsOpenGL.append("	glPopMatrix(); \n")
        self.__rowsOpenGL.append("	\n")
        self.__rowsOpenGL.append("	for (unsigned int i = 0; i < SIGNAL_SIZE_MAX; i++) {\n")
        self.__rowsOpenGL.append("		glPushMatrix();\n")
        self.__rowsOpenGL.append("		glTranslated((float)(TIME_STEP * i), points[i] / VALUES_MULTIPLIER, zSTEP * signalID);\n")
        self.__rowsOpenGL.append("		glutWireSphere(0.01, 50.0, 50.0);\n")
        self.__rowsOpenGL.append("		glPopMatrix(); \n")
        self.__rowsOpenGL.append("	}	\n")
        self.__rowsOpenGL.append("	glEnd();\n")
        self.__rowsOpenGL.append("	\n")

        self.__rowsOpenGL.append("	//Draw Lines or 2D Areas with the xX' axis\n")
        self.__rowsOpenGL.append("	unsigned int first = 0; // ID of the previous points' y value	\n")
        self.__rowsOpenGL.append("	if (showAreas == 0)\n")
        self.__rowsOpenGL.append("	{\n")
        self.__rowsOpenGL.append("		// Connect each point with a line\n")
        self.__rowsOpenGL.append("		glLineWidth(1);\n")
        self.__rowsOpenGL.append("		glBegin(GL_LINES);\n")
        self.__rowsOpenGL.append("		glColor3f(0.8, 0.8, 1.0);\n")
        self.__rowsOpenGL.append("		setColour(signalID % 6);\n")
        self.__rowsOpenGL.append("		unsigned int first = 0; // ID of the previous points' y value\n")
        self.__rowsOpenGL.append("		for (unsigned int i = 1; i < SIGNAL_SIZE_MAX; i++) {\n")
        self.__rowsOpenGL.append("			glVertex3f((float)(TIME_STEP * (i - 1)), points[first] / VALUES_MULTIPLIER, zSTEP * signalID);\n")
        self.__rowsOpenGL.append("			glVertex3f((float)(TIME_STEP * i), points[i] / VALUES_MULTIPLIER, zSTEP * signalID);\n")
        self.__rowsOpenGL.append("			first = i;\n")
        self.__rowsOpenGL.append("		}\n")
        self.__rowsOpenGL.append("		glEnd();\n")
        self.__rowsOpenGL.append("	} else {\n")
        self.__rowsOpenGL.append("		// Draw trapezoids - integrals below each line\n")
        self.__rowsOpenGL.append("		glLineWidth(1);\n")
        self.__rowsOpenGL.append("		glBegin(GL_QUADS);\n")
        self.__rowsOpenGL.append("		glColor3f(0.8, 0.8, 1.0);\n")
        self.__rowsOpenGL.append("		setColour(signalID % 6);	\n")
        self.__rowsOpenGL.append("		for (unsigned int i = 1; i < SIGNAL_SIZE_MAX; i++) {\n")
        self.__rowsOpenGL.append("			glVertex3f((float)(TIME_STEP * (i - 1)), points[first] / VALUES_MULTIPLIER, zSTEP * signalID);\n")
        self.__rowsOpenGL.append("			glVertex3f((float)(TIME_STEP * i), points[i] / VALUES_MULTIPLIER, zSTEP * signalID);\n")
        self.__rowsOpenGL.append("			glVertex3f((float)(TIME_STEP * (i - 1)), 0.0f , zSTEP * signalID);\n")
        self.__rowsOpenGL.append("			glVertex3f((float)(TIME_STEP * i), 0.0f, zSTEP * signalID);\n")
        self.__rowsOpenGL.append("			first = i;\n")
        self.__rowsOpenGL.append("		}\n")
        self.__rowsOpenGL.append("		glEnd();\n")
        self.__rowsOpenGL.append("	}")
        self.__rowsOpenGL.append("}\n")
        self.__rowsOpenGL.append("\n")

        self.__rowsOpenGL.append("// Display Callback function\n")
        self.__rowsOpenGL.append("void display() {\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	//  Clear screen and Z-buffer\n")
        self.__rowsOpenGL.append("	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Reset transformations\n")
        self.__rowsOpenGL.append("	glLoadIdentity();\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Set the camera viewpoint\n")
        self.__rowsOpenGL.append("	gluLookAt(x, y, z,\n")
        self.__rowsOpenGL.append("		x+lx, 1.0f,  z+lz,\n")
        self.__rowsOpenGL.append("		0.0f, 2.0f,  0.0f);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Print helpful information to the console\n")
        self.__rowsOpenGL.append("	printf(\"scale, eyesx, eyesy, eyesz, rotate_x, rotate_y: %f %f %f %f %f \\n\", scalePercentage, x, y, z, rotate_x, rotate_y);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Draw the grid, axis, labels, divisions\n")
        self.__rowsOpenGL.append("	draw3DCartesianSystem();\n")
        self.__rowsOpenGL.append("\n")

        # Create Ansi C local variables
        self.__rowsOpenGL.append("	// Create the host (local) variables\n")
        self.__rowsOpenGL.append("	unsigned int i = 0;\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenGL.append("	float *ansiC_%s = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);\n" % currentBlockName.upper())
        self.__rowsOpenGL.append("	float *ansiC_OUT = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);\n")

        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Pack Ansi C variables with data\n")
        self.__rowsOpenGL.append("	for (i = 0; i < SIGNAL_SIZE_MAX; i++) {\n")
        # Put constant data on Constant buffers
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if self.__rand == False:
                    self.__rowsOpenGL.append("		ansiC_%s[i] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))
                else:
                    self.__rowsOpenGL.append("		ansiC_%s[i] = rand() %s 10;\n" % (currentBlockName.upper(), "%"))
        self.__rowsOpenGL.append("	}\n")

        self.__rowsOpenGL.append("\n")

        # Initialize intermediate variables
        self.__rowsOpenGL.append("	// Initialize intermediate variables\n")
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                self.__rowsOpenGL.append("	float ansiC_%s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsOpenGL.append("\n")

        self.__rowsOpenGL.append("	// Calculate data\n")
        self.__rowsOpenGL.append("	for (i = 0; i < SIGNAL_SIZE_MAX; i++) {\n")

        # Print intermediate variables
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                self.__rowsOpenGL.append("	    ansiC_%s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsOpenGL.append("\n")

        ##
        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = self.getAnsiCBlockComputationString(block)
            currentBlockType = block.getBlockType()

            if currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = self.getAnsiInputBlockComputationString(block, "IN1")

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "(- 1.0) * %s;" % inputString
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s;" % inputString
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s);" % (block.getBlockOperator(), inputString)

                self.__rowsOpenGL.append("        %s = %s\n" % (currentBlockName, operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = self.getAnsiInputBlockComputationString(block, "IN1")
                inputString2 = self.getAnsiInputBlockComputationString(block, "IN2")

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    self.__rowsOpenGL.append("        %s = pow(%s, %s);\n" % (currentBlockName, inputString1, inputString2))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    self.__rowsOpenGL.append("        %s = %s %s %s;\n" % (currentBlockName, inputString1, operatorString , inputString2))
        ##

        # Last line, store the last variable to the OUT global float
        lastBlock = self.__cbd.getBlocks()[-1].getBlockName().upper()
        self.__rowsOpenGL.append("        ansiC_OUT[i] = ansiC_%s;\n" % lastBlock)
        self.__rowsOpenGL.append("	}\n")
        self.__rowsOpenGL.append("\n")

        counter = 0
        self.__rowsOpenGL.append("	// Do draw all the signals\n")
        self.__rowsOpenGL.append("	drawSignal(\"OUT\", ansiC_OUT, %d);\n" % (counter))
        counter += 1
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenGL.append("	drawSignal(\"%s\", ansiC_%s, %d);\n" % (currentBlockName.upper(), currentBlockName.upper(), counter))
                counter += 1
        self.__rowsOpenGL.append("\n")

        self.__rowsOpenGL.append("	glFlush();\n")
        self.__rowsOpenGL.append("	glutSwapBuffers();\n")
        self.__rowsOpenGL.append("}\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Callback for key presses\n")
        self.__rowsOpenGL.append("void specialKeys( int key, int x, int y ) {\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Right arrow - increase rotation by 5 degrees\n")
        self.__rowsOpenGL.append("	if (key == GLUT_KEY_RIGHT)\n")
        self.__rowsOpenGL.append("		rotate_y += 5; \n")
        self.__rowsOpenGL.append("	// Left arrow - decrease rotation by 5 degree\n")
        self.__rowsOpenGL.append("	else if (key == GLUT_KEY_LEFT)\n")
        self.__rowsOpenGL.append("		rotate_y -= 5; \n")
        self.__rowsOpenGL.append("	else if (key == GLUT_KEY_UP)\n")
        self.__rowsOpenGL.append("		rotate_x -= 5; \n")
        self.__rowsOpenGL.append("	else if (key == GLUT_KEY_DOWN)\n")
        self.__rowsOpenGL.append("		rotate_x += 5;\n")
        self.__rowsOpenGL.append("	// Keys 1, 2 for scale in/out, 3 to toggle grid \n")
        self.__rowsOpenGL.append("	else if (key == 49)\n")
        self.__rowsOpenGL.append("		scalePercentage+=0.1;\n")
        self.__rowsOpenGL.append("	else if (key == 50)\n")
        self.__rowsOpenGL.append("		scalePercentage-=0.1;\n")
        self.__rowsOpenGL.append("	else if (key == 51)\n")
        self.__rowsOpenGL.append("		if (showGrid == 1)\n")
        self.__rowsOpenGL.append("			showGrid = 0;\n")
        self.__rowsOpenGL.append("		else\n")
        self.__rowsOpenGL.append("			showGrid = 1;\n")
        self.__rowsOpenGL.append("	else if (key == 52)\n")
        self.__rowsOpenGL.append("		if (showAreas == 1)\n")
        self.__rowsOpenGL.append("			showAreas = 0;\n")
        self.__rowsOpenGL.append("		else\n")
        self.__rowsOpenGL.append("			showAreas = 1;\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	//  Request display update\n")
        self.__rowsOpenGL.append("	glutPostRedisplay();\n")
        self.__rowsOpenGL.append("}\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("// Main execution point\n")
        self.__rowsOpenGL.append("int main(int argc, char* argv[]){\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Initialize GLUT and process user parameters\n")
        self.__rowsOpenGL.append("	glutInit(&argc,argv);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Request double buffered true color window with Z-buffer\n")
        self.__rowsOpenGL.append("	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Create window\n")
        self.__rowsOpenGL.append("	glutInitWindowPosition(100,100);	\n")
        self.__rowsOpenGL.append("	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);\n")
        self.__rowsOpenGL.append("	glutCreateWindow(\"CBD 3D Graph\");\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Enable Z-buffer depth test\n")
        self.__rowsOpenGL.append("	glEnable(GL_DEPTH_TEST);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Callback functions\n")
        self.__rowsOpenGL.append("	glutDisplayFunc(display);\n")
        self.__rowsOpenGL.append("	glutSpecialFunc(specialKeys);\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	// Pass control to GLUT for events\n")
        self.__rowsOpenGL.append("	glutMainLoop();\n")
        self.__rowsOpenGL.append("\n")
        self.__rowsOpenGL.append("	return 0;\n")
        self.__rowsOpenGL.append("}")

    def walkOpenGL(self):

        # Clean the output list of lines
        self.__rowsOpenGL = []

        # Dump the makefile
        self.dumpOpenGL()

        return "".join(self.__rowsOpenGL)

    def toDisk(self):

        outputString = self.walkOpenGL()
        text_file = open(self.__directoryPath + "/" + "outputOpenGL" + ".c", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output OpenGL src on %s." % self.__directoryPath