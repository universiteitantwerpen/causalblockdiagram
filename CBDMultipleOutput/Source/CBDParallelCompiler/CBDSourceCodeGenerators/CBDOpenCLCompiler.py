__author__ = 'madks_000'

import os

class CBDOpenCLCompiler():
    """
    A class that generates the Makefile required to build OpenCL code
    """
    def __init__(self, compilerSettings):
        self.__blockNamesList = []
        self.__rowsOpenCL = []

        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__rand = compilerSettings.getRand()
        self.__fileName = compilerSettings.getFileName()
        self.__cbd = compilerSettings.getCBD()
        self.__signalCount = compilerSettings.getSignalCount()
        self.__iterations = compilerSettings.getIterations()
        self.__blockNamesList = compilerSettings.getBlockNames()
        self.__expectingResult = compilerSettings.getExpectingResult()
        self.__benchmarkFileName = compilerSettings.getBenchmarkFileName()
        self.__currentPath = compilerSettings.getCurrentPath()
        self.__fileName = compilerSettings.getFileName()
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()

    def getIterations(self):
        return self.__iterations

    def getSignalCount(self):
        return self.__signalCount

    def getInputBlockComputationString(self, block, port):
        inputType = block.getLinksIn()[port][0].getBlockType()
        returningString = block.getLinksIn()[port][0].getBlockName().upper()

        if (inputType == "ConstantBlock"):
            returningString += "[i]"
        return returningString

    def getGenericBlockComputationString(self, block):
        inputType = block.getBlockType()
        returningString = block.getBlockName().upper()

        if (inputType == "ConstantBlock"):
            returningString += "[i]"
        return returningString

    def dumpOpenCLLibraries(self):
        # Dump header comments
        self.__rowsOpenCL.append("/*\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("OpenCL output src code from the \"CBDCompilerOpenCL.py\". This example\n")
        self.__rowsOpenCL.append("has the ability to execute on internal or external GPU on a mac book pro.\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("Usage:\n")
        self.__rowsOpenCL.append("./test -t cpu32|cpu64|gpu32|gpu64 -i num -f kernel.bc\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("For example, to execute against the 32bit GPU in your system:\n")
        self.__rowsOpenCL.append("./test -t gpu32 -i 0 -f kernel.gpu32.bc\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("Or to test 32bit CPU bitcode:\n")
        self.__rowsOpenCL.append("arch -i386 ./test -t cpu32 -f kernel.cpu32.bc\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("Or 64bit CPU, presuming a 64bit machine:\n")
        self.__rowsOpenCL.append("./test -t cpu64 -f kernel.cpu64.bc\n")
        self.__rowsOpenCL.append("*/\n")
        self.__rowsOpenCL.append("\n")

        # Dump Included libraries
        self.__rowsOpenCL.append("#include <stdio.h>\n")
        self.__rowsOpenCL.append("#include <stdlib.h>\n")
        self.__rowsOpenCL.append("#include <string.h>\n")
        self.__rowsOpenCL.append("#include <getopt.h>\n")
        self.__rowsOpenCL.append("#include <time.h>\n")
        self.__rowsOpenCL.append("#include <math.h>\n")
        self.__rowsOpenCL.append("#include <sys/stat.h>\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("#include <OpenCL/opencl.h>\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("#define MAXPATHLEN  512\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLIterations(self):
        self.__rowsOpenCL.append("// The number of total simulation steps\n")
        self.__rowsOpenCL.append("#define NELEMENTS %d\n" % self.getIterations())

    def dumbOpenCLGlobalObjects(self):
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("// Needed OpenCL objects\n")
        self.__rowsOpenCL.append("int nElements;\n")
        self.__rowsOpenCL.append("int              device_index;\n")
        self.__rowsOpenCL.append("cl_device_type   device_type;\n")
        self.__rowsOpenCL.append("cl_device_id     device;\n")
        self.__rowsOpenCL.append("cl_context       context;\n")
        self.__rowsOpenCL.append("cl_command_queue queue;\n")
        self.__rowsOpenCL.append("cl_program       program;\n")
        self.__rowsOpenCL.append("cl_kernel        kernel;\n")

        # Add all constant variables names
        self.__rowsOpenCL.append("cl_mem           ")
        i = 0
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if (i > 0):
                    self.__rowsOpenCL.append(", ")
                self.__rowsOpenCL.append("%s" % (currentBlockName.upper()))
                i += 1
        self.__rowsOpenCL.append(", OUT;\n")

        self.__rowsOpenCL.append("bool             is32bit;\n")
        self.__rowsOpenCL.append("float *host_OUT;\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLCheckStatusUtility(self):
        self.__rowsOpenCL.append("// A utility function to simplify error checking messages.\n")
        self.__rowsOpenCL.append("static void check_status(char* msg, cl_int err) {\n")
        self.__rowsOpenCL.append("	if (err != CL_SUCCESS) {\n")
        self.__rowsOpenCL.append("		fprintf(stderr, \"%s failed. Error: %d\\n\", msg, err);\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("}\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLCreateProgramFromBitcode(self):
        self.__rowsOpenCL.append("static void create_program_from_bitcode(char* bitcode_path) {\n")
        self.__rowsOpenCL.append("	cl_int err;\n")
        self.__rowsOpenCL.append("	unsigned int i;\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Pass the pre-compiled bitcode to create the program\n")
        self.__rowsOpenCL.append("	size_t len = strlen(bitcode_path);\n")
        self.__rowsOpenCL.append("	program = clCreateProgramWithBinary(context, 1, &device, &len, (const unsigned char**)&bitcode_path, NULL, &err);\n")
        self.__rowsOpenCL.append("	check_status(\"clCreateProgramWithBinary\", err);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Build the program to create executable binary for the specific chosen device\n")
        self.__rowsOpenCL.append("	err = clBuildProgram(program, 1, &device, NULL, NULL, NULL);\n")
        self.__rowsOpenCL.append("	check_status(\"clBuildProgram\", err);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Create the Kernel\n")
        self.__rowsOpenCL.append("	kernel = clCreateKernel(program, \"cbdCompute\", &err);\n")
        self.__rowsOpenCL.append("	check_status(\"clCreateKernel\", err);\n")
        self.__rowsOpenCL.append("\n")

        # Create host local variables
        self.__rowsOpenCL.append("	// Create the host (local) variables\n")
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenCL.append("	float *host_%s = (float*)malloc(sizeof(float)*nElements);\n" % currentBlockName.upper())
        self.__rowsOpenCL.append("	host_OUT = (float*)malloc(sizeof(float)*nElements);\n")

        self.__rowsOpenCL.append("	bool is32bit;\n")
        self.__rowsOpenCL.append("\n")

        self.__rowsOpenCL.append("	// Pack buffers with data\n")
        self.__rowsOpenCL.append("	for (i = 0; i < nElements; i++) {\n")
        # Put constant data on Constant buffers
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if self.__rand == False:
                    self.__rowsOpenCL.append("		host_%s[i] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))
                else:
                    self.__rowsOpenCL.append("		host_%s[i] = rand() %s 10;\n" % (currentBlockName.upper(), "%"))
        self.__rowsOpenCL.append("	}\n")


        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Create CL memory buffers and load them with the host data  \n")
        # Crete the CL memory buffers for the constants
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenCL.append("	cl_mem %s = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*nElements, host_%s, &err);\n" % (currentBlockName.upper(), currentBlockName.upper()))

        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Set Buffer OUT as write only, since we only store the results\n")
        self.__rowsOpenCL.append("	cl_mem OUT = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float)*nElements, NULL, &err);\n")
        self.__rowsOpenCL.append("\n")

        # Check the creation of the memory objects
        self.__rowsOpenCL.append("	if (")
        i = 0
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if (i > 0):
                    self.__rowsOpenCL.append(" || ")
                self.__rowsOpenCL.append("%s == NULL" % currentBlockName.upper())
                i += 1
        self.__rowsOpenCL.append(" || OUT == NULL) {\n")

        self.__rowsOpenCL.append("		fprintf(stderr, \"Error: Unable to create OpenCL buffer memory objects.\\n\");\n")
        self.__rowsOpenCL.append("		exit(1);\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// We set the CL buffers as arguments for the 'cbdCompute' kernel.\n")
        self.__rowsOpenCL.append("	int argc = 0;\n")

        # Set the CL buffers for the compute kernel
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenCL.append("	err |= clSetKernelArg(kernel, argc++, sizeof(cl_mem), &%s);\n" % currentBlockName.upper())
        self.__rowsOpenCL.append("	err |= clSetKernelArg(kernel, argc++, sizeof(cl_mem), &OUT);\n")

        self.__rowsOpenCL.append("	check_status(\"clSetKernelArg\", err);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Launch the kernel with a single dimension. OpenCL selects the local dimensions by\n")
        self.__rowsOpenCL.append("	// passing \"NULL\" as the 6th argument\n")
        self.__rowsOpenCL.append("	size_t global = nElements;\n")
        self.__rowsOpenCL.append("	err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, NULL, 0, NULL, NULL);\n")
        self.__rowsOpenCL.append("	check_status(\"clEnqueueNDRangeKernel\", err);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Read the results from the N1 buffer in blocking mod (CL_TRUE) in order to synchronise\n")
        self.__rowsOpenCL.append("	// by waiting for all kernels to finish\n")
        self.__rowsOpenCL.append("	clEnqueueReadBuffer(queue, OUT, CL_TRUE, 0, nElements*sizeof(cl_float), host_OUT, 0, NULL, NULL);\n")
        self.__rowsOpenCL.append("\n")

        self.__rowsOpenCL.append("}\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLInit(self):
        self.__rowsOpenCL.append("// OpenCL Initialization routine\n")
        self.__rowsOpenCL.append("static void init_opencl() {\n")
        self.__rowsOpenCL.append("	cl_int err;\n")
        self.__rowsOpenCL.append("	cl_uint num_devices;\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Get the number of compute-capable devices\n")
        self.__rowsOpenCL.append("	clGetDeviceIDs(NULL, device_type, 0, NULL, &num_devices);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Check if the devicex index is within device cound bounds\n")
        self.__rowsOpenCL.append("	if (device_index < 0 || device_index > num_devices - 1) {\n")
        self.__rowsOpenCL.append("		fprintf(stdout, \"Requested index (%d) is out of range.  Using 0.\\n\", \n")
        self.__rowsOpenCL.append("		device_index);\n")
        self.__rowsOpenCL.append("		device_index = 0;\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Get the selected device\n")
        self.__rowsOpenCL.append("	cl_device_id all_devices[num_devices];\n")
        self.__rowsOpenCL.append("	clGetDeviceIDs(NULL, device_type, num_devices, all_devices, NULL);\n")
        self.__rowsOpenCL.append("	device = all_devices[device_index];\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Dump info about the device\n")
        self.__rowsOpenCL.append("	char name[128];\n")
        self.__rowsOpenCL.append("	clGetDeviceInfo(device, CL_DEVICE_NAME, 128*sizeof(char), name, NULL);\n")
        self.__rowsOpenCL.append("	fprintf(stdout, \"Using OpenCL device: %s\\n\", name);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Create OpenCL context for the selected device\n")
        self.__rowsOpenCL.append("	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);\n")
        self.__rowsOpenCL.append("	check_status(\"clCreateContext\", err);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Create a command queue for the context of our selected device\n")
        self.__rowsOpenCL.append("	queue = clCreateCommandQueue(context, device, 0, &err);\n")
        self.__rowsOpenCL.append("	check_status(\"clCreateCommandQueue\", err);\n")
        self.__rowsOpenCL.append("}\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLShutdown(self):
        self.__rowsOpenCL.append("// Release/Free all the buffers, memory objects, program, kernels, queues, context\n")
        self.__rowsOpenCL.append("static void shutdown_opencl() {\n")

        # Release the constants
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenCL.append("	clReleaseMemObject(%s);\n" % currentBlockName.upper())
        self.__rowsOpenCL.append("	clReleaseMemObject(OUT);\n")

        self.__rowsOpenCL.append("	clReleaseKernel(kernel);\n")
        self.__rowsOpenCL.append("	clReleaseProgram(program);\n")
        self.__rowsOpenCL.append("	clReleaseCommandQueue(queue);\n")
        self.__rowsOpenCL.append("	clReleaseContext(context);\n")
        self.__rowsOpenCL.append("}\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLMessageHelper(self):
        self.__rowsOpenCL.append("// Executable usage message\n")
        self.__rowsOpenCL.append("static void usage(char* name) {\n")
        self.__rowsOpenCL.append("	fprintf(stdout, \"\\nUsage:   %s -t gpu32|gpu64|cpu32|cpu64 [-i index] -f filename\\n\", name);\n")
        self.__rowsOpenCL.append("	fprintf(stdout, \"Example: %s -t gpu32 -i 0 -f kernel.gpu32.bc\\n\\n\", name);\n")
        self.__rowsOpenCL.append("	exit(0);\n")
        self.__rowsOpenCL.append("}\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLArgumentsChecker(self):
        self.__rowsOpenCL.append("// Command line Arguments check\n")
        self.__rowsOpenCL.append("static void process_arguments(int argc, char* const *argv, char* filepath) {\n")
        self.__rowsOpenCL.append("	int c;\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	static struct option longopts[] = {\n")
        self.__rowsOpenCL.append("		{\"type\", required_argument, NULL, 't'},\n")
        self.__rowsOpenCL.append("		{\"filename\", required_argument, NULL, 'f'},\n")
        self.__rowsOpenCL.append("		{\"index\", required_argument, NULL, 'i'},\n")
        self.__rowsOpenCL.append("		{\"help\", no_argument, NULL, 'h'},\n")
        self.__rowsOpenCL.append("		{0, 0, 0, 0}\n")
        self.__rowsOpenCL.append("	};\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	while ((c = getopt_long(argc, argv, \"t:f:i:h\", longopts, NULL)) != -1) {\n")
        self.__rowsOpenCL.append("		switch (c) {\n")
        self.__rowsOpenCL.append("			case 'f':\n")
        self.__rowsOpenCL.append("				filepath[0] = '\\0';\n")
        self.__rowsOpenCL.append("				strlcat(filepath, optarg, MAXPATHLEN);\n")
        self.__rowsOpenCL.append("				break;\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("			case 't':\n")
        self.__rowsOpenCL.append("				if (0 == strncmp(optarg, \"gpu\", 3)) {\n")
        self.__rowsOpenCL.append("					device_type = CL_DEVICE_TYPE_GPU;\n")
        self.__rowsOpenCL.append("				} else if (0 == strncmp(optarg, \"cpu\", 3)) {\n")
        self.__rowsOpenCL.append("					device_type = CL_DEVICE_TYPE_CPU;\n")
        self.__rowsOpenCL.append("				} else {\n")
        self.__rowsOpenCL.append("					fprintf(stderr, \"Unsupported test device type '%s'; using 'gpu'.\\n\", optarg);\n")
        self.__rowsOpenCL.append("				}\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("				if (0 == strncmp(optarg+3, \"32\", 2)) {\n")
        self.__rowsOpenCL.append("					is32bit = true;\n")
        self.__rowsOpenCL.append("				} else if (0 == strncmp(optarg+3, \"64\", 2)) {\n")
        self.__rowsOpenCL.append("					is32bit = false;\n")
        self.__rowsOpenCL.append("				} else {\n")
        self.__rowsOpenCL.append("					is32bit = true;\n")
        self.__rowsOpenCL.append("					fprintf(stderr, \"Unsupported test device type '%s'; using 'gpu32'.\\n\", optarg);\n")
        self.__rowsOpenCL.append("				}\n")
        self.__rowsOpenCL.append("				break;\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("			case 'i':\n")
        self.__rowsOpenCL.append("				device_index = atoi(optarg);\n")
        self.__rowsOpenCL.append("				break;\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("			case 'h':\n")
        self.__rowsOpenCL.append("				default:\n")
        self.__rowsOpenCL.append("				usage(argv[0]);\n")
        self.__rowsOpenCL.append("		}\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Ensure the the selected devie type is valid\n")
        self.__rowsOpenCL.append("	if ((device_type != CL_DEVICE_TYPE_GPU) && (device_type != CL_DEVICE_TYPE_CPU)) {\n")
        self.__rowsOpenCL.append("		fprintf(stderr, \"Error: device type not specified.\\n\");\n")
        self.__rowsOpenCL.append("		exit(1);\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Ensure that the precompiled kernel exists\n")
        self.__rowsOpenCL.append("	struct stat stat_buf;\n")
        self.__rowsOpenCL.append("	if (0 != stat(filepath, &stat_buf)) {\n")
        self.__rowsOpenCL.append("		fprintf(stderr, \"Error: file '%s' does not exist.\\n\", filepath);\n")
        self.__rowsOpenCL.append("		exit(1);\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("}\n")
        self.__rowsOpenCL.append("\n")

    def dumpOpenCLMain(self):
        self.__rowsOpenCL.append("// Main execution path\n")
        self.__rowsOpenCL.append("int main (int argc, char* const *argv)\n")
        self.__rowsOpenCL.append("{\n")
        self.__rowsOpenCL.append("	clock_t start, end;\n")

        self.__rowsOpenCL.append("	nElements = NELEMENTS;\n")

        self.__rowsOpenCL.append("    if (argc > 7) {\n")
        self.__rowsOpenCL.append("        nElements = atoi(argv[7]);\n")
        self.__rowsOpenCL.append("    }\n")

        self.__rowsOpenCL.append("	char filepath[MAXPATHLEN];\n")
        self.__rowsOpenCL.append("	filepath[0] = '\\0';\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	process_arguments(argc, argv, filepath);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	printf(\"================= GPU ==================\\nNumber of discrete time steps: %d\\n\\n\",nElements);\n")
        self.__rowsOpenCL.append("	// Start the execution time counter for OpenCL\n")
        self.__rowsOpenCL.append("	start = clock();\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Initialize OpenCL objects, context and command queue\n")
        self.__rowsOpenCL.append("	init_opencl();\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Ensure that the current architecture is compatible\n")
        self.__rowsOpenCL.append("	if (device_type == CL_DEVICE_TYPE_CPU)\n")
        self.__rowsOpenCL.append("	{\n")

        # Preprocessor directives for OpenCL specific extensions
        self.__rowsOpenCL.append("#if __LP64__\n")
        self.__rowsOpenCL.append("		if (is32bit)\n")
        self.__rowsOpenCL.append("			fprintf(stderr, \"Warning: user specified the 'cpu32' option on the 64bit architecture.\\n\");\n")
        self.__rowsOpenCL.append("#else\n")
        self.__rowsOpenCL.append("		if (!is32bit)\n")
        self.__rowsOpenCL.append("			fprintf(stderr, \"Warning: user specified the 'cpu64' option on the 32bit architecture.\\n\");\n")
        self.__rowsOpenCL.append("#endif\n")

        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("	else if (device_type == CL_DEVICE_TYPE_GPU)\n")
        self.__rowsOpenCL.append("	{\n")
        self.__rowsOpenCL.append("		cl_int err;\n")
        self.__rowsOpenCL.append("		cl_uint address_bits = 0;\n")
        self.__rowsOpenCL.append("		err = clGetDeviceInfo(device, CL_DEVICE_ADDRESS_BITS, sizeof(address_bits), &address_bits, NULL);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("		if (!is32bit && (address_bits == 32))\n")
        self.__rowsOpenCL.append("			fprintf(stderr, \"Warning: user specified the 'gpu64' option on the 32bit architecture.\\n\");\n")
        self.__rowsOpenCL.append("		else if (is32bit && (address_bits == 64))\n")
        self.__rowsOpenCL.append("			fprintf(stderr, \"Warning: user specified the 'gpu32' option on the 64bit architecture.\\n\");\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Run the program on the kernel\n")
        self.__rowsOpenCL.append("	create_program_from_bitcode(filepath);\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Shutdown the opencl device\n")
        self.__rowsOpenCL.append("	shutdown_opencl();\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	// Stop the openCL clock\n")
        self.__rowsOpenCL.append("	end = clock();\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	printf(\"Execution time(secs): %f\\n\\n\", (end-start)/(double)CLOCKS_PER_SEC );\n")
        self.__rowsOpenCL.append("\n")

        self.__rowsOpenCL.append("	// Validate the results -optional- should use if we already know the final results\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	int success = 1;\n")
        self.__rowsOpenCL.append("	unsigned int i;\n")
        self.__rowsOpenCL.append("	for (i = 0; i < nElements; i++) {  \n")
        self.__rowsOpenCL.append("		if ( host_OUT[i] != %s) {\n" % self.__expectingResult)
        self.__rowsOpenCL.append("			success = 0;\n")
        self.__rowsOpenCL.append("			printf(\"openCL_OUT = %f\\n\", host_OUT[i]);\n")
        self.__rowsOpenCL.append("			fprintf(stderr, \"Validation failed at index %d\\n----------------------------------------\\n\", i);\n")
        self.__rowsOpenCL.append("			break;\n")
        self.__rowsOpenCL.append("		}\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("	if (success) {\n")
        self.__rowsOpenCL.append("		fprintf(stdout, \"Validation successful.\\n----------------------------------------\\n\");\n")
        self.__rowsOpenCL.append("	}\n")
        self.__rowsOpenCL.append("\n")

        self.dumpSaveResultsToCSV()

        self.__rowsOpenCL.append("	return 0;\n")
        self.__rowsOpenCL.append("}\n")

    def dumpKernel(self):
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("int main(int argc, char *argv[]){\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("    /*The implementation of your CBD model starts here. The example shows a simple model with 2 constant blocks, an add block and a product block. If there are linear algebraic loops in your model, you need to call the gaussian elimination solver.*/\n")
        self.__rowsOpenCL.append("\n")
        self.__rowsOpenCL.append("    int i;\n")
        self.__rowsOpenCL.append("    double result[M][N]; /* the signal values are stored here: M is output of block, N are the number of iterations of your CBD simulation. Here we have a 4 by 100 matrix to store the results of our simulation */\n")

        # Loop through all the signal names
        namesArray = "    char *names[M] = {"
        for i, hashSignalName in enumerate(self.__blockNamesList):
            if i > 0 :
                namesArray += ", "
            namesArray += "\"" + hashSignalName.lower() + "\""
        namesArray += " };\n"
        self.__rowsOpenCL.append(namesArray)

        self.__rowsOpenCL.append("    /* First iteration */\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()

            if currentBlockType == "ConstantBlock":
                self.__rowsOpenCL.append("    %s[0] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))

            elif currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = block.getLinksIn()["IN1"][0].getBlockName()

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "- 1.0 * %s[0];" % inputString.upper()
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s[0];" % inputString.upper()
                elif currentBlockType == "GenericBlock":
                    operationString = "generic(%s[0]);" % inputString.upper()

                self.__rowsOpenCL.append("    %s[0] = %s\n" % (currentBlockName.upper(), operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = block.getLinksIn()["IN1"][0].getBlockName()
                inputString2 = block.getLinksIn()["IN2"][0].getBlockName()

                operatorString = ""
                if currentBlockType == "ProductBlock":
                    operatorString = "*"
                elif currentBlockType == "AdderBlock":
                    operatorString = "+"
                elif currentBlockType == "ModuloBlock":
                    operatorString = "%"
                elif currentBlockType == "RootBlock":
                    operatorString = "pow"

                self.__rowsOpenCL.append("    %s[0] = %s[0] %s %s[0];\n" % (currentBlockName.upper(), inputString1.upper(), operatorString , inputString2.upper()))


        self.__rowsOpenCL.append("    /* Other iterations */\n")
        self.__rowsOpenCL.append("    for (i=1; i<N; i++){\n")

        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenCL.append("        %s[i] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[i + 1].value))

            elif currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = block.getLinksIn()["IN1"][0].getBlockName()

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "- 1.0 * %s[i];" % inputString.upper()
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s[i];" % inputString.upper()
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s[i]);" % (block.getBlockOperator(), inputString.upper())

                self.__rowsOpenCL.append("        %s[i] = %s\n" % (currentBlockName.upper(), operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = block.getLinksIn()["IN1"][0].getBlockName()
                inputString2 = block.getLinksIn()["IN2"][0].getBlockName()

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    self.__rowsOpenCL.append("        %s[i] = pow(%s[i], %s[i]);\n" % (currentBlockName.upper(), inputString1.upper(), inputString2.upper()))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    self.__rowsOpenCL.append("        %s[i] = %s[i] %s %s[i];\n" % (currentBlockName.upper(), inputString1.upper(), operatorString , inputString2.upper()))

        self.__rowsOpenCL.append("    }\n")
        self.__rowsOpenCL.append("}\n")

    def dumpSaveResultsToCSV(self):
        self.__rowsOpenCL.append("   // Save results to CSV\n")
        self.__rowsOpenCL.append("   FILE *fp;\n")
        self.__rowsOpenCL.append("\n")

        csvFileSavePath = self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName

        self.__rowsOpenCL.append("   if (argc > 8) {\n")
        self.__rowsOpenCL.append("       fp = fopen(argv[8], \"a+\");\n")
        self.__rowsOpenCL.append("       fprintf(fp, \"%s OpenCL%s\", device_index);\n" % ("GPU \%d", self.__csvSeparationCharacter))
        self.__rowsOpenCL.append("       fprintf(fp, \"%s%s\", nElements);\n" %("%d", self.__csvSeparationCharacter))
        self.__rowsOpenCL.append("       fprintf(fp, \"%s%s\");\n" % (self.__fileName, self.__csvSeparationCharacter))
        self.__rowsOpenCL.append("       fprintf(fp, \"%s%s\", success);\n" % ("%d", self.__csvSeparationCharacter))
        self.__rowsOpenCL.append("       fprintf(fp, \"%f\\n\", (end-start)/(double)CLOCKS_PER_SEC);\n")
        self.__rowsOpenCL.append("       fclose(fp);\n")
        self.__rowsOpenCL.append("       printf(\"Saved results in %s\\n\", argv[8]);\n")
        self.__rowsOpenCL.append("   }\n")

    def walkOpenCL(self):

        # Clean the output list of lines
        self.__rowsOpenCL = []

        # Dump all the OpenCL parts
        self.dumpOpenCLLibraries()
        self.dumpOpenCLIterations()
        self.dumbOpenCLGlobalObjects()
        self.dumpOpenCLCheckStatusUtility()
        self.dumpOpenCLCreateProgramFromBitcode()
        self.dumpOpenCLInit()
        self.dumpOpenCLShutdown()
        self.dumpOpenCLMessageHelper()
        self.dumpOpenCLArgumentsChecker()
        self.dumpOpenCLMain()

        return "".join(self.__rowsOpenCL)

    def toDisk(self):

        outputString = self.walkOpenCL()
        text_file = open(self.__directoryPath + "/" + self.__fileName + ".c", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output OpenCL src on %s." % self.__directoryPath