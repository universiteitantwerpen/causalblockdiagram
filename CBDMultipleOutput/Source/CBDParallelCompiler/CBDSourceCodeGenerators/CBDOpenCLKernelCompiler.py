__author__ = 'madks_000'

import os

class CBDOpenCLKernelCompiler():
    """
    A class that generates the kernel for the OpenCL src code trace
    """
    def __init__(self, compilerSettings):
        self.__blockNamesList = []
        self.__rowsOpenCLKernel = []

        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__fileName = compilerSettings.getFileName()
        self.__cbd = compilerSettings.getCBD()
        self.__blockNamesList = compilerSettings.getBlockNames()
        self.__strongComponentNames = compilerSettings.getStrongComponentNames()
        self.__gaussianSolverInput = compilerSettings.getGaussianSolverInput()

    def getInputBlockComputationString(self, block, port):
        inputType = block.getLinksIn()[port][0].getBlockType()
        returningString = block.getLinksIn()[port][0].getBlockName().upper()

        if (inputType == "ConstantBlock"):
            returningString += "[i]"
        return returningString

    def getGenericBlockComputationString(self, block):
        inputType = block.getBlockType()
        returningString = block.getBlockName().upper()

        if (inputType == "ConstantBlock"):
            returningString += "[i]"
        return returningString

    def dumpGaussianSolver(self):
        self.__rowsOpenCLKernel.append("#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))\n")
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("void swap_row(float *a, float *b, int r1, int r2, int n)\n")
        self.__rowsOpenCLKernel.append("{\n")
        self.__rowsOpenCLKernel.append("	float tmp, *p1, *p2;\n")
        self.__rowsOpenCLKernel.append("	int i;\n")
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("	if (r1 == r2) return;\n")
        self.__rowsOpenCLKernel.append("	for (i = 0; i < n; i++) {\n")
        self.__rowsOpenCLKernel.append("		p1 = mat_elem(a, r1, i, n);\n")
        self.__rowsOpenCLKernel.append("		p2 = mat_elem(a, r2, i, n);\n")
        self.__rowsOpenCLKernel.append("		tmp = *p1, *p1 = *p2, *p2 = tmp;\n")
        self.__rowsOpenCLKernel.append("	}\n")
        self.__rowsOpenCLKernel.append("	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;\n")
        self.__rowsOpenCLKernel.append("}\n")
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("void gauss_eliminate(float *a, float *b, float *x, int n)\n")
        self.__rowsOpenCLKernel.append("{\n")
        self.__rowsOpenCLKernel.append("#define A(y, x) (*mat_elem(a, y, x, n))\n")
        self.__rowsOpenCLKernel.append("	int i, j, col, row, max_row,dia;\n")
        self.__rowsOpenCLKernel.append("	float max, tmp;\n")
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("	for (dia = 0; dia < n; dia++) {\n")
        self.__rowsOpenCLKernel.append("		max_row = dia, max = A(dia, dia);\n")
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("		for (row = dia + 1; row < n; row++)\n")
        self.__rowsOpenCLKernel.append("			if ((tmp = fabs(A(row, dia))) > max)\n")
        self.__rowsOpenCLKernel.append("				max_row = row, max = tmp;\n")
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("		swap_row(a, b, dia, max_row, n);\n")
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("		for (row = dia + 1; row < n; row++) {\n")
        self.__rowsOpenCLKernel.append("			tmp = A(row, dia) / A(dia, dia);\n")
        self.__rowsOpenCLKernel.append("			for (col = dia+1; col < n; col++)\n")
        self.__rowsOpenCLKernel.append("				A(row, col) -= tmp * A(dia, col);\n")
        self.__rowsOpenCLKernel.append("			A(row, dia) = 0;\n")
        self.__rowsOpenCLKernel.append("			b[row] -= tmp * b[dia];\n")
        self.__rowsOpenCLKernel.append("		}\n")
        self.__rowsOpenCLKernel.append("	}\n")
        self.__rowsOpenCLKernel.append("	for (row = n - 1; row >= 0; row--) {\n")
        self.__rowsOpenCLKernel.append("		tmp = b[row];\n")
        self.__rowsOpenCLKernel.append("		for (j = n - 1; j > row; j--)\n")
        self.__rowsOpenCLKernel.append("			tmp -= x[j] * A(row, j);\n")
        self.__rowsOpenCLKernel.append("		x[row] = tmp / A(row, row);\n")
        self.__rowsOpenCLKernel.append("	}\n")
        self.__rowsOpenCLKernel.append("#undef A\n")
        self.__rowsOpenCLKernel.append("}\n")
        self.__rowsOpenCLKernel.append("\n")

    def dumpAlgebraicCBDLoopSolver(self):
        self.__rowsOpenCLKernel.append("void getSolution(float *coefficients, float *result)\n")
        self.__rowsOpenCLKernel.append("{\n")

        self.__rowsOpenCLKernel.append("	float a[] = {")
        for i, currentRow in enumerate(self.__gaussianSolverInput[0]):
            if i > 0:
                self.__rowsOpenCLKernel.append(",")
            self.__rowsOpenCLKernel.append("%s" % (','.join(map(str, currentRow))))
        self.__rowsOpenCLKernel.append("};\n")

        self.__rowsOpenCLKernel.append("	gauss_eliminate(a, coefficients, result, %s);\n" % len(self.__gaussianSolverInput[1]))
        self.__rowsOpenCLKernel.append(" \n")
        self.__rowsOpenCLKernel.append("}\n")

    def dumpOpenCLKernel(self):
        self.__rowsOpenCLKernel.append("/*\n")
        self.__rowsOpenCLKernel.append("This kernel computes a flattened CBD with multiple input values as constants and one output\n")
        self.__rowsOpenCLKernel.append("*/\n")
        self.__rowsOpenCLKernel.append("\n")

        # Some preprocessor OpenCL extension declarations
        self.__rowsOpenCLKernel.append("// Optimization 1: The restrict qualifier for the kernel arugments aids in caching but you MUST ensure that the\n")
        self.__rowsOpenCLKernel.append("// pointers do not point on overlapping locations.\n")
        self.__rowsOpenCLKernel.append("// Optimization 2: Enabling double precision floating point (which is also faster than single precision\n")
        self.__rowsOpenCLKernel.append("// since no casting is used to downgrade the accuracy\n")
        self.__rowsOpenCLKernel.append("#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n")
        self.__rowsOpenCLKernel.append("\n")

        # Check if an algebraic loop exists
        if len(self.__strongComponentNames) > 0:
            self.dumpGaussianSolver()
            self.dumpAlgebraicCBDLoopSolver()

        #  Print Global variables
        self.__rowsOpenCLKernel.append("kernel void cbdCompute(")
        i = 0
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if (i > 0):
                    self.__rowsOpenCLKernel.append(", ")
                self.__rowsOpenCLKernel.append("global const float* restrict %s" % (currentBlockName.upper()))
                i += 1
        self.__rowsOpenCLKernel.append(", global float* restrict OUT) {\n")

        self.__rowsOpenCLKernel.append("	size_t i = get_global_id(0);\n")
        self.__rowsOpenCLKernel.append("\n")

        if len(self.__strongComponentNames) > 0:
            strOut = []
            for elementName in self.__gaussianSolverInput[3]:
                if not elementName:
                    strOut.append("0.0")
                else:
                    strOut.append("- %s[i]" % elementName.upper())

        self.__rowsOpenCLKernel.append("\n")

        if len(self.__strongComponentNames) > 0:
            self.__rowsOpenCLKernel.append("    //Solve the algebraic loop\n")
            self.__rowsOpenCLKernel.append("    float coefficients[] = { %s };\n" % (', '.join(map(str, strOut))))
            self.__rowsOpenCLKernel.append("    float x[%s];\n" % len(self.__gaussianSolverInput[1]))
            self.__rowsOpenCLKernel.append("    getSolution(coefficients, x);\n")
            self.__rowsOpenCLKernel.append("\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                if len(self.__strongComponentNames) > 0:
                    if currentBlockName in self.__strongComponentNames:
                        self.__rowsOpenCLKernel.append("    __private float %s = x[%s];\n" % (currentBlockName.upper(), self.__strongComponentNames.index(currentBlockName)))
                    else:
                            self.__rowsOpenCLKernel.append("    __private float %s = 0.0;\n" % (currentBlockName.upper()))

                else:
                    self.__rowsOpenCLKernel.append("    __private float %s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsOpenCLKernel.append("\n")

        ##
        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = self.getGenericBlockComputationString(block)
            currentBlockType = block.getBlockType()

            if currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = self.getInputBlockComputationString(block, "IN1")

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "(- 1.0) * %s;" % inputString
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s;" % inputString
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s);" % (block.getBlockOperator(), inputString)

                self.__rowsOpenCLKernel.append("    %s = %s\n" % (currentBlockName, operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = self.getInputBlockComputationString(block, "IN1")
                inputString2 = self.getInputBlockComputationString(block, "IN2")

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    self.__rowsOpenCLKernel.append("    %s = pow(%s, %s);\n" % (currentBlockName, inputString1, inputString2))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    self.__rowsOpenCLKernel.append("    %s = %s %s %s;\n" % (currentBlockName, inputString1, operatorString , inputString2))

        # Last line, store the last variable to the OUT global float
        lastBlock = self.__cbd.getBlocks()[-1].getBlockName().upper()
        self.__rowsOpenCLKernel.append("    OUT[i] = %s;\n" % lastBlock)


        self.__rowsOpenCLKernel.append("}\n")

    def walkOpenCLKernel(self):

        # Clean the output list of lines
        self.__rowsOpenCLKernel = []

        # Dump the Kernel
        self.dumpOpenCLKernel()

        return "".join(self.__rowsOpenCLKernel)

    def toDisk(self):

        outputString = self.walkOpenCLKernel()
        text_file = open(self.__directoryPath + "/" + self.__fileName + "Kernel.cl", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output OpenCL Kernel src on %s." % self.__directoryPath