__author__ = 'madks_000'

import os

class CBDOpenMPParallelForCompiler():
    """
    A class that generates the OpenMP CPU Parallel For src code
    """
    def __init__(self, compilerSettings):
        self.__blockNamesList = []
        self.__rowsOpenMP = []
        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__rand = compilerSettings.getRand()
        self.__numMaxThreads = compilerSettings.getMaxThreads()
        self.__cbd = compilerSettings.getCBD()
        self.__signalCount = compilerSettings.getSignalCount()
        self.__iterations = compilerSettings.getIterations()
        self.__blockNamesList = compilerSettings.getBlockNames()
        self.__expectingResult = compilerSettings.getExpectingResult()
        self.__strongComponentNames = compilerSettings.getStrongComponentNames()
        self.__gaussianSolverInput = compilerSettings.getGaussianSolverInput()
        self.__intermediateVariablesList = []
        self.__benchmarkFileName = compilerSettings.getBenchmarkFileName()
        self.__currentPath = compilerSettings.getCurrentPath()
        self.__fileName = compilerSettings.getFileName()
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()

    def getIterations(self):
        return self.__iterations

    def getAnsiInputBlockComputationString(self, block, port, ansiC=True):
        inputType = block.getLinksIn()[port][0].getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getLinksIn()[port][0].getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def getAnsiCBlockComputationString(self, block, ansiC=True):
        inputType = block.getBlockType()
        if ansiC:
            returningString = "ansiC_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i]"
        else:
            returningString = "openMP_" + block.getBlockName().upper()

            if (inputType == "ConstantBlock"):
                returningString += "[i_mp]"

        return returningString

    def dumpGaussianSolver(self):
        self.__rowsOpenMP.append("#define mat_elem(a, y, x, n) (a + ((y) * (n) + (x)))\n")
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("void swap_row(float *a, float *b, int r1, int r2, int n)\n")
        self.__rowsOpenMP.append("{\n")
        self.__rowsOpenMP.append("	float tmp, *p1, *p2;\n")
        self.__rowsOpenMP.append("	int i;\n")
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("	if (r1 == r2) return;\n")
        self.__rowsOpenMP.append("	for (i = 0; i < n; i++) {\n")
        self.__rowsOpenMP.append("		p1 = mat_elem(a, r1, i, n);\n")
        self.__rowsOpenMP.append("		p2 = mat_elem(a, r2, i, n);\n")
        self.__rowsOpenMP.append("		tmp = *p1, *p1 = *p2, *p2 = tmp;\n")
        self.__rowsOpenMP.append("	}\n")
        self.__rowsOpenMP.append("	tmp = b[r1], b[r1] = b[r2], b[r2] = tmp;\n")
        self.__rowsOpenMP.append("}\n")
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("void gauss_eliminate(float *a, float *b, float *x, int n)\n")
        self.__rowsOpenMP.append("{\n")
        self.__rowsOpenMP.append("#define A(y, x) (*mat_elem(a, y, x, n))\n")
        self.__rowsOpenMP.append("	int i, j, col, row, max_row,dia;\n")
        self.__rowsOpenMP.append("	float max, tmp;\n")
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("	for (dia = 0; dia < n; dia++) {\n")
        self.__rowsOpenMP.append("		max_row = dia, max = A(dia, dia);\n")
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("		for (row = dia + 1; row < n; row++)\n")
        self.__rowsOpenMP.append("			if ((tmp = fabs(A(row, dia))) > max)\n")
        self.__rowsOpenMP.append("				max_row = row, max = tmp;\n")
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("		swap_row(a, b, dia, max_row, n);\n")
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("		for (row = dia + 1; row < n; row++) {\n")
        self.__rowsOpenMP.append("			tmp = A(row, dia) / A(dia, dia);\n")
        self.__rowsOpenMP.append("			for (col = dia+1; col < n; col++)\n")
        self.__rowsOpenMP.append("				A(row, col) -= tmp * A(dia, col);\n")
        self.__rowsOpenMP.append("			A(row, dia) = 0;\n")
        self.__rowsOpenMP.append("			b[row] -= tmp * b[dia];\n")
        self.__rowsOpenMP.append("		}\n")
        self.__rowsOpenMP.append("	}\n")
        self.__rowsOpenMP.append("	for (row = n - 1; row >= 0; row--) {\n")
        self.__rowsOpenMP.append("		tmp = b[row];\n")
        self.__rowsOpenMP.append("		for (j = n - 1; j > row; j--)\n")
        self.__rowsOpenMP.append("			tmp -= x[j] * A(row, j);\n")
        self.__rowsOpenMP.append("		x[row] = tmp / A(row, row);\n")
        self.__rowsOpenMP.append("	}\n")
        self.__rowsOpenMP.append("#undef A\n")
        self.__rowsOpenMP.append("}\n")
        self.__rowsOpenMP.append("\n")

    def dumpAlgebraicCBDLoopSolver(self):
        self.__rowsOpenMP.append("void getSolution(float *coefficients, float *result)\n")
        self.__rowsOpenMP.append("{\n")

        self.__rowsOpenMP.append("	float a[] = {")
        for i, currentRow in enumerate(self.__gaussianSolverInput[0]):
            if i > 0:
                self.__rowsOpenMP.append(",")
            self.__rowsOpenMP.append("%s" % (','.join(map(str, currentRow))))
        self.__rowsOpenMP.append("};\n")
        self.__rowsOpenMP.append("	gauss_eliminate(a, coefficients, result, %s);\n" % len(self.__gaussianSolverInput[1]))
        self.__rowsOpenMP.append(" \n")
        self.__rowsOpenMP.append("}\n")

    def dumpOpenMP(self):
        self.__rowsOpenMP.append("/*\n")
        self.__rowsOpenMP.append("	OpenMP output src code from the CBD model simulation for mac\n")
        self.__rowsOpenMP.append("	applying data/domain decomposition\n")
        self.__rowsOpenMP.append("	Do check the environment variables first (i.e):\n")
        self.__rowsOpenMP.append("	export DYLD_LIBRARY_PATH=\"/opt/intel/lib\" // for Intel C++ open mp compiler for Mac\n")
        self.__rowsOpenMP.append("	export OMP_NUM_THREADS=4 // The number of maximum threads\n")
        self.__rowsOpenMP.append("	export OMP_STACKSIZE=\"50M\" // Small stack size causes fragmentation errors, increase accordingly\n")
        self.__rowsOpenMP.append("*/\n")
        self.__rowsOpenMP.append("\n")
        self.__rowsOpenMP.append("#include <stdlib.h>\n")
        self.__rowsOpenMP.append("#include <stdio.h>\n")
        self.__rowsOpenMP.append("#include <string.h>\n")
        self.__rowsOpenMP.append("#include <getopt.h>\n")
        self.__rowsOpenMP.append("#include <time.h>\n")
        self.__rowsOpenMP.append("#include <math.h>\n")
        self.__rowsOpenMP.append("#include <sys/stat.h>\n")
        self.__rowsOpenMP.append("#include <omp.h>\n")
        self.__rowsOpenMP.append("\n")

        self.__rowsOpenMP.append("// The number of total simulation steps\n")
        self.__rowsOpenMP.append("#define NELEMENTS %d\n" % self.getIterations())

        self.__rowsOpenMP.append("// The number of maxinum spawning threads\n")
        self.__rowsOpenMP.append("#define MAX_THREADS %d\n" % self.__numMaxThreads)

        if len(self.__strongComponentNames) > 0:
            self.dumpGaussianSolver()
            self.dumpAlgebraicCBDLoopSolver()

        self.__rowsOpenMP.append("\n")
        self.__rowsOpenMP.append("// Main execution path\n")
        self.__rowsOpenMP.append("int main (int argc, char* const *argv)\n")
        self.__rowsOpenMP.append("{\n")
        self.__rowsOpenMP.append("	clock_t start, end;\n")
        self.__rowsOpenMP.append("	\n")

        self.__rowsOpenMP.append("	unsigned int nElements = NELEMENTS;\n")

        self.__rowsOpenMP.append("    if (argc > 1) {\n")
        self.__rowsOpenMP.append("        nElements = atoi(argv[1]);\n")
        self.__rowsOpenMP.append("    }\n")

        self.__rowsOpenMP.append("	// OpenMP variables: number of total threads, current thread id, min I item, max I item\n")
        self.__rowsOpenMP.append("	int currentThreadId;\n")
        self.__rowsOpenMP.append("	int maxThreads = MAX_THREADS;\n")
        self.__rowsOpenMP.append("	unsigned int minI, maxI;\n")
        self.__rowsOpenMP.append("	\n")
        self.__rowsOpenMP.append("	// Enforce the number of maximum threads\n")
        self.__rowsOpenMP.append("	omp_set_num_threads(maxThreads);\n")
        self.__rowsOpenMP.append("	\n")
        self.__rowsOpenMP.append("	// Job size/number of iterations for each thread\n")
        self.__rowsOpenMP.append("	unsigned int threadJobSize = nElements / maxThreads;\n")
        self.__rowsOpenMP.append("	\n")
        self.__rowsOpenMP.append("	printf(\"=========== PARALLEL_FOR CPU ===========\\nNumber of discrete time steps: %d\\n\\n\",nElements);	\n")
        self.__rowsOpenMP.append("	\n")
        self.__rowsOpenMP.append("	printf(\"Using: OpenMP & CPU\\n\");\n")
        self.__rowsOpenMP.append("	printf(\"Number of threads: %d\\n\", maxThreads);	\n")
        self.__rowsOpenMP.append("	printf(\"Job size per thread: %d\\n\",threadJobSize);	\n")
        self.__rowsOpenMP.append("\n")
        self.__rowsOpenMP.append("	// Start the execution time counter for OpenMP\n")
        self.__rowsOpenMP.append("	start = clock();\n")
        self.__rowsOpenMP.append("	\n")

         # Create OpenMP local variables
        self.__rowsOpenMP.append("	// Create the input/constant variables\n")
        self.__rowsOpenMP.append("	unsigned int i = 0;\n")

        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                self.__rowsOpenMP.append("	float *openMP_%s = (float*)malloc(sizeof(float)*nElements);\n" % currentBlockName.upper())
        self.__rowsOpenMP.append("	float *openMP_OUT = (float*)malloc(sizeof(float)*nElements);\n")

        self.__rowsOpenMP.append("\n")

        if len(self.__strongComponentNames) > 0:
            # Allocate the coefficients array
            self.__rowsOpenMP.append("	// Allocate the coefficients array\n")
            self.__rowsOpenMP.append("	float *x = (float*)malloc(sizeof(float)*%s);\n" % len(self.__gaussianSolverInput[1]))
            self.__rowsOpenMP.append("\n")

        self.__rowsOpenMP.append("	// Pack input variables with data\n")
        self.__rowsOpenMP.append("	for (i = 0; i < nElements; i++) {\n")
        # Put constant data on Constant buffers
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType == "ConstantBlock":
                if self.__rand == False:
                    self.__rowsOpenMP.append("		openMP_%s[i] = %s;\n" % (currentBlockName.upper(), self.__cbd.getSignal(currentBlockName)[0].value))
                else:
                    self.__rowsOpenMP.append("		openMP_%s[i] = rand() %s 10;\n" % (currentBlockName.upper(), "%"))
        self.__rowsOpenMP.append("	}\n")

        self.__rowsOpenMP.append("\n")
        self.__rowsOpenMP.append("	// Do Calculate CBD\n")
        self.__rowsOpenMP.append("	\n")
        self.__rowsOpenMP.append("	// Iteration counter for the current thread\n")
        self.__rowsOpenMP.append("	unsigned int i_mp = 0;\n")
        self.__rowsOpenMP.append("\n")

        # Initialize intermediate variables
        self.__rowsOpenMP.append("	    // Initialize private intermediate variables\n")
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                self.__rowsOpenMP.append("       	float openMP_%s = 0.0;\n" % (currentBlockName.upper()))
                self.__intermediateVariablesList.append("openMP_%s" % (currentBlockName.upper()))
        self.__rowsOpenMP.append("\n")


        self.__rowsOpenMP.append("	#pragma omp parallel for private(")
        intermediateVariablesString = ', '.join(map(str, self.__intermediateVariablesList))
        self.__rowsOpenMP.append(intermediateVariablesString)
        self.__rowsOpenMP.append(")\n")

        self.__rowsOpenMP.append("	for (i_mp = 0; i_mp < nElements; i_mp++) {\n")

        if len(self.__strongComponentNames) > 0:
            strOut = []
            for elementName in self.__gaussianSolverInput[3]:
                if not elementName:
                    strOut.append("0.0")
                else:
                    strOut.append("- openMP_%s[i_mp]" % elementName.upper())

        self.__rowsOpenMP.append("\n")

        if len(self.__strongComponentNames) > 0:
            self.__rowsOpenMP.append("	       //Solve the algebraic loop\n")
            self.__rowsOpenMP.append("	       float coefficients[] = { %s };\n" % (', '.join(map(str, strOut))))
            self.__rowsOpenMP.append("	       float x[%s];\n" % len(self.__gaussianSolverInput[1]))
            self.__rowsOpenMP.append("	       getSolution(coefficients, x);\n")
            self.__rowsOpenMP.append("\n")

        # Print intermediate variables
        for block in self.__cbd.getBlocks():
            currentBlockName = block.getBlockName()
            currentBlockType = block.getBlockType()
            if currentBlockType is not "ConstantBlock":
                if len(self.__strongComponentNames) > 0:
                    if currentBlockName in self.__strongComponentNames:
                        self.__rowsOpenMP.append("	       openMP_%s = x[%s];\n" % (currentBlockName.upper(), self.__strongComponentNames.index(currentBlockName)))

                else:
                    self.__rowsOpenMP.append("	       openMP_%s = 0.0;\n" % (currentBlockName.upper()))
        self.__rowsOpenMP.append("\n")

        ##
        for i, block in enumerate(self.__cbd.getBlocks()):
            currentBlockName = self.getAnsiCBlockComputationString(block, False)
            currentBlockType = block.getBlockType()

            if currentBlockType == "NegatorBlock" or currentBlockType == "InverterBlock" or  currentBlockType == "GenericBlock":

                inputString = self.getAnsiInputBlockComputationString(block, "IN1", False)

                operationString = ""
                if currentBlockType == "NegatorBlock":
                    operationString = "(- 1.0) * %s;" % inputString
                elif currentBlockType == "InverterBlock":
                    operationString = "1.0 / %s;" % inputString
                elif currentBlockType == "GenericBlock":
                    operationString = "%s(%s);" % (block.getBlockOperator(), inputString)

                self.__rowsOpenMP.append("        %s = %s\n" % (currentBlockName, operationString))

            elif currentBlockType == "ProductBlock" or currentBlockType == "AdderBlock" or \
                            currentBlockType == "RootBlock" or currentBlockType == "ModuloBlock":

                inputString1 = self.getAnsiInputBlockComputationString(block, "IN1", False)
                inputString2 = self.getAnsiInputBlockComputationString(block, "IN2", False)

                operatorString = ""

                if currentBlockType == "RootBlock":
                    operatorString = "pow"
                    self.__rowsOpenMP.append("        %s = pow(%s, %s);\n" % (currentBlockName, inputString1, inputString2))
                else:
                    if currentBlockType == "ProductBlock":
                        operatorString = "*"
                    elif currentBlockType == "AdderBlock":
                        operatorString = "+"
                    elif currentBlockType == "ModuloBlock":
                        operatorString = "%"
                    self.__rowsOpenMP.append("        %s = %s %s %s;\n" % (currentBlockName, inputString1, operatorString , inputString2))
        ##

        # Last line, store the last variable to the OUT global float
        lastBlock = self.__cbd.getBlocks()[-1].getBlockName().upper()
        self.__rowsOpenMP.append("        openMP_OUT[i_mp] = openMP_%s;\n" % lastBlock)

        self.__rowsOpenMP.append("	}\n")
        self.__rowsOpenMP.append("\n")

        self.__rowsOpenMP.append("	// Stop the clock\n")
        self.__rowsOpenMP.append("	end = clock();\n")
        self.__rowsOpenMP.append("	printf(\"Execution time(secs): %f\\n\\n\", (end-start)/(double)CLOCKS_PER_SEC );\n")
        self.__rowsOpenMP.append("\n")

        self.__rowsOpenMP.append("	// Validate the results -optional- should use if we already know the final results\n")
        self.__rowsOpenMP.append("\n")
        self.__rowsOpenMP.append("	int success = 1;\n")
        self.__rowsOpenMP.append("	for (i = 0; i < nElements; i++) {  \n")
        self.__rowsOpenMP.append("		if ( openMP_OUT[i] != %s) {\n" % self.__expectingResult)
        self.__rowsOpenMP.append("			success = 0;\n")
        self.__rowsOpenMP.append("			printf(\"openMP_OUT = %f\\n\", openMP_OUT[i]);\n")
        self.__rowsOpenMP.append("			fprintf(stderr, \"Validation failed at index %d\\n----------------------------------------\\n\", i);\n")
        self.__rowsOpenMP.append("			break;\n")
        self.__rowsOpenMP.append("		}\n")
        self.__rowsOpenMP.append("	}\n")
        self.__rowsOpenMP.append("\n")
        self.__rowsOpenMP.append("	if (success) {\n")
        self.__rowsOpenMP.append("		fprintf(stdout, \"Validation successful.\\n----------------------------------------\\n\");\n")
        self.__rowsOpenMP.append("	}\n")
        self.__rowsOpenMP.append("\n")

        self.dumpSaveResultsToCSV()

        self.__rowsOpenMP.append("	return 0;\n")
        self.__rowsOpenMP.append("}\n")

    def dumpSaveResultsToCSV(self):
        self.__rowsOpenMP.append("   // Save results to CSV\n")
        self.__rowsOpenMP.append("   FILE *fp;\n")
        self.__rowsOpenMP.append("\n")

        csvFileSavePath = self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName

        self.__rowsOpenMP.append("   if (argc > 2) {\n")
        self.__rowsOpenMP.append("       fp = fopen(argv[2], \"a+\");\n")
        self.__rowsOpenMP.append("       fprintf(fp, \"ParallelForOpenMP%s\");\n" % self.__csvSeparationCharacter)
        self.__rowsOpenMP.append("       fprintf(fp, \"%s%s\", nElements);\n"% ("%d", self.__csvSeparationCharacter))
        self.__rowsOpenMP.append("       fprintf(fp, \"%s%s\");\n" % (self.__fileName, self.__csvSeparationCharacter))
        self.__rowsOpenMP.append("       fprintf(fp, \"%s%s\", success);\n" % ("%d", self.__csvSeparationCharacter))
        self.__rowsOpenMP.append("       fprintf(fp, \"%f\\n\", (end-start)/(double)CLOCKS_PER_SEC);\n")
        self.__rowsOpenMP.append("       fclose(fp);\n")
        self.__rowsOpenMP.append("       printf(\"Saved results in %s\\n\", argv[2]);\n")
        self.__rowsOpenMP.append("   }\n")

    def walkOpenMP(self):

        # Clean the output list of lines
        self.__rowsOpenMP = []

        # Dump the makefile
        self.dumpOpenMP()

        return "".join(self.__rowsOpenMP)

    def toDisk(self):

        outputString = self.walkOpenMP()
        text_file = open(self.__directoryPath + "/" + "outputOpenMP" + ".c", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output OpenMP src on %s." % self.__directoryPath