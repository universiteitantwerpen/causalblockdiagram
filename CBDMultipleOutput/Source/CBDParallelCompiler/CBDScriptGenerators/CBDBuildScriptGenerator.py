__author__ = 'madks_000'

import os

class CBDBuildScriptGenerator():
    """
    A class that generates the build script that compiles and builds all the resulting src code
    """
    def __init__(self, compilerSettings):
        self.__rowsBuildScript = []
        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__fileName = compilerSettings.getFileName()
        self.optimizationFlag = compilerSettings.getOptimization()

    def dumpBuildScript(self, inFileName):
        self.__rowsBuildScript.append("chmod +x *\n")
        self.__rowsBuildScript.append("make\n")
        self.__rowsBuildScript.append("gcc -o outputAnsiC.o outputAnsiC.c\n")
        self.__rowsBuildScript.append("gcc -o outputOpenGL.o outputOpenGL.c -framework GLUT -w -framework OpenGL\n")
        self.__rowsBuildScript.append("icc -ltbb outputIntelTBB.cpp -std=c++11 -ltbbmalloc -o outputIntelTBB.o %s\n" % self.optimizationFlag)
        self.__rowsBuildScript.append("icc -openmp outputOpenMP.c -o outputOpenMP.o %s\n" % self.optimizationFlag)
        self.__rowsBuildScript.append("icc -openmp outputOpenMPSections.c -o outputOpenMPSections.o %s\n" % self.optimizationFlag)

    def walkBuildScript(self):

        # Clean the output list of lines
        self.__rowsBuildScript = []

        # Dump the makefile
        self.dumpBuildScript(self.__fileName)

        return "".join(self.__rowsBuildScript)

    def toDisk(self):

        outputString = self.walkBuildScript()
        text_file = open(self.__directoryPath + "/" + "BUILD" + ".sh", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output BUILD script on %s." % self.__directoryPath