__author__ = 'madks_000'

import os

class CBDMakefileGenerator():
    """
    A class that generates the Makefile required to build OpenCL GPU code, both for integrated and discrete GPU
    """
    def __init__(self, compilerSettings):
        self.__rowsMakefile = []
        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__fileName = compilerSettings.getFileName()
        self.optimizationFlag = compilerSettings.getOptimization()

    def dumpMakefile(self, inFileName):
        self.__rowsMakefile.append("# File: Makefile\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("TARGET = %s\n" % inFileName)
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("# #\n")
        self.__rowsMakefile.append("# The OpenCL compiler, used to compile OpenCL kernels.\n")
        self.__rowsMakefile.append("# #\n")
        self.__rowsMakefile.append("CLC = /System/Library/Frameworks/OpenCL.framework/Libraries/openclc\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("CC = cc\n")
        self.__rowsMakefile.append("ARCHS = -arch i386 -arch x86_64\n")
        # self.__rowsMakefile.append("CFLAGS = -c -Wall -g %s\n" % self.optimizationFlag)
        self.__rowsMakefile.append("CFLAGS = -c -g %s\n" % self.optimizationFlag)
        self.__rowsMakefile.append("FRAMEWORKS = -framework OpenCL\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("SOURCES = %s.c\n" % inFileName)
        self.__rowsMakefile.append("CL_SOURCES = %sKernel.cl\n" % inFileName)
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("# #\n")
        self.__rowsMakefile.append("# For each OpenCL C source file, we want to build:\n")
        self.__rowsMakefile.append("#\n")
        self.__rowsMakefile.append("#   file.cpu32.bc, file.cpu64.bc, file.gpu32.bc, file.gpu64.bc, where file\n")
        self.__rowsMakefile.append("#   is the source name preceding the .cl extension.\n")
        self.__rowsMakefile.append("# #\n")
        self.__rowsMakefile.append("BITCODE += ${CL_SOURCES:.cl=.cpu32.bc}\n")
        self.__rowsMakefile.append("BITCODE += ${CL_SOURCES:.cl=.cpu64.bc}\n")
        self.__rowsMakefile.append("BITCODE += ${CL_SOURCES:.cl=.gpu32.bc}\n")
        self.__rowsMakefile.append("BITCODE += ${CL_SOURCES:.cl=.gpu64.bc}\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("OBJECTS := ${SOURCES:.c=.o}\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("$(TARGET): $(BITCODE) $(OBJECTS)\n")
        self.__rowsMakefile.append("	$(CC) $(OBJECTS) -o $@ $(FRAMEWORKS) $(ARCHS)\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("%.o: %.c\n")
        self.__rowsMakefile.append("	$(CC) $(CFLAGS) $(ARCHS) $< -o $@\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("# #\n")
        self.__rowsMakefile.append("# The OpenCL C compilation commands for 32/64bit CPUs and GPUs:\n")
        self.__rowsMakefile.append("#\n")
        self.__rowsMakefile.append("# As an example, to compile for a 32bit GPU:\n")
        self.__rowsMakefile.append("# openclc -emit-llvm -c -arch gpu_32 kernel.cl -o kernel.bc\n")
        self.__rowsMakefile.append("# #\n")
        self.__rowsMakefile.append("%.cpu32.bc: %.cl\n")
        self.__rowsMakefile.append("	$(CLC) -emit-llvm -c -arch i386 $< -o $@\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("%.cpu64.bc: %.cl\n")
        self.__rowsMakefile.append("	$(CLC) -emit-llvm -c -arch x86_64 $< -o $@\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("%.gpu32.bc: %.cl\n")
        self.__rowsMakefile.append("	$(CLC) -emit-llvm -c -cl-std=CL1.2 -o3 -cl-denorms-are-zero -cl-auto-vectorize-enable -arch gpu_32 $< -o $@\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("%.gpu64.bc: %.cl\n")
        self.__rowsMakefile.append("	$(CLC) -emit-llvm -c -cl-std=CL1.2 -o3 -cl-denorms-are-zero -cl-auto-vectorize-enable -arch gpu_64 $< -o $@\n")
        self.__rowsMakefile.append("\n")
        self.__rowsMakefile.append("clean:\n")
        self.__rowsMakefile.append("	rm -rf $(TARGET) $(BITCODE) $(OBJECTS)\n")

    def walkMakefile(self):

        # Clean the output list of lines
        self.__rowsMakefile = []

        # Dump the makefile
        self.dumpMakefile(self.__fileName)

        return "".join(self.__rowsMakefile)

    def toDisk(self):
        outputString = self.walkMakefile()
        text_file = open(self.__directoryPath + "/" + "Makefile", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output Makefile on %s." % self.__directoryPath