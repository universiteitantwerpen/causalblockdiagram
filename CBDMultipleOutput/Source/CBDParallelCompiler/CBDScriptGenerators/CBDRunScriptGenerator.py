__author__ = 'madks_000'

import os

class CBDRunScriptGenerator():
    """
    A class that generates the RUN script files for all the compiled src code
    """
    def __init__(self, compilerSettings):
        self.__rowsRunScript = []
        self.__directoryPath = compilerSettings.getDirectoryPath()
        self.__fileName = compilerSettings.getFileName()

    def dumpRunScript(self, computeDevice):
        if (computeDevice == "gpu0"):
            self.__rowsRunScript.append("./%s '-t' gpu64 -i 0 -f %sKernel.gpu64.bc $1 $2" % (self.__fileName, self.__fileName))
        elif (computeDevice == "cpu0"):
            self.__rowsRunScript.append("./%s '-t' cpu32 -i 1 -f %sKernel.cpu32.bc $1 $2" % (self.__fileName, self.__fileName))
        else:
            self.__rowsRunScript.append("./%s '-t' gpu32 -i 1 -f %sKernel.gpu32.bc $1 $2" % (self.__fileName, self.__fileName))

    def walkRunScript(self, gpuDevice):

        # Clean the output list of lines
        self.__rowsRunScript = []

        # Dump the makefile
        self.dumpRunScript(gpuDevice)

        return "".join(self.__rowsRunScript)

    def toDisk(self):

        # Generate additional Scripts
        text_file = open(self.__directoryPath + "/" + "RUN_ANSIC_CPU" + ".sh", "w")
        text_file.write("./outputAnsiC.o $1 $2")
        text_file.close()

        text_file = open(self.__directoryPath + "/" + "RUN_OPENMP_CPU" + ".sh", "w")
        text_file.write("./outputOpenMP.o $1 $2")
        text_file.close()

        text_file = open(self.__directoryPath + "/" + "RUN_OPENMP_SECTIONS" + ".sh", "w")
        text_file.write("./outputOpenMPSections.o $1 $2")
        text_file.close()

        text_file = open(self.__directoryPath + "/" + "RUN_INTEL_TBB_CPU" + ".sh", "w")
        text_file.write("./outputIntelTBB.o $1 $2")
        text_file.close()

        text_file = open(self.__directoryPath + "/" + "RUN_3D_GRAPH_OPENGL" + ".sh", "w")
        text_file.write("./outputOpenGL.o $1 $2")
        text_file.close()

        text_file = open(self.__directoryPath + "/" + "RUN_ALL_BENCHMARKS" + ".sh", "w")
        text_file.write("./RUN_ANSIC_CPU.sh \"$1\" \"$2\" && ./RUN_OPENMP_CPU.sh \"$1\" \"$2\" && ./RUN_INTEL_TBB_CPU.sh \"$1\" \"$2\" && ./RUN_GPU_0.sh \"$1\" \"$2\" && ./RUN_GPU_1.sh \"$1\" \"$2\"")
        # text_file.write("./RUN_INTEL_TBB_CPU.sh \"$1\" \"$2\"")
        text_file.close()

        text_file = open(self.__directoryPath + "/" + "RUN_ALL" + ".sh", "w")
        text_file.write("./RUN_ANSIC_CPU.sh && ./RUN_OPENMP_CPU.sh && ./RUN_INTEL_TBB_CPU.sh && ./RUN_GPU_0.sh && ./RUN_GPU_1.sh && ./RUN_3D_GRAPH_OPENGL.sh")
        text_file.close()

        outputString = self.walkRunScript("gpu0")
        text_file = open(self.__directoryPath + "/" + "RUN_GPU_0" + ".sh", "w")
        text_file.write(outputString)
        text_file.close()

        outputString = self.walkRunScript("gpu1")
        text_file = open(self.__directoryPath + "/" + "RUN_GPU_1" + ".sh", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output RUN scripts %s." % self.__directoryPath