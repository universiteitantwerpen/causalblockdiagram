__author__ = 'madks_000'

import os

class CBDBenchmarkScriptGenerator():
    """
    A class that generates the script that compiles, builds, runs all the resulting src code and stores the result to a
     CSV file
    """
    def __init__(self, compilerSettings):
        self.__rowsBuildScript = []
        self.__currentPath = compilerSettings.getCurrentPath()
        self.__fileName = compilerSettings.getFileName()
        self.optimizationFlag = compilerSettings.getOptimization()
        self.__benchmarkFileName = compilerSettings.getBenchmarkFileName()
        self.__outputDirectories = []
        self.__outputDirectories = ["outputParallelLoopSource1", "outputParallelSource1", "outputParallelSource2", "outputParallelSource3", "outputParallelSource4"]
        self.__csvSeparationCharacter = compilerSettings.getCsvSeparationCharacter()

    def dumpBuildScript(self, inFileName):
        self.__rowsBuildScript.append("echo -e \"Platform%sIterations%sModelName%sSuccess%sTime\" > \"%s\"\n" % (self.__csvSeparationCharacter, self.__csvSeparationCharacter, self.__csvSeparationCharacter, self.__csvSeparationCharacter, self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName))
        if len(self.__outputDirectories):

            iterationRange = []
            for x in range(5,8):
                iterationRange.append(1.0 * (10 ** x))
                iterationRange.append(1.35 * (10 ** x))
                iterationRange.append(1.75 * (10 ** x))
                iterationRange.append(2.35 * (10 ** x))
                iterationRange.append(3.0 * (10 ** x))
                iterationRange.append(4.2 * (10 ** x))
                iterationRange.append(5.5 * (10 ** x))
                iterationRange.append(7.5 * (10 ** x))
            # print iterationRange
            #iterationRange = [5 * 10 ** x for x in range(0,8)]

            for currentDirectory in self.__outputDirectories:
                self.__rowsBuildScript.append("echo '-- Entering %s'\n" % currentDirectory)
                self.__rowsBuildScript.append("cd %s\n" % currentDirectory)
                for currentIterations in iterationRange:
                    self.__rowsBuildScript.append("chmod +x * && make && ./BUILD.sh && ./RUN_ALL_BENCHMARKS.sh %s %s\n" % (currentIterations, self.__currentPath + "/outputBenchmarkResults/" + self.__benchmarkFileName))
                self.__rowsBuildScript.append("echo '-- Done with %s'\n" % currentDirectory)
                self.__rowsBuildScript.append("cd ..\n")

    def walkBenchmarkScript(self):

        # Clean the output list of lines
        self.__rowsBuildScript = []

        # Dump the makefile
        self.dumpBuildScript(self.__fileName)

        return "".join(self.__rowsBuildScript)

    def toDisk(self):

        outputString = self.walkBenchmarkScript()
        text_file = open(self.__currentPath + "/" + "BENCHMARK" + ".sh", "w")
        text_file.write(outputString)
        text_file.close()

        print "Done saving the output BENCHMARK script on %s." % self.__currentPath

        if not os.path.exists(self.__currentPath + "/outputBenchmarkResults/"):
            os.makedirs(self.__currentPath + "/outputBenchmarkResults/")

        print "Done saving the output BENCHMARK CSV file on %s." % self.__currentPath