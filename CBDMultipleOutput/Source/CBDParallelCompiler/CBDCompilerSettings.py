__author__ = 'madks_000'

import time
import os

class CBDCompilerSettings():
    """
    Helper class that provides common information of the current CBD model to all the output Source code traces
    """
    def __init__(self, rand, iterations, pipelineStages, cbd, directoryPath, numMaxThreads, fileName, optimizedPipelineStagesDictionary, optimizationFastest=True, expectingResult=0.0):
        self.__rand = rand
        self.__iterations = iterations
        self.__pipelineStages = pipelineStages
        self.__cbd = cbd
        self.__currentPath = directoryPath

        self.__setBenchmarkFileName()

        self.__directoryPath = directoryPath  + "/" + fileName
        self.__numMaxThreads = numMaxThreads
        self.__fileName = fileName
        self.__optimizationString = self.getNoOptimization()
        self.__signalCount = len(self.__cbd.getBlocks())
        self.__optimizedPipelineStagesDictionary = {}
        self.__optimizedPipelineStagesDictionary = optimizedPipelineStagesDictionary
        self.__expectingResult = expectingResult
        self.__gaussianSolverInput = cbd.getGaussianSolverInput()
        self.__strongComponentNames = []
        if len(self.__gaussianSolverInput) > 0:
            self.__strongComponentNames = self.__gaussianSolverInput[2]
        self.__constantBlockNames = [x.getBlockName() for x in self.__cbd.getBlocks() if x.getBlockType() == "ConstantBlock"]
        self.__blockNamesList = [x.getBlockName() for x in self.__cbd.getBlocks()]
        self.__csvSeparationCharacter = ','

        if optimizationFastest:
            self.__optimizationString = self.getOptimizationFastest()

    def __setBenchmarkFileName(self):
        timeNow = time.localtime(time.time())
        self.__benchmarkFileName = "benchmarkResults" + str(timeNow.tm_hour) + str(timeNow.tm_min) + str(timeNow.tm_sec) + ".csv"

    def getCsvSeparationCharacter(self):
        return self.__csvSeparationCharacter

    def getBenchmarkFileName(self):
        return self.__benchmarkFileName

    def getConstantBlockNames(self):
        return self.__constantBlockNames

    def getStrongComponentNames(self):
        return self.__strongComponentNames

    def getGaussianSolverInput(self):
        return self.__gaussianSolverInput

    def getRand(self):
        return self.__rand

    def getDirectoryPath(self):
        return self.__directoryPath

    def getCurrentPath(self):
        return self.__currentPath

    def getIterations(self):
        return self.__iterations

    def getPipelineStages(self):
        return self.__pipelineStages

    def getExpectingResult(self):
        return self.__expectingResult

    def getCBD(self):
        return self.__cbd

    def getMaxThreads(self):
        return self.__numMaxThreads

    def getFileName(self):
        return self.__fileName

    def getOptimizationFastest(self):
        return "-O3"

    def getNoOptimization(self):
        return "-O0"

    def getOptimization(self):
        return self.__optimizationString

    def getSignalCount(self):
        return self.__signalCount

    def getBlockNames(self):
        return self.__blockNamesList

    def getOptimizedPipelineStagesDictionary(self):
        return self.__optimizedPipelineStagesDictionary