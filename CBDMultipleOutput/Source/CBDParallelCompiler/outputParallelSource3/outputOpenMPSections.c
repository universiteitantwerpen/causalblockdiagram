/*
	OpenMP output src code from the CBD model simulation for mac
	applying data/domain decomposition
	Do check the environment variables first (i.e):
	export DYLD_LIBRARY_PATH="/opt/intel/lib" // for Intel C++ open mp compiler for Mac
	export OMP_NUM_THREADS=4 // The number of maximum threads
	export OMP_STACKSIZE="50M" // Small stack size causes fragmentation errors, increase accordingly
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <omp.h>

// The number of total simulation steps
#define NELEMENTS 5000000
// The number of maxinum spawning threads
#define MAX_THREADS 4

// Main execution path
int main (int argc, char* const *argv)
{
	clock_t start, end;
	
	unsigned int nElements = NELEMENTS;
    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	// OpenMP variables: number of total threads, current thread id, min I item, max I item
	int currentThreadId;
	int maxThreads = MAX_THREADS;
	unsigned int minI, maxI;
	
	// Enforce the number of maximum threads
	omp_set_num_threads(maxThreads);
	
	// Job size/number of iterations for each thread
	unsigned int threadJobSize = nElements / maxThreads;
	
	printf("============= PIPELINE CPU =============\nNumber of discrete time steps: %d\n\n",nElements);	
	
	printf("Using: OpenMP & Optimized Pipeline\n");
	printf("Number of threads: %d\n", maxThreads);	
	printf("Number of Stages: %d\n", maxThreads);	

	// Start the execution time counter for OpenCL
	start = clock();
	
	// Create the input/constant variables
	unsigned int i = 0;
	float *openMP_C1 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_C2 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_C3 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_C4 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_C5 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_OUT = (float*)malloc(sizeof(float)*nElements);

	// Pack input variables with data
	for (i = 0; i < nElements; i++) {
		openMP_C1[i] = 5.0;
		openMP_C2[i] = 3.0;
		openMP_C3[i] = 5.0;
		openMP_C4[i] = 5.0;
		openMP_C5[i] = 5.0;
	}

		// Iteration counter for the current thread
		unsigned int i_mp = 0;

	    // Initialize private intermediate variables
       	float openMP_D = 0.0;
       	float openMP_A1 = 0.0;
       	float openMP_A2 = 0.0;
       	float openMP_N1 = 0.0;
       	float openMP_A3 = 0.0;
       	float openMP_M1 = 0.0;
       	float openMP_ML1 = 0.0;
       	float openMP_A4 = 0.0;
       	float openMP_R1 = 0.0;
       	float openMP_I1 = 0.0;
       	float openMP_G1 = 0.0;

	    // Iterate/calculate through all the steps via RoundRobin fashion
		for (i_mp = 0; i_mp < nElements; i_mp++) {
	       openMP_D = 0.0;
	       openMP_A1 = 0.0;
	       openMP_A2 = 0.0;
	       openMP_N1 = 0.0;
	       openMP_A3 = 0.0;
	       openMP_M1 = 0.0;
	       openMP_ML1 = 0.0;
	       openMP_A4 = 0.0;
	       openMP_R1 = 0.0;
	       openMP_I1 = 0.0;
	       openMP_G1 = 0.0;

	    	#pragma omp parallel sections private(openMP_D, openMP_A1, openMP_A2, openMP_N1, openMP_A3, openMP_M1, openMP_ML1, openMP_A4, openMP_R1, openMP_I1, openMP_G1)
		    {
		    	#pragma omp section
		    	{
				openMP_A1 = openMP_D + openMP_C3[i_mp];
				openMP_A2 = openMP_C4[i_mp] + openMP_C5[i_mp];
				openMP_N1 = (- 1.0) * openMP_A2;
		    	}
		    	#pragma omp section
		    	{
				openMP_A3 = openMP_A2 + openMP_C5[i_mp];
				openMP_M1 = openMP_A1 * openMP_N1;
				openMP_ML1 = openMP_M1 * openMP_A2;
		    	}
		    	#pragma omp section
		    	{
				openMP_A4 = openMP_C1[i_mp] + openMP_C2[i_mp];
				openMP_R1 = pow(openMP_A4, openMP_C2[i_mp]);
				openMP_I1 = 1.0 / openMP_R1;
		    	}
		    	#pragma omp section
		    	{
				openMP_G1 = fabs(openMP_N1);
				openMP_OUT[i_mp] = openMP_G1;
		    	}
	         }// OpenMP sections region ends here

	      }

	// Stop the clock
	end = clock();
	printf("Execution time(secs): %f\n\n", (end-start)/(double)CLOCKS_PER_SEC );

	// Validate the results -optional- should use if we already know the final results

	int success = 1;
	for (i = 0; i < nElements; i++) {  
		if ( openMP_OUT[i] != 10.0) {
			success = 0;
			printf("openMP_OUT = %f\n", openMP_OUT[i]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "PipeLineOpenMP,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelSource3,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", (end-start)/(double)CLOCKS_PER_SEC);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
