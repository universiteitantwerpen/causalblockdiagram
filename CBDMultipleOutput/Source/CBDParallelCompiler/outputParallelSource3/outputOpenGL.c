/*
	Generated OpenGL code to display a 3D graph of CBD models
*/

#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// Constants
#define xMIN 0.0
#define xMAX 10.0
#define yMIN 0.0
#define yMAX 10.0
#define zMIN 0.0
#define zMAX 10.0
#define zSTEP 0.1
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 800
#define TIME_STEP 0.05
#define VALUES_MULTIPLIER 10.0
#define GRAPH_STEP 0.1
#define GRAPH_MIN 0.0
#define GRAPH_MAX 10.0
#define GRID_MAX 1.0
#define GRID_STEP 0.1
#define SIGNAL_SIZE_MAX 200

// Function Prototypes
void display();
void specialKeys();

// Global Variables
float rotate_y= -20.0; // Starting x, y camera angle
float rotate_x= 70.0;
float angle=0.0; // angle of rotation for the camera direction
float lx=0.0f,lz=-1.0f; // actual vector representing the camera's direction
float x=0.5f,z=0.5f, y = 0.0f; // XZ position of the camera
float scalePercentage = 1.0;
int showGrid = 1; // Toggle grid on/off
int showAreas = 1; // Toggle grid on/off

// Helpful function to draw a char* array of characters
void drawString (void * font, char *s, float x, float y, float z) {
	unsigned int i;
	glRasterPos3f(x, y, z);
	for (i = 0; i < strlen (s); i++)
		glutBitmapCharacter (font, s[i]);
}

// Helps in looping through all the colours based on ID
void setColour(int colour) {
	switch(colour) 
	{
		case 0: // White
			glColor3f(1.0, 1.0, 1.0);
			break;
		case 1: // Green
			glColor3f(0.0, 1.0, 0.0);
			break;
		case 2: // Blue
			glColor3f(0.0, 0.0, 1.0);
			break;
		case 3: // Yellow
			glColor3f(1.0, 1.0, 0.0);
			break;
		case 4: //
			glColor3f(0.0, 1.0, 1.0);
			break;
		case 5: //
			glColor3f(1.0, 0.0, 1.0);
			break;
		case -1: // Red
			glColor3f(1.0, 0.0, 0.0);
			break;
	}
}

// Draw the 3d Cartesian system without any points
void draw3DCartesianSystem() {

	// Print stationary help text
	glColor3f(1.0, 1.0, 1.0);
	drawString(GLUT_BITMAP_TIMES_ROMAN_24, "Signals vs Time Steps", x -0.2, y + 1.2, z); // Chart Legend	
	glColor3f(1.0, 1.0, 1.0);
	drawString(GLUT_BITMAP_HELVETICA_12, "Rotation: UP, DOWN, LEFT, RIGHT keys.", x - 0.9, y - 1.35, z);
	drawString(GLUT_BITMAP_HELVETICA_12, "Scale IN/OUT: 1, 2 keys.", x - 0.9, y - 1.3, z);
	drawString(GLUT_BITMAP_HELVETICA_12, "Toggle grid: 3 key.", x - 0.9, y - 1.25, z);
	drawString(GLUT_BITMAP_HELVETICA_12, "Toggle 2D Signal Areas: 4 key.", x - 0.9, y - 1.4, z);

	// Rotate when user changes rotate_x and rotate_y angles
	glRotatef(rotate_x, 2.0, 0.0, 0.0 );
	glRotatef(rotate_y, 0.0, 2.0, 0.0 );

	// Set the zoom scale
	glScalef(scalePercentage, scalePercentage, scalePercentage);

	// Draw x, y, z Axis
	glLineWidth(4);
	glBegin(GL_LINES);	
	// Axis X
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(xMIN, 0.0, 0.0);
	glVertex3f(xMAX, 0.0, 0.0);	
	// Axis Y
	glColor3f(1.0, 1.0, 0.0);
	glVertex3f(0.0, yMIN, 0.0);
	glVertex3f(0.0, yMAX, 0.0);
	// Axis Z
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, zMIN);
	glVertex3f(0.0, 0.0, zMAX); 
	glEnd();

	// Draw Axis labels
	glColor3f(1.0,  1.0,  1.0);
	drawString(GLUT_BITMAP_HELVETICA_18, "x (Time Step)", 1.1, -0.2, 0);
	drawString(GLUT_BITMAP_HELVETICA_18, "y (Value)", -0.2, 1.1, 0);
	drawString(GLUT_BITMAP_HELVETICA_18, "z (Signal)", 0, -0.2, 1.1); 

	// Draw Grid lines
	if (showGrid == 1) 	{
		glLineWidth(1);
		glBegin(GL_LINES);	
		for (float i = GRAPH_MIN; i < GRAPH_MAX; i+=0.1) {
			// Plane X
		    glColor3f(0.0, 1.0, 0.0);
			glVertex3f(xMIN, i, 0.0);
			glVertex3f(xMAX, i, 0.0);
			if ((int)(i * 10) % 2 == 0) {
				glVertex3f(i, yMIN, 0.0);
				glVertex3f(i, yMAX, 0.0);
			}
			// Plane Z
		    glColor3f(0.0, 0.0, 1.0);
			glVertex3f(zMIN, 0.0, i);
			glVertex3f(zMAX, 0.0, i); 
			if ((int)(i * 10) % 2 == 0.0) {
				glVertex3f(i, 0.0, zMIN);
				glVertex3f(i, 0.0, zMAX); 
			}
		}
		glEnd();
	}

	// Draw axis division digit labels
	glColor3f(1.0,  1.0,  0.0);	
	for (float i = xMIN; i <= xMAX; i+=GRAPH_STEP) {
		char digits[5];

		snprintf(digits, sizeof(digits), "%f", i * VALUES_MULTIPLIER);
		drawString(GLUT_BITMAP_HELVETICA_10, digits, i, -0.05, 0);
		drawString(GLUT_BITMAP_HELVETICA_10, digits, -0.05, i, 0);
	}
}

// Helpful function to draw CBD signals from a float array
void drawSignal(char* label, float* points, int signalID) {

	setColour(signalID % 6);// Set Colour rotation based on ID
	
	drawString(GLUT_BITMAP_HELVETICA_10, label, -0.1, 0.0, zSTEP * signalID); // Label
	
	// Draw points
	glBegin(GL_POINTS);
	glPushMatrix();
	glTranslated(0.0, 0.0, 0.0); // draw an empty circle, hack to remove a bug
	glutSolidSphere(0.0, 50.0, 50.0);
	glPopMatrix(); 
	
	for (unsigned int i = 0; i < SIGNAL_SIZE_MAX; i++) {
		glPushMatrix();
		glTranslated((float)(TIME_STEP * i), points[i] / VALUES_MULTIPLIER, zSTEP * signalID);
		glutWireSphere(0.01, 50.0, 50.0);
		glPopMatrix(); 
	}	
	glEnd();
	
	//Draw Lines or 2D Areas with the xX' axis
	unsigned int first = 0; // ID of the previous points' y value	
	if (showAreas == 0)
	{
		// Connect each point with a line
		glLineWidth(1);
		glBegin(GL_LINES);
		glColor3f(0.8, 0.8, 1.0);
		setColour(signalID % 6);
		unsigned int first = 0; // ID of the previous points' y value
		for (unsigned int i = 1; i < SIGNAL_SIZE_MAX; i++) {
			glVertex3f((float)(TIME_STEP * (i - 1)), points[first] / VALUES_MULTIPLIER, zSTEP * signalID);
			glVertex3f((float)(TIME_STEP * i), points[i] / VALUES_MULTIPLIER, zSTEP * signalID);
			first = i;
		}
		glEnd();
	} else {
		// Draw trapezoids - integrals below each line
		glLineWidth(1);
		glBegin(GL_QUADS);
		glColor3f(0.8, 0.8, 1.0);
		setColour(signalID % 6);	
		for (unsigned int i = 1; i < SIGNAL_SIZE_MAX; i++) {
			glVertex3f((float)(TIME_STEP * (i - 1)), points[first] / VALUES_MULTIPLIER, zSTEP * signalID);
			glVertex3f((float)(TIME_STEP * i), points[i] / VALUES_MULTIPLIER, zSTEP * signalID);
			glVertex3f((float)(TIME_STEP * (i - 1)), 0.0f , zSTEP * signalID);
			glVertex3f((float)(TIME_STEP * i), 0.0f, zSTEP * signalID);
			first = i;
		}
		glEnd();
	}}

// Display Callback function
void display() {

	//  Clear screen and Z-buffer
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();

	// Set the camera viewpoint
	gluLookAt(x, y, z,
		x+lx, 1.0f,  z+lz,
		0.0f, 2.0f,  0.0f);

	// Print helpful information to the console
	printf("scale, eyesx, eyesy, eyesz, rotate_x, rotate_y: %f %f %f %f %f \n", scalePercentage, x, y, z, rotate_x, rotate_y);

	// Draw the grid, axis, labels, divisions
	draw3DCartesianSystem();

	// Create the host (local) variables
	unsigned int i = 0;
	float *ansiC_C1 = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);
	float *ansiC_C2 = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);
	float *ansiC_C3 = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);
	float *ansiC_C4 = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);
	float *ansiC_C5 = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);
	float *ansiC_OUT = (float*)malloc(sizeof(float)*SIGNAL_SIZE_MAX);

	// Pack Ansi C variables with data
	for (i = 0; i < SIGNAL_SIZE_MAX; i++) {
		ansiC_C1[i] = 5.0;
		ansiC_C2[i] = 3.0;
		ansiC_C3[i] = 5.0;
		ansiC_C4[i] = 5.0;
		ansiC_C5[i] = 5.0;
	}

	// Initialize intermediate variables
	float ansiC_D = 0.0;
	float ansiC_A1 = 0.0;
	float ansiC_A2 = 0.0;
	float ansiC_N1 = 0.0;
	float ansiC_A3 = 0.0;
	float ansiC_M1 = 0.0;
	float ansiC_ML1 = 0.0;
	float ansiC_A4 = 0.0;
	float ansiC_R1 = 0.0;
	float ansiC_I1 = 0.0;
	float ansiC_G1 = 0.0;

	// Calculate data
	for (i = 0; i < SIGNAL_SIZE_MAX; i++) {
	    ansiC_D = 0.0;
	    ansiC_A1 = 0.0;
	    ansiC_A2 = 0.0;
	    ansiC_N1 = 0.0;
	    ansiC_A3 = 0.0;
	    ansiC_M1 = 0.0;
	    ansiC_ML1 = 0.0;
	    ansiC_A4 = 0.0;
	    ansiC_R1 = 0.0;
	    ansiC_I1 = 0.0;
	    ansiC_G1 = 0.0;

        ansiC_A1 = ansiC_D + ansiC_C3[i];
        ansiC_A2 = ansiC_C4[i] + ansiC_C5[i];
        ansiC_N1 = (- 1.0) * ansiC_A2;
        ansiC_A3 = ansiC_A2 + ansiC_C5[i];
        ansiC_M1 = ansiC_A1 * ansiC_N1;
        ansiC_ML1 = ansiC_M1 * ansiC_A2;
        ansiC_A4 = ansiC_C1[i] + ansiC_C2[i];
        ansiC_R1 = pow(ansiC_A4, ansiC_C2[i]);
        ansiC_I1 = 1.0 / ansiC_R1;
        ansiC_G1 = fabs(ansiC_N1);
        ansiC_OUT[i] = ansiC_G1;
	}

	// Do draw all the signals
	drawSignal("OUT", ansiC_OUT, 0);
	drawSignal("C1", ansiC_C1, 1);
	drawSignal("C2", ansiC_C2, 2);
	drawSignal("C3", ansiC_C3, 3);
	drawSignal("C4", ansiC_C4, 4);
	drawSignal("C5", ansiC_C5, 5);

	glFlush();
	glutSwapBuffers();
}

// Callback for key presses
void specialKeys( int key, int x, int y ) {

	// Right arrow - increase rotation by 5 degrees
	if (key == GLUT_KEY_RIGHT)
		rotate_y += 5; 
	// Left arrow - decrease rotation by 5 degree
	else if (key == GLUT_KEY_LEFT)
		rotate_y -= 5; 
	else if (key == GLUT_KEY_UP)
		rotate_x -= 5; 
	else if (key == GLUT_KEY_DOWN)
		rotate_x += 5;
	// Keys 1, 2 for scale in/out, 3 to toggle grid 
	else if (key == 49)
		scalePercentage+=0.1;
	else if (key == 50)
		scalePercentage-=0.1;
	else if (key == 51)
		if (showGrid == 1)
			showGrid = 0;
		else
			showGrid = 1;
	else if (key == 52)
		if (showAreas == 1)
			showAreas = 0;
		else
			showAreas = 1;

	//  Request display update
	glutPostRedisplay();
}

// Main execution point
int main(int argc, char* argv[]){

	// Initialize GLUT and process user parameters
	glutInit(&argc,argv);

	// Request double buffered true color window with Z-buffer
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

	// Create window
	glutInitWindowPosition(100,100);	
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutCreateWindow("CBD 3D Graph");

	// Enable Z-buffer depth test
	glEnable(GL_DEPTH_TEST);

	// Callback functions
	glutDisplayFunc(display);
	glutSpecialFunc(specialKeys);

	// Pass control to GLUT for events
	glutMainLoop();

	return 0;
}