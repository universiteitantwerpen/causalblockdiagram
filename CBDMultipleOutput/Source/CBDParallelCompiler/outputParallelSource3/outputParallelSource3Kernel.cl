/*
This kernel computes a flattened CBD with multiple input values as constants and one output
*/

// Optimization 1: The restrict qualifier for the kernel arugments aids in caching but you MUST ensure that the
// pointers do not point on overlapping locations.
// Optimization 2: Enabling double precision floating point (which is also faster than single precision
// since no casting is used to downgrade the accuracy
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

kernel void cbdCompute(global const float* restrict C1, global const float* restrict C2, global const float* restrict C3, global const float* restrict C4, global const float* restrict C5, global float* restrict OUT) {
	size_t i = get_global_id(0);


    __private float D = 0.0;
    __private float A1 = 0.0;
    __private float A2 = 0.0;
    __private float N1 = 0.0;
    __private float A3 = 0.0;
    __private float M1 = 0.0;
    __private float ML1 = 0.0;
    __private float A4 = 0.0;
    __private float R1 = 0.0;
    __private float I1 = 0.0;
    __private float G1 = 0.0;

    A1 = D + C3[i];
    A2 = C4[i] + C5[i];
    N1 = (- 1.0) * A2;
    A3 = A2 + C5[i];
    M1 = A1 * N1;
    ML1 = M1 * A2;
    A4 = C1[i] + C2[i];
    R1 = pow(A4, C2[i]);
    I1 = 1.0 / R1;
    G1 = fabs(N1);
    OUT[i] = G1;
}
