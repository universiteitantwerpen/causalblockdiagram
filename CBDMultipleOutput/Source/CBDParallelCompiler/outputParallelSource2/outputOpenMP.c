/*
	OpenMP output src code from the CBD model simulation for mac
	applying data/domain decomposition
	Do check the environment variables first (i.e):
	export DYLD_LIBRARY_PATH="/opt/intel/lib" // for Intel C++ open mp compiler for Mac
	export OMP_NUM_THREADS=4 // The number of maximum threads
	export OMP_STACKSIZE="50M" // Small stack size causes fragmentation errors, increase accordingly
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <omp.h>

// The number of total simulation steps
#define NELEMENTS 52000000
// The number of maxinum spawning threads
#define MAX_THREADS 4

// Main execution path
int main (int argc, char* const *argv)
{
	clock_t start, end;
	
	unsigned int nElements = NELEMENTS;
    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	// OpenMP variables: number of total threads, current thread id, min I item, max I item
	int currentThreadId;
	int maxThreads = MAX_THREADS;
	unsigned int minI, maxI;
	
	// Enforce the number of maximum threads
	omp_set_num_threads(maxThreads);
	
	// Job size/number of iterations for each thread
	unsigned int threadJobSize = nElements / maxThreads;
	
	printf("=========== PARALLEL_FOR CPU ===========\nNumber of discrete time steps: %d\n\n",nElements);	
	
	printf("Using: OpenMP & CPU\n");
	printf("Number of threads: %d\n", maxThreads);	
	printf("Job size per thread: %d\n",threadJobSize);	

	// Start the execution time counter for OpenMP
	start = clock();
	
	// Create the input/constant variables
	unsigned int i = 0;
	float *openMP_C1 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_C2 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_C3 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_C4 = (float*)malloc(sizeof(float)*nElements);
	float *openMP_OUT = (float*)malloc(sizeof(float)*nElements);

	// Pack input variables with data
	for (i = 0; i < nElements; i++) {
		openMP_C1[i] = 3.3;
		openMP_C2[i] = 5;
		openMP_C3[i] = 12;
		openMP_C4[i] = -2;
	}

	// Do Calculate CBD
	
	// Iteration counter for the current thread
	unsigned int i_mp = 0;

	    // Initialize private intermediate variables
       	float openMP_P = 0.0;
       	float openMP_A1 = 0.0;
       	float openMP_N1 = 0.0;
       	float openMP_N2 = 0.0;
       	float openMP_I1 = 0.0;

	#pragma omp parallel for private(openMP_P, openMP_A1, openMP_N1, openMP_N2, openMP_I1)
	for (i_mp = 0; i_mp < nElements; i_mp++) {

	       openMP_P = 0.0;
	       openMP_A1 = 0.0;
	       openMP_N1 = 0.0;
	       openMP_N2 = 0.0;
	       openMP_I1 = 0.0;

        openMP_P = openMP_C1[i_mp] * openMP_C2[i_mp];
        openMP_A1 = openMP_P + openMP_C3[i_mp];
        openMP_N1 = (- 1.0) * openMP_A1;
        openMP_N2 = (- 1.0) * openMP_C4[i_mp];
        openMP_I1 = 1.0 / openMP_N2;
        openMP_OUT[i_mp] = openMP_I1;
	}

	// Stop the clock
	end = clock();
	printf("Execution time(secs): %f\n\n", (end-start)/(double)CLOCKS_PER_SEC );

	// Validate the results -optional- should use if we already know the final results

	int success = 1;
	for (i = 0; i < nElements; i++) {  
		if ( openMP_OUT[i] != 0.5) {
			success = 0;
			printf("openMP_OUT = %f\n", openMP_OUT[i]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "ParallelForOpenMP,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelSource2,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", (end-start)/(double)CLOCKS_PER_SEC);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
