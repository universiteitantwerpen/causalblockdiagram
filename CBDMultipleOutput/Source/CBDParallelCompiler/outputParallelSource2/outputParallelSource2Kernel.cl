/*
This kernel computes a flattened CBD with multiple input values as constants and one output
*/

// Optimization 1: The restrict qualifier for the kernel arugments aids in caching but you MUST ensure that the
// pointers do not point on overlapping locations.
// Optimization 2: Enabling double precision floating point (which is also faster than single precision
// since no casting is used to downgrade the accuracy
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

kernel void cbdCompute(global const float* restrict C1, global const float* restrict C2, global const float* restrict C3, global const float* restrict C4, global float* restrict OUT) {
	size_t i = get_global_id(0);


    __private float P = 0.0;
    __private float A1 = 0.0;
    __private float N1 = 0.0;
    __private float N2 = 0.0;
    __private float I1 = 0.0;

    P = C1[i] * C2[i];
    A1 = P + C3[i];
    N1 = (- 1.0) * A1;
    N2 = (- 1.0) * C4[i];
    I1 = 1.0 / N2;
    OUT[i] = I1;
}
