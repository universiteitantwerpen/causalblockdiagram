/*
	Intel TBB Pipeline output src code from the CBD model simulation for mac
*/

#include <algorithm>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>
#include "tbb/pipeline.h"
#include "tbb/tick_count.h"
#include "tbb/cache_aligned_allocator.h"

// The number of total simulation steps
#define NELEMENTS 52000000
// The number of maxinum tokens
#define MAX_TOKENS 4

using namespace std;
using namespace tbb;

// Main execution path
int main (int argc, char* const *argv) {

	// Memory allocators to avoid false sharing
	tbb::cache_aligned_allocator<float> floatCacheAlignedAllocator;
	tbb::cache_aligned_allocator<unsigned int> uIntCacheAlignedAllocator;

	// Create the input/constant variables
	unsigned int nElements = *uIntCacheAlignedAllocator.allocate(1);
	unsigned int i_tbb = *uIntCacheAlignedAllocator.allocate(1);
	nElements = NELEMENTS;
	i_tbb = 0;

    if (argc > 1) {
        nElements = atoi(argv[1]);
    }
	printf("============ TBB PIPELINE ==============\nNumber of discrete time steps: %d\n\n",nElements);	
	
	printf("Using: IntelTBB & Optimized Pipeline\n");
	printf("Number of Tokens: %d\n", MAX_TOKENS);	

	// Start the execution time counter for OpenCL
	tbb::tick_count mainStartTime = tbb::tick_count::now();
	
	float *intelTBB_C1 = floatCacheAlignedAllocator.allocate(nElements);
	float *intelTBB_C2 = floatCacheAlignedAllocator.allocate(nElements);
	float *intelTBB_C3 = floatCacheAlignedAllocator.allocate(nElements);
	float *intelTBB_C4 = floatCacheAlignedAllocator.allocate(nElements);
	float *intelTBB_OUT = floatCacheAlignedAllocator.allocate(nElements);

	// Pack input variables with data
	for (i_tbb = 0; i_tbb < nElements; i_tbb++) {
		intelTBB_C1[i_tbb] = 3.3;
		intelTBB_C2[i_tbb] = 5;
		intelTBB_C3[i_tbb] = 12;
		intelTBB_C4[i_tbb] = -2;
	}

    // Initialize private intermediate variables
    float intelTBB_P = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_A1 = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_N1 = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_N2 = *floatCacheAlignedAllocator.allocate(1);
    float intelTBB_I1 = *floatCacheAlignedAllocator.allocate(1);

    i_tbb = 0;

    // Pipeline Start
    parallel_pipeline( /*max_number_of_live_token=*/MAX_TOKENS,
        // First Stage Start
        tbb::make_filter<void,int>(
            filter::serial_in_order,
            [&](flow_control& fc)-> int{
                if(i_tbb < nElements) {

					intelTBB_P = 0.0;
					intelTBB_A1 = 0.0;
					intelTBB_N1 = 0.0;
					intelTBB_N2 = 0.0;
					intelTBB_I1 = 0.0;

				intelTBB_P = intelTBB_C1[i_tbb] * intelTBB_C2[i_tbb];
                    return i_tbb++;
                 } else {
                    fc.stop();
                    return NULL;
                }
            }
        ) &
		// First Stage end
        // Stage 1 start
        make_filter<int,int>(
            filter::serial_in_order,
            [&](int i_tbb){
				intelTBB_A1 = intelTBB_P + intelTBB_C3[i_tbb];
                return i_tbb;
            }
        ) &
        // Stage 1 End
        // Stage 2 start
        make_filter<int,int>(
            filter::serial_in_order,
            [&](int i_tbb){
				intelTBB_N1 = (- 1.0) * intelTBB_A1;
                return i_tbb;
            }
        ) &
        // Stage 2 End
        // Last Stage 3 start
        make_filter<int,void>(
            filter::serial_in_order,
            [&](int i_tbb) {
				intelTBB_N2 = (- 1.0) * intelTBB_C4[i_tbb];
				intelTBB_I1 = 1.0 / intelTBB_N2;
				intelTBB_OUT[i_tbb] = intelTBB_I1;
            }
        )
        // Last Stage 3 End
    );
	// Stop the clock
	double end = (double)(tbb::tick_count::now() - mainStartTime).seconds();
	printf("Execution time(secs): %f\n\n", end);

	// Validate the results -optional- should use if we already know the final results

	int success = 1;
	for (i_tbb = 0; i_tbb < nElements; i_tbb++) {  
		if ( intelTBB_OUT[i_tbb] != 0.5) {
			success = 0;
			printf("intelTBB_OUT = %f\n", intelTBB_OUT[i_tbb]);
			fprintf(stderr, "Validation failed at index %d\n----------------------------------------\n", i_tbb);
			break;
		}
	}

	if (success) {
		fprintf(stdout, "Validation successful.\n----------------------------------------\n");
	}

   // Save results to CSV
   FILE *fp;

   if (argc > 2) {
       fp = fopen(argv[2], "a+");
       fprintf(fp, "PipeLineIntelTBB,");
       fprintf(fp, "%d,", nElements);
       fprintf(fp, "outputParallelSource2,");
       fprintf(fp, "%d,", success);
       fprintf(fp, "%f\n", end);
       fclose(fp);
       printf("Saved results in %s\n", argv[2]);
   }
	return 0;
}
